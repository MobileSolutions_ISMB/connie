﻿using API.Models;
using Microsoft.Azure.Devices.Common.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    public class TokensController : ApiController
    {
        private ITokensRepository _tokenRepository;
        private IDeviceValidator _deviceValidator;

        public TokensController(ITokensRepository tokenRepository, IDeviceValidator deviceValidator)
        {
            _tokenRepository = tokenRepository;
            _deviceValidator = deviceValidator;
        }

        // GET: api/Tokens
        public async Task<IEnumerable<IoTHubToken>> Get()
        {
            return await _tokenRepository.GetAll();
        }

        // GET: api/Tokens/{id}
        public async Task<IoTHubToken> Get(string id)
        {
            IoTHubToken token = await _tokenRepository.Get(id);

            if (token == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent("Device \"" + id + "\" not found."),
                    ReasonPhrase = "Device not found"
                });
            }

            return token;
        }

        // POST: api/Tokens
        public async Task<IoTHubToken> Post([FromBody]string id) // id = uuid+mac
        {
            if (id == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("ID must not be null"),
                    ReasonPhrase = "ID must not be null"
                });
            }

            string uuid = null;
            string mac = null;

            var r = id.Split('+');
            if (r.Length != 2)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.ExpectationFailed)
                {
                    Content = new StringContent("ID format error"),
                    ReasonPhrase = "ID format error"
                });
            }

            uuid = r[0];
            mac = r[1];

            if (!_deviceValidator.isValid(uuid))
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    Content = new StringContent("Device \"" + uuid + "\" is not valid."),
                    ReasonPhrase = "Device is not valid"
                });
            }

            // TODO: Add check on MAC Address on length and vendor ID

            try
            {
                return await _tokenRepository.Add(mac);
            }
            catch (DeviceAlreadyExistsException)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Conflict)
                {
                    Content = new StringContent("Device \"" + mac + "\" already exists."),
                    ReasonPhrase = "Device already exists"
                });
            }
            catch (Exception)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Bad Request"),
                    ReasonPhrase = "Bad Request"
                });
            }
        }

        public async Task Delete(string deviceId)
        {
            if (deviceId == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("ID must not be null"),
                    ReasonPhrase = "ID must not be null"
                });
            }

            try
            {
                await _tokenRepository.Remove(deviceId);
            }
            catch (DeviceNotFoundException ex)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(ex.Message),
                    ReasonPhrase = "Device not found"
                });
            }
        }
    }
}
