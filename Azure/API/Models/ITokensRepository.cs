﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace API.Models
{
    public interface ITokensRepository
    {
        Task<IEnumerable<IoTHubToken>> GetAll();
        Task<IoTHubToken> Get(string deviceId);
        Task<IoTHubToken> Add(string deviceId);
        Task Remove(string deviceId);
    }
}