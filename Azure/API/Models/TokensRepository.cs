﻿using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace API.Models
{
    public class TokensRepository : ITokensRepository
    {
        private RegistryManager registryManager;

        public TokensRepository(string iothubConnectionString)
        {
            registryManager = RegistryManager.CreateFromConnectionString(iothubConnectionString);
        }

        public async Task<IoTHubToken> Add(string deviceId)
        {
            Device device = await registryManager.AddDeviceAsync(new Device(deviceId));
            return new IoTHubToken()
            {
                DeviceID = device.Id,
                Token = device.Authentication.SymmetricKey.PrimaryKey
            };
        }

        public async Task<IoTHubToken> Get(string deviceId)
        {

            Device device = await registryManager.GetDeviceAsync(deviceId);

            if (device == null)
                return null;

            return new IoTHubToken()
            {
                DeviceID = device.Id,
                Token = device.Authentication.SymmetricKey.PrimaryKey
            };

        }

        public async Task<IEnumerable<IoTHubToken>> GetAll()
        {
            List<IoTHubToken> tokens = new List<IoTHubToken>();
            IEnumerable<Device> devices;

            devices = await registryManager.GetDevicesAsync(100);
            foreach (Device d in devices)
            {
                tokens.Add(new IoTHubToken()
                {
                    DeviceID = d.Id,
                    Token = d.Authentication.SymmetricKey.PrimaryKey
                });
            }

            return tokens;
        }

        public async Task Remove(string deviceId)
        {
            Device device = await registryManager.GetDeviceAsync(deviceId);

            if (device == null)
                throw new DeviceNotFoundException(deviceId);

            await registryManager.RemoveDeviceAsync(device);
        }
    }
}