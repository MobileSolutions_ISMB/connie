var http = require('http');
var https = require('https');
var crypto = require('crypto');

exports.post = function(request, response) {
    if(request.headers['content-type'] == "application/json") {
        var payload = request.body;
        payload['timecreated'] = new Date().toISOString();
        sendTemperature(JSON.stringify(payload));
        console.log(request.body);
        response.send(statusCodes.OK, payload);
    } else {
        response.send(statusCodes.OK, 'Not a valid JSON');
    }
    
};

// Add Timestamp to payload
//    request.body["timecreated"] = new Date().toISOString();

exports.get = function(request, response) {
    console.log(request.body);
    response.send(statusCodes.OK, { message : 'Hello World!' });
};

function sendTemperature(payload) {
// Event Hubs parameters
var namespace = 'micto-ehctd';
var hubname ='ehdevices';

// Shared access key (from Event Hub configuration) 
var my_key_name = 'D1'; 
var my_key = 'pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ=';

// Payload to send
//payload = "{ \"temp\": \"100\", \"hmdt\": \"78\", \"subject\": \"wthr\", \"dspl\": \"test\"," + "\"time\": " + "\"" + new Date().toISOString() + "\" }";

// Full Event Hub publisher URI
var my_uri = 'https://' + namespace + '.servicebus.windows.net' + '/' + hubname + '/publishers/'+ '1' + '/messages';

// Create a SAS token
// See http://msdn.microsoft.com/library/azure/dn170477.aspx

function create_sas_token(uri, key_name, key)
{
    // Token expires in one hour
    var now = new Date().getTime();
    var tmp = new Date(now + 3600 * 1000).getTime() / 1000;
	var expiry = Math.round(tmp);
    

    var string_to_sign = encodeURIComponent(uri) + '\n' + expiry;
    var hmac = crypto.createHmac('sha256', key);
    hmac.update(string_to_sign);
    var signature = hmac.digest('base64');
    var token = 'SharedAccessSignature sr=' + encodeURIComponent(uri) + '&sig=' + encodeURIComponent(signature) + '&se=' + expiry + '&skn=' + key_name;

    return token;
}

var my_sas = create_sas_token(my_uri, my_key_name, my_key)

console.log(my_sas);

// Send the request to the Event Hub

var options = {
  hostname: namespace + '.servicebus.windows.net',
  //hostname: '130.192.85.138',
  port: 443,
  path: '/' + hubname + '/publishers/'+ '1' + '/messages',
  method: 'POST',
  headers: {
    'Authorization': my_sas,
    'Content-Length': payload.length,
    'Content-Type': 'application/atom+xml;type=entry;charset=utf-8'
  }
};

var req = https.request(options, function(res) {
  console.log("statusCode: ", res.statusCode);
  console.log("headers: ", res.headers);

  res.on('data', function(d) {
    //process.stdout.write(d);
  });
});

req.on('error', function(e) {
  //console.error(e);
});

req.write(payload);
req.end();

}