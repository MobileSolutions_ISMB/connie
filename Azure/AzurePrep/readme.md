Il progetto richiede un certo numero di risorse Azure e di servizi che devono essere create prima di poter aggiungere i dispositivi all'infrastruttura. La configurazione di tali servizi, è prioritaria, in quanto fornirà dei parametri che dovranno essere inseriti nelle configurazioni dei dispositivi, in modo da permettere la comunicazione con la piattaforma cloud.
La configurazione manuale è molto semplice tuttavia il progetto fornisce [Azure Prep], che offre una procedura automatica per la configurazione dell'infrastruttura.
I servizi che vengono attivati e la loro configurazione, viene descritta di seguito.


## Prerequisiti ##

Make sure you have all software installed and necessary subscriptions as indicated in the Readme.md file for the project. To repeat them here, you need
Assicuratevi di avere tutto il software necessario installato e la sottoiscrizione (come abbiamo detto in precedenza)
Per riassumere, sarà necessario avere:

1. Una sottoscrizione Microsoft Azure ([free trial subscription](http://azure.microsoft.com/en-us/pricing/free-trial/) è sufficiente)
1. L'accesso alla [Azure Streaming Analytics Preview](https://account.windowsazure.com/PreviewFeatures)
1. Visual Studio 2013 – [Community Edition](http://www.visualstudio.com/downloads/download-visual-studio-vs)

## Creazione di un'infrastruttura di IoT con le risorse di Windows Azure ##

###Configurazione di Event Hubs###

* Aprite la solution ConnectTheDots\Azure\AzurePrep\AzurePrep.sln in Visual Studio e compilate il progetto (Selezionate Release, non Debug)
* Avviate l'applicazione (con il tasto F5 oppure con un doppio-click sull'eseguibile)
* Effettuate l'accesso ad Azure e se siete in possesso più di una sottoscrizione, selezionate quella in cui volete configurare i servizi
* Inserite il prefisso del namespace con cui tu vorreste nominare il vostro Service Bus per l'Event Hub
* Sciegliete il datacenter dove volete che il servizio Service Bus venga eseguito
* NOTA: l'applicazione mostrerà delle string di connessione che serviranno ai device per la connessione. Queste stringhe successivamente dovranno essere inserite nelle configurazione dei device. L'applicazione AzurePrep, crea un file di output dal quale sarà facile recuperare tali stringhe.
   
Le connection string sono formate come segue:
 
			Service Bus management connection string (i.e. per usare Service Bus Explorer):
			Endpoint=sb://ctdtest1-ns.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=zzzzzzz

			Device AMQP address strings
			amqps://D1:xxxxxxxx@yyyyyyyy.servicebus.windows.net
			amqps://D2:xxxxxxxx@yyyyyyyy.servicebus.windows.net
			amqps://D3:xxxxxxxx@yyyyyyyy.servicebus.windows.net
			amqps://D4:xxxxxxxx@yyyyyyyy.servicebus.windows.net

L'applicazione AzurePrep.exe crea 2 Event Hubs: EHDevices e EHAlerts. Il primo (EHDevices) è quello che comunicherà con i device. Per questo Event Hub sono quindi creati quattro endpoints con le relative stringhe di connessione da assegnare a quattro device come ad esempio Raspberry Pi.
Nel caso il file di output vada perso, le stringhe di connessione potranno essere facilmente recuperate dal Management Portal di Azure in questo modo:

1. aprite la pagina http://manage.windowsazure.com ed effettuate il login
2. selezionete la voce di menu  Service Bus (nella barra di navigazione sulla sinistra)
3. scegliete il vostro Namespace
4. selezionate Event Hubs dal menù in alto
5. selezionate EHDevices
6. Selezionate, nella sezione in basso, la voce Connection Infromation

###Configurazione del WebSite###
* Eseguite l'applicazione ConnecttheDots\Azure\AzurePrep\CreateWebConfig\bin\Release\CreateWebConfig.exe
* Accedete usanto le vostre credenziali della sottoscrizione Azure
* Selezionate il prefisso del Namespace e la posizione che hai scelto per la creazione degli Event Hubs
* L'applicazione creerà un web.config file sul vostro desktop. Dovrete sostituire il file creato nella nella cartella ConnectTheDotsWebSite che contiene il progetto del Sito Web del progetto