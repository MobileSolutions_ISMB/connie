﻿using CTDWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CTDWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string id)
        {
            HomeModel model = new HomeModel();

            if (id != null)
            {
                model.MAC = id;
                //Store the mac address to a session
                Session["MAC"] = id;
            }
            else
            {
                return RedirectToAction("Insert", "Home");
            }

            return View(model);
        }

        public ActionResult Insert()
        {
            InsertModel model = new InsertModel()
            {
                MAC1 = "",
                MAC2 = "",
                MAC3 = "",
                MAC4 = "",
                MAC5 = "",
                MAC6 = ""
            };

            var mac = Session["MAC"] as string;

            if(!string.IsNullOrEmpty(mac) && mac.Length == 12)
            {
                model.MAC1 = mac.Substring(0, 2);
                model.MAC2 = mac.Substring(2, 2);
                model.MAC3 = mac.Substring(4, 2);
                model.MAC4 = mac.Substring(6, 2);
                model.MAC5 = mac.Substring(8, 2);
                model.MAC6 = mac.Substring(10, 2);
            }

            return View(model);
        }




        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
    }
}