﻿var enableDebugMessages = false;


function messageOutput(message, active, error) {

    if (enableDebugMessages) {
    if (error) {
        $('#messages').prepend('<div class="errormessage">' + message + '<div>');
    }
    else {
        $('#messages').prepend('<div>' + message + '<div>');
    }

    
        if (active == true) {
            $('#messages-container').style.display = 'block';
        }
        else {
            $('#messages-container').style.display = 'none';
        }
    }
    

}