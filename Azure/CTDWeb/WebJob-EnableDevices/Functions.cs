﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using WebJobs_Shared.DeviceManager;

namespace WebJob_EnableDevices
{
    public class Functions
    {
        private readonly IDeviceManager _deviceManager;

        public Functions(IDeviceManager deviceManager)
        {
            _deviceManager = deviceManager;
        }

        // Triggers every day at 00:00:00
        public async void CronJob([TimerTrigger("0 0 0 * * *")] TimerInfo timer, TextWriter log)
        {
            await _deviceManager.enableAll();
        }
    }
}
