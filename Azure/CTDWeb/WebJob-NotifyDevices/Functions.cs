﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using WebJob_NotifyDevices.Model;
using Newtonsoft.Json;
using WebJobs_Shared.DeviceManager;
using System.Configuration;
using Microsoft.Azure.Devices.Common.Exceptions;

namespace WebJob_NotifyDevices
{
    public class Functions
    {

        private readonly IC2DMessageSender _messageSender;
        private readonly IDeviceManager _deviceManager;

        public Functions(IC2DMessageSender messageSender, IDeviceManager deviceManager)
        {
            _messageSender = messageSender;
            _deviceManager = deviceManager;
        }


        // Triggered when service bus receive message in a queue 
        public async void SBQueue2IotHubDevice(
            [ServiceBusTrigger("%queueName%")] string message,
            TextWriter log)
        {
            QueueReceivedMessage m = null;

            try
            {
                m = JsonConvert.DeserializeObject<QueueReceivedMessage>(message);
            } catch (Exception)
            {
                log.WriteLine("Exception JSONConverter: " + message);
            }

            if (m != null)
            {
                log.WriteLine("SBQueue2IotHubDevice: " + m.MacAddress);
                var slowDownMessage = new C2DMessage()
                {
                    Type = "SlowDown",
                    Message = m.MessagesCount + " messages in the last hour",
                    TimeStamp = m.Timestamp
                };
                try
                {
                    await _messageSender.SendCloudToDeviceMessageAsync(m.MacAddress, JsonConvert.SerializeObject(slowDownMessage));
                    await _deviceManager.disable(m.MacAddress);
                } catch (DeviceNotFoundException)
                {

                }
                
            }
        }
    }
}