﻿using System.Threading.Tasks;

namespace WebJobs_Shared.DeviceManager
{
    public interface IDeviceManager
    {
        Task enable(string mac);
        Task disable(string mac);
        Task remove(string mac);
        Task enableAll();
        Task disableAll();

    }
}
