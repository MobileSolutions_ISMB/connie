﻿Login-AzureRmAccount
# Below statement used to capture subscription id in variable.
$SubscriptionId = Get-AzureSubscription -Current
Select-AzureRmSubscription -SubscriptionId $SubscriptionId.Subscription.SubscriptionId

# Pick an Output folder before you run the code if needed to change.
$folder ="C:\temp\"

## No Edits below here required
## ======================================
$folder = $folder + "StreamAnalytics\"


# Helper Function to do the output prints
function PrintJobAndLog
(
 [Microsoft.Azure.Commands.StreamAnalytics.Models.PSJob] $PrintJob,
 [System.DateTime] $BeginDate,
 [System.DateTime] $EndDate,
 [System.String] $FileFolder
 )
{

    Try
    {
        # Build the file paths
        $FilePath = $FileFolder + '{0}_' + $BeginDate.ToString("yyyyMMdd") + "_" + $EndDate.ToString("yyyyMMdd") + '.txt'
       
        $ResourceId = "/subscriptions/" + $SubscriptionId.Subscription.SubscriptionId + "/resourceGroups/" + $PrintJob.ResourceGroupName + "/providers/Microsoft.StreamAnalytics/streamingjobs/" + $PrintJob.JobName

        Write-Host 'Writing log files.... ' -ForegroundColor Yellow

        # Print the Operation metrics - may take a while        
        $metrics = Get-AzureRmMetric -ResourceId $ResourceId -DetailedOutput -TimeGrain 00:01:00 -StartTime $BeginDate -EndTime $EndDate
        foreach($metric in $metrics) {
            $FilePathM = $FilePath -f $metric.Name
            Write-Host 'Saving file'  $FilePathM
            $values = $metric.MetricValues 
            $values | Out-File  $FilePathM
        }

        Write-Host 'Finished Writing log files.... at ' $FilePath ' and ' $FilePathJSON -ForegroundColor Yellow
        Write-Host '==========================================================='-ForegroundColor Yellow
    }
    Catch
    {
        Write-Host "Error in PrintAndLog:", $_ -BackgroundColor Red
    }
}


#Main

#Put all jobs into a collection
Write-Host "Enumerating your Stream Analytics jobs" -ForegroundColor Cyan

Try
    {
    
        $AllJobs = Get-AzureRmStreamAnalyticsJob
    }
Catch
    {
        Write-Host "Error in retrieving stream analytics job : ", $_ -BackgroundColor Red
    }

#Number the jobs 0 to ... then print the table to the screen
[System.Int32] $i = 0
ForEach($Job in $AllJobs) 
{ 
   Add-Member -NotePropertyName JobNum -NotePropertyValue $i -InputObject $Job 
   $i++
}
$AllJobs | Format-Table -AutoSize -Property *
Write-Host "    -1    ALL *"

#Let the user pick a number from the list of jobs
Write-Host "==============================================="
Do{
    Write-Host 'Please enter the job number from the list above. Or -1 for ALL jobs.' -ForegroundColor Cyan
    [System.Int32] $getJobNumber = Read-Host 'JobNum'
    
}
Until ($getJobNumber -lt $AllJobs.Count)

$folder = $folder + $AllJobs[$getJobNumber].JobName + "\"
Write-Host "Making a folder to hold the output:", $folder
New-Item -Path $folder  -ItemType directory


#Get input from the user to specify a number of days of log data to collect between 1 and 14 (15 max may cause errors)
Do{
    Write-Host 'How many past days worth of logs do you want to gather? 14 days maximum.' -ForegroundColor Cyan
    [System.Int32] $LogDays = Read-Host 'Days'
}
Until ($LogDays -gt 0  -and $LogDays -lt 15)
$CurrentDate= Get-Date
$StartDate = $CurrentDate.AddDays($LogDays * -1)
Write-Host "These logs will include data from ", $StartDate.ToString("yyyyMMdd"), " until today ", $CurrentDate.ToString("yyyyMMdd"), "."


PrintJobAndLog -PrintJob $AllJobs[$getJobNumber] -BeginDate $StartDate -EndDate $CurrentDate -FileFolder $folder
