
SELECT id, name, System.Timestamp AS ts, AVG(temp) AS temp, AVG(hum) AS hum, AVG(accX) AS accX, AVG(accY) AS accY, AVG(accZ) AS accZ, AVG(gyrX) AS gyrX, AVG(gyrY) AS gyrY, AVG(gyrZ) AS gyrZ,
       CASE 
            WHEN (AVG(temp) > 30) THEN 'alr'
            ELSE 'ins'
       END AS mtype,
       CASE 
            WHEN (AVG(temp) > 30) THEN 'Temperature'
            ELSE 'None'
       END AS alerttype
INTO
    ehOut
FROM
    IoTHub PARTITION BY id TIMESTAMP BY ts
GROUP BY id, name, TumblingWindow(second, 2)