Le istruzioni che seguono vi aiuteranno a configurare le queries nel componente Stream Analytics messo a disposizione da Windows Azure. Le queries descritte, potranno essere modificate secondo le vostre personali esigenze.
Prerequisito di questo documento è che voi abbiate tutto il software e le sottoscrizioni necessarie e il codice del progetto sulla vostra macchina.

## Prerequisiti ##
Per riassumere, sarà necessario avere:

1. Una sottoscrizione Microsoft Azure ([free trial subscription](http://azure.microsoft.com/en-us/pricing/free-trial/) è sufficiente)
1. L'accesso alla [Azure Streaming Analytics Preview](https://account.windowsazure.com/PreviewFeatures)
1. Visual Studio 2013 – [Community Edition](http://www.visualstudio.com/downloads/download-visual-studio-vs)

NOTA: queste queries sono create su misura dei dati inviati dai dispositivi. Questo significa che si riferiscono al JSON descritto precedentemente nelle altre pagine di questa mini guida. Quindi la struttura del JSON deve essere rispettata fedelmente, considerando anche maiuscole e minuscole: le queries sono CASE SENSITIVE, quindi "temperature" <> "TEMPERATURE".
Dovreste assicurarvi che i nomi delle misure che vengono inviate dai dispositivi siano identiche a come vengono chiamate all'interno delle queries.

## Creazione di tre jobs Azure Stream Analytics (ASA) ##

* Assicuratevi di avere accesso alla scheda degli Stream Analytics> Se non è cosi, accedi tramite  [https://account.windowsazure.com/PreviewFeatures ](https://account.windowsazure.com/PreviewFeatures )
* Creazione del primo Job
    * Aprite il management portal di Azure e create un nuovo job chiamato "Aggregetes":
        * cliccate sul tasto "+” nell'angolo in basso a sinistra -> Data Services -> Stream Analytics -> Quick Create -> Job Name: “Aggregates”.
    * Creazione di un input
        * Selezionate la sezione Input all'interno del job "Aggregates"
            * Inputs tab -> Add an Input -> Data Stream, Event Hub
        * Input Alias: “DevicesInput”
        * Subscription: “Use Event Hub from Current Subscription”
        * Scegliete il namespace `<name>`-ns, dove `<name>` è il nome scelto precedentemente, durante l'esecuzione di AzurePrep.exe per creare gli Event Hubs
        * Event Hub: “ehdevices”
        * Policy Name: “StreamAnalytics”
        * Serialization: JSON, UTF8
    * Creazione di una Query
		* Selezionate la tab Query all'interno del job Aggregates
        * Copy/paste contents “Aggregates.sql” found in the ConnectTheDots\Azure\StreamAnalyticsQueries folder in Windows Explorer
        * Copiate e incollare il contenuto del file “Aggregates.sql” che potete trovare nella cartella ConnectTheDots\Azure\StreamAnalyticsQueries
        * Salvate

    * Creazione di un output
        * All'interno della pagina del job Aggregates, selezionate il tab Output
		    * Output tab -> Add an Output, Event Hub,
		* Scegliete il namespace <name>-ns, 
        * Event Hub “ehdevices”
        * Policy name “StreamAnalytics”
        * Serialization “JSON”, UTF8
    * Fate eseguire il Job
        * Dashboard, Start
* Create un second job “Alerts”: allo stesso modo del precedente, ma usate il contenuto di “alert.sql” per la query, ed usate "ehalerts" come output, non "ehdevices".
* Create un terzo job “LightSensor”: allo stesso modo dei precedenti due. In questo caso userete “lightsensor.sql” per il contenuto della query, and userate "ehalerts" come output.