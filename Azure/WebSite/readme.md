Questo documento spiega come compilare e caricare su Azure un semplice sito web che verrà usato per la visuzlizzazione dei dati del progetto. Questo comprende, i dati inviati dai device, le rielaborazioni degli Stream Analytics e gli allarmi generati da essi. 
Prerequisito di questo documento è che voi abbiate tutto il software e le sottoscrizioni necessarie e il codice del progetto sulla vostra macchina.

## Prerequisiti ##
Per riassumere, sarà necessario avere:

1. Una sottoscrizione Microsoft Azure ([free trial subscription](http://azure.microsoft.com/en-us/pricing/free-trial/) è sufficiente)
1. L'accesso alla [Azure Streaming Analytics Preview](https://account.windowsazure.com/PreviewFeatures)
1. Visual Studio 2013 – [Community Edition](http://www.visualstudio.com/downloads/download-visual-studio-vs)


Inoltre, è dato per scontato, che siano già state eseguite le istruzioni di AzurePrep e dell'applicazione CreateWebConfig. Questo implica che voi abbiate già creato gli Event Hub su Azure, e siate già in possesso del file Web.Config generato dal tool messo a disposizione.
Se avete già fatto queste azioni preliminari spiegate nei precedenti manuali, potete procedere con il seguente manuale.
Aprite il file ConnectTheDots\Azure\WebSite\ConnectTheDotsWebSite\web.config e cercate le linee:

    <add key="Microsoft.ServiceBus.EventHubDevices" value="ehdevices" />
    <add key="Microsoft.ServiceBus.EventHubAlerts" value="ehalerts" />
    <add key="Microsoft.ServiceBus.ConnectionString" value="Endpoint=sb://{namespace-name}.servicebus.windows.net/;SharedSecretIssuer=owner;SharedSecretValue={key}" />
    <add key="Microsoft.ServiceBus.ConnectionStringDevices" value="Endpoint=sb://{namespace-name}.servicebus.windows.net/;SharedAccessKeyName=WebSite;SharedAccessKey={key}" />
    <add key="Microsoft.ServiceBus.ConnectionStringAlerts" value="Endpoint=sb://{namespace-name}.servicebus.windows.net/;SharedAccessKeyName=WebSite;SharedAccessKey={key}" />
    <add key="Microsoft.Storage.ConnectionString" value="DefaultEndpointsProtocol=https;AccountName={storageaccount};AccountKey={key}" />

Sarà necessario sostituire le ultime quattro stringhe di connessione con i valori forniti dal management portal, che potranno essere trovati su [https://manage.windowsazure.com](https://manage.windowsazure.com) così come segue:

1. **ServiceBus.ConnectionString**. Selezionate Service Bus dalla barra di navigazione a sinistra, selezionate il Namespace creato in precedenza, cliccate su Connection Information nella parte bassa dello schermo, copiate la Connection string al posto del valore key.
2. **ServiceBus.ConnectionStringDevices**. Selezionate Service Bus dalla barra di navigazione a sinistra, selezionate il Namespace creato in precedenza, evidenziate EHDevices, cliccate su Connection Information nella parte bassa dello schermo, copiate la Connection string al posto del valore key.
3. **ServiceBus.ConnectionStringAlerts**. Seleziona Servicete Bus dalla barra di navigazione a sinistra, selezionate il Namespace creato in precedenza, evidenziate EHAlerts, cliccate su Connection Information nella parte bassa dello schermo, copiate la Connection string al posto del valore key.
4. **Storage.ConnectionString**. Selezionate Storage dalla barra di navigazione a sinistra, evidenziate lo Storage Account creato in precedenza, cliccate su Manage Access Keys nella parte bassa dello schero, copiate la Primary Access key al posto del balore key

## Publicazione del Sito us Azure ##

* Aprite la soluziona ConnectTheDots\Azure\WebSite\ConnectTheDotsWebSite.sln in Visual Studio
* In VS, Tasto destro sul nome del progetto e selezionate Publish
* Selezionate Azure Web Sites, Createne uno nuovo
    * Site name: [scegliete un nome univoco]
    * Region: [scegliete la stessa regione usata per gli Stream Analytics]
    * Database server: nessun database
    * Password: [lasciate quella suggerita]

* Publish (you might need to install WebDeploy extension if you are having an error stating that the Web deployment task failed. You can find WebDeploy [here](http://www.iis.net/downloads/microsoft/web-deploy)).
* Publicate (se apparirà un errore che vi annuncierà un errore nel processo di Web Deployment, forse avrete bisogno di insallare l'estensione WebDeploy. Puoi trovare il WebDeploy [qui](http://www.iis.net/downloads/microsoft/web-deploy)).

## Configurazione Websockets ##
* Attiva i WebSockets per il nuovo sito caricato su Azure
    * Naviga all'interno di https://manage.windowsazure.com e seleziona il sito appena caricato
    * Clicca su Configure. Attiva WevSockets mettendo su On l'opzione e clicca su Save
	
##Esegui il sito
* Apri il sito in un browser per verificare che sia stato deployato correttamente
    * Al fondo della paggina dovresti vedere "Connected". Se vedi "ERROR undefined" probabilmente non hai attivato i WebSockets per Azure Web Site (vedi lo step precedente)