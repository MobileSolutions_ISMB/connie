## Comunicazione con Azure ##
La comunicazione con la piattaforma Windows Azure, avviene tramite la configurazione del servizio **Event Hub**. Questo servizio mette a disposizione un punto di accesso a cui può essere inviato un qualsiasi JSON che verrà memorizzato in una tabella non relazionale, immagazzinata nella piattaforma cloud, e le quali colonne saranno formate dai campi del JSON inviato. Questi dati successivamente potranno essere elaborati/visualizzati/modificati per vari scopi.
L'unica specifica richiesta dal progetto, per la comunicazione dei punti (sensori) con la piattaforma, è **il formato dei dati spediti**: 

I dati dovranno essere spediti all'Event Hub con il seguanete formato JSON.


    {
	"guid" 			:	"string",
	"organization"	:	"string",
	"displayname"	:	"string",
	"location"		:	"string",
	"measurename"	:	"string",
	"unitofmeasure"	:	"string",
	"value" 		:	double/float/integer,
	"timecreated"	:	"string"
	}
	
Un esempio di dati inviati: 

    {"guid":"62X74059-A444-4797-8A7E-526C3EF9D64B","organization":"My Org Name","displayname":"Sensor Name","location":"Sensor Location","measurename":"Temperature","unitofmeasure":"C","value":32.47, "timecreated":"2015-06-19T07:52:37.003712Z"}

oppure

    {"guid":"62X74059-A444-4797-8A7E-526C3EF9D64B","organization":"my org name","displayname":"sensor name","location":"sensor location","measurename":"Temperature","unitofmeasure":"F","value":74.0001, "timecreated":"2015-06-19T07:52:37.003712Z"}

La stringa JSON deve essere mandata all'Event Hub di windows azure impacchettata in messaggi AMQP oppure all'interno di un pacchetto REST.

Nelle pagine descrittive per ogni dispositivo, è fornito un esempio più dettagliato del pacchetto JSON utilizzato.


## Prerequisiti Software ##
Per ricostruire la piattaforma cloud del progetto, è necessario avere:

1. Una sottoscrizione per Microsoft Azure  (è sufficiente una [free trial subscription](http://azure.microsoft.com/en-us/pricing/free-trial/))
1. Visual Studio 2013 [Community Edition](http://www.visualstudio.com/downloads/download-visual-studio-vs) o superiori

## Setup Tasks ##
I passi da eseguire per ottenere la struttura di IoT che vogliamo, sono i seguenti:

1. Clonare o copiare il progetto sulla vostra macchina
1. [Preparazione della piattaforma Azure](AzurePrep/readme.md) - Impostazione base delle risorse di Windows Azure da utilizzare
1. [Configurazione dei setup](../Devices/readme.md) - Configurazione dei device
1. [Caricamento dell'interfaccia WEB sul Cloud ](WebSite/readme.md) - Publicazione di un semplice e generico sito WEB per la visualizzazione dei dati
2. [Creazione e configurazione di un Azure Mobile Service](AzureMobileService/readme.md) - in caso di dispositivi senza possibilità di connessione sicura tramite HTTPS 

All'interno del progetto potrete trovare istruzioni più accurate di come eseguire questi semplici passi

## Risultato ##
Una volta che tutti i servizi e i device saranno configurati correttamente e il sito sarà pubblicato, inizieranno a comparire sulla pagina WEB i dati inviati dai device.

Dovrebbero apparire le misure istantanee inviate dai dispositivi e la media ogni 20 secondi.

![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Images/WebSiteCapture.png)
