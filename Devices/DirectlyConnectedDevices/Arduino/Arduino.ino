﻿#include <Wire.h>
#include <SPI.h>
#include <Ethernet.h>
#include "Sensor.h"
#include "EventHub.h"

byte mac[] = { 0x90, 0xA2, 0xDA, 0x0F, 0xC5, 0xA6 };
IPAddress ip(130, 192, 85, 139);
IPAddress subnet(255, 255, 255, 0);
IPAddress gateway(130, 192, 85, 17);
IPAddress dnsServ(130, 192, 3, 21);
EthernetClient client;
SensorClass sensor;
EventHubClass eventDispatcher;

void setup()
{
	// add setup code here

	// Initialize Serial Port
	Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for Leonardo only
	}

	//Ethernet.begin(mac, ip, dnsServ, gateway, subnet);	// Static Address
	///*
	// Initialize the Ethernet controller
	if (Ethernet.begin(mac) == 0) {
		Serial.println("Failed to configure Ethernet using DHCP");
		// no point in carrying on, so do nothing forevermore:
		Ethernet.begin(mac, ip, dnsServ, gateway, subnet);	// Static Address

	}
	else {
		Serial.println("Ethernet initialization done!");
	}
	//*/

	delay(5000);

	// Initialize temperature sensor
	sensor.init();
	eventDispatcher.init(client);


}

void loop()
{
	// add main program code here

	// Read temperature from sensor and dispatch to Event Hub
	float a = sensor.getTemperature();
	eventDispatcher.send_measure(a);
	delay(1000);
}