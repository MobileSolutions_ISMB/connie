// Sensor.h

#ifndef _SENSOR_h
#define _SENSOR_h

#define HTS221

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Wire.h>

#ifdef HTS221
#define I2C_ADDR	0x5F			// Temperature sensor I2C Address: HTS221 datasheet
#define CTRL_REG1	0x20			// Control Register 1
#define CAL_REG1	0x32			// Calibration values Register 1
#define CAL_REG2	0x3C			// Calibration values Register 2
#define T_OUT_REG	0x2A			// Temperature Out Register
#define AUTOINCREMENT	(1<<7)		// Address modifier for autoincrement
#else
#define I2C_ADDR	0x4F
#define CONF_REG	0x01
#define T_OUT_REG	0x00
#endif

class SensorClass
{
 private:
#ifdef HTS221
	 // Calibration variables
	 float T0_degC, T1_degC;
	 int T0_OUT, T1_OUT;
#endif

 public:
	void init();
	float getTemperature();
};

extern SensorClass Sensor;

#endif

