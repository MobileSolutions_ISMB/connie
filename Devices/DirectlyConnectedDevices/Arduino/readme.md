## Teoria di funzionamento ##
Il seguente esempio prevede l'utilizzo di una board **Arduino Ethernet** (oppure un Arduino Uno + Ethernet Shield), l'expansion sensor board **ST X-NUCLEO-IKS01A1** e un **AzureMobileService**.

Il progetto è stato creato con Visual Studio 2013 Community Edition tramite il plugin **VisualMicro**. 

Sull'expansion board sono presenti diversi sensori, tra cui un sensore di temperatura (ST HTS211). Questo esempio si limiterà a dimostrare l'utilizzo solo di quest'ultimo. Tale sensore utilizza il bus **I2C** per la comunicazione con il microcontrollore.

Per una **descrizione dettagliata del protocollo** utilizzato dal sensore HTS221 fare riferimento a [[Protocollo di comunicazione HTS211](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/HTS221/readme.md)]


### Schema Logico ###
![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/ArduinoToolchain/6.png)

Data la mancanza della potenza di calcolo necessaria per l'utilizzo di una connessione HTTP sicura, verrà utilizzato un AzureMobileService, che fungerà da **proxy** verso l'eventHub.

L'applicazione di esempio provvede a leggere a intervalli regolari (1 sec.) tramite il bus I2C il valore di temperatura dal sensore, crea il messaggio JSON e lo invia tramite una chiamata **HTTP POST** all'AzureMobileService.
Poichè non è presente un modulo RTC (Real Time Controller) all'interno del microcontrollore a bordo dell'Arduino, il servizio mobile provvede ad aggiungere al messaggio JSON ricevuto il timestamp corrente. In seguito esso genera il **SAS Token tramite HMAC-SHA256**, e infine reinvia il nuovo messaggio JSON tramite **HTTPS POST** all'EventHub Azure.

###### Esempio di HTTP POST da Arduino a AzureMobileService ######
	POST /api/<amsApi> HTTP/1.1
	Host: <ams>
	X-ZUMO-APPLICATION: <amsKey>
	Cache-Control: no-cache
	Content-type: application/json
	Content-Length: <body length>

	{"guid": "<guid>","organization": "ISMB","displayname": "Arduino","location": "Torino","measurename": "Temperature","unitofmeasure": "C","value": 26.43 }

###### Esempio di HTTPS POST da AzureMobileService a EventHub ######
	POST /<eventHub>/publishers/<device>/messages HTTP/1.1
	Host: <eventHub>.servicebus.windows.net
	Cache-Control: no-cache
	Content-Type: application/json; charset=UTF-8
	Content-Length: <body length>
	Authorization: SharedAccessSignature <sas string>
	
	{"guid": "<guid>","organization": "ISMB","displayname": "Arduino","location": "Torino","measurename": "Temperature","unitofmeasure": "C","timecreated": "2015-05-19T07:52:37.003712Z","value": 26.43 }

Negli esempi soprastanti è necessario sostituire tutte le stringhe comprese tra < e > con dei valori validi.

### Requisiti Hardware ###
- Arduino Ethernet (o Arduino Uno + Shield Ethernet) [[Specifiche tecniche](http://www.arduino.cc/en/Main/ArduinoBoardEthernet)]
- Sensor board ST X-Nucleo-IKS01A1 [[Specifiche tecniche](http://www.st.com/web/catalog/tools/FM116/SC1248/PF261191)]
- Connessione a internet tramite cavo ethernet

### Requisiti Software ###

- Visual Studio 2013 Community Edition
- VisualMicro plugin [[Guida all'installazione](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/VisualMicro/readme.md)]

