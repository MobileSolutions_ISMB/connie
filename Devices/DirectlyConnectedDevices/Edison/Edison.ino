#include <WiFiUdp.h>
#include <WiFiServer.h>
#include <WiFiClient.h>
#include <WiFi.h>
#include <Dns.h>
#include <Dhcp.h>
#include <Wire.h>
#include <SPI.h>
#include <math.h>

#include "Sensor.h"
#include "EventHub.h"

byte mac[] = { 0x90, 0xA2, 0xDA, 0x0F, 0xC5, 0xA6 };	//EDISON
char ssid[] = "h2u_Guest";								//  your network SSID (name) 
char pass[] = "h2uguest2015";							// your network password (use for WPA, or use as key for WEP)

char str_temperature[10];

int status = WL_IDLE_STATUS;

WiFiClient client;
SensorClass sensor;
EventHubClass eventDispatcher;

void setup()
{
	// add setup code here

	// Initialize Serial Port
	Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for Leonardo only
	}

	Serial.println("Starting...");

	// check for the presence of the shield:
	if (WiFi.status() == WL_NO_SHIELD) {
		Serial.println("WiFi shield not present");
		// don't continue:
		while (true);
	}

	String fv = WiFi.firmwareVersion();
	if (fv != "1.1.0")
		Serial.println("Please upgrade the firmware");

	// attempt to connect to Wifi network:
	while (status != WL_CONNECTED) {
		Serial.print("Attempting to connect to SSID: ");
		Serial.println(ssid);
		// Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
		status = WiFi.begin(ssid, pass);

		// wait 10 seconds for connection:
		for (int i = 0; i < 10; i++) {
			delay(1000);
			Serial.print("Status: ");
			Serial.println(status);
		}
	}
	Serial.println("Connected to wifi");
	printWifiStatus();

	delay(5000);

	// Initialize temperature sensor
	sensor.init();
	eventDispatcher.init(client);


}

void loop()
{
	// add main program code here

	// Read temperature from sensor and dispatch to Event Hub
	float a = sensor.getTemperature();
	floatToStr(a, 2, str_temperature);
	Serial.println(str_temperature);
	eventDispatcher.send_measure(str_temperature);
	delay(1000);
}

void printWifiStatus() {
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your WiFi shield's IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);

	// print the received signal strength:
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.print(rssi);
	Serial.println(" dBm");
}

static void floatToStr(float in, uint8_t dec_prec, char *str)
{
	int32_t out_int = (int32_t)in;
	in = in - (float)(out_int);
	int32_t out_dec = (int32_t)trunc(in*pow(10.0f, dec_prec));
	sprintf(str, "%d.%d", out_int, out_dec);
}