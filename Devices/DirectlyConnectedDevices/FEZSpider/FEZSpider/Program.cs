﻿using System;
using System.Collections;
using System.Threading;
using System.IO;
using System.Net;
using System.Text;
using System.Security.Cryptography.X509Certificates;
using Microsoft.SPOT;
using Microsoft.SPOT.Net;
using Microsoft.SPOT.Net.NetworkInformation;
using Microsoft.SPOT.Time;

using GT = Gadgeteer;
using GTM = Gadgeteer.Modules;
using Gadgeteer.Modules.GHIElectronics;
using GHINet = GHI.Networking;

using Toolbox.NETMF;
using Json.NETMF;


namespace FEZSpider
{
    public partial class Program
    {

        private GT.Timer timer;
        private int TokenValidity = 0;

        private const int ServiceBusPort = 80;
        private const string ServiceBus = "micto-ehctd";
        private const string EventHubName = "ehdevices";
        private const string KeyName = "D1";
        private const string Key = "pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ=";
        private const string DisplayName = "FEZSpider";
        private string SAS = "";

        private Uri uri;
        private DataPacket dataPkt;

        // This method is run when the mainboard is powered up or reset.   
        void ProgramStarted()
        {
            /*******************************************************************************************
            Modules added in the Program.gadgeteer designer view are used by typing 
            their name followed by a period, e.g.  button.  or  camera.
            
            Many modules generate useful events. Type +=<tab><tab> to add a handler to an event, e.g.:
                button.ButtonPressed +=<tab><tab>
            
            If you want to do something periodically, use a GT.Timer and handle its Tick event, e.g.:
                GT.Timer timer = new GT.Timer(1000); // every second (1000ms)
                timer.Tick +=<tab><tab>
                timer.Start();
            *******************************************************************************************/

            // Use Debug.Print to show messages in Visual Studio's "Output" window during debugging.
            Debug.Print("Program Started");

            timer = new GT.Timer(2000);                // every second (1000ms)
            timer.Tick += timer_Tick;

            
            uri = new Uri("https://" + ServiceBus +
                          ".servicebus.windows.net/" + EventHubName +
                          "/publishers/" + DisplayName + "/messages");
            
            dataPkt = new DataPacket(
                "1178a348-e2f9-4438-ab23-82a3930662ab",
                "ISMB",
                DisplayName,
                "Torino",
                "Temperature",
                "C");

            SetupEthernet();           
        }

        void SetupEthernet()
        {
            
            /* NETMF 4.2
            eth.Interface.NetworkAddressChanged += Interface_NetworkAddressChanged;
            eth.Interface.CableConnectivityChanged += Interface_CableConnectivityChanged;
            eth.UseThisNetworkInterface();
            eth.UseDHCP();            
            */

            Microsoft.SPOT.Net.NetworkInformation.NetworkChange.NetworkAvailabilityChanged += NetworkChange_NetworkAvailabilityChanged;
            NetworkChange.NetworkAddressChanged += Interface_NetworkAddressChanged;
            eth.NetworkInterface.Open();
            eth.NetworkInterface.EnableDhcp();
            eth.NetworkInterface.EnableDynamicDns();
        }

        void NetworkChange_NetworkAvailabilityChanged(object sender, NetworkAvailabilityEventArgs e)
        {
            Debug.Print("Cable connectivity changed! now is " + (e.IsAvailable ? " " : "NOT ") + "connected");
            if (!e.IsAvailable)
                timer.Stop();
        }

        /*
        void Interface_CableConnectivityChanged(object sender, GHINet.EthernetBuiltIn.CableConnectivityEventArgs e)
        {
            Debug.Print("Cable connectivity changed! now is " + (e.IsConnected ? " ": "NOT ") + "connected");
        }
        */

        void Interface_NetworkAddressChanged(object sender, EventArgs e)
        {
            //if(eth.Interface.NetworkInterface.IPAddress != "0.0.0.0")     // for 4.2
            if (eth.NetworkInterface.IPAddress != "0.0.0.0")
            {
                ListNetworkInterfaces();
                SyncRTC();

                timer.Start();
                Debug.Print("Timer started!");
            }
        }

        void ListNetworkInterfaces()
        {
            NetworkInterface EthIface = eth.NetworkInterface.NetworkInterface;
            //NetworkInterface EthIface = eth.Interface.NetworkInterface;       // for 4.2

            // Display network config for debugging
            Debug.Print("Network configuration");
            Debug.Print(" MAC Address: " + ByteExtensions.ToHexString(EthIface.PhysicalAddress));
            Debug.Print(" DHCP enabled: " + EthIface.IsDhcpEnabled.ToString());
            Debug.Print(" Dynamic DNS enabled: " + EthIface.IsDynamicDnsEnabled.ToString());
            Debug.Print(" IP Address: " + EthIface.IPAddress.ToString());
            Debug.Print(" Subnet Mask: " + EthIface.SubnetMask.ToString());
            Debug.Print(" Gateway: " + EthIface.GatewayAddress.ToString());
            foreach (string dnsAddress in EthIface.DnsAddresses)
            {
                Debug.Print(" DNS Server: " + dnsAddress.ToString());
            }
        }

        void SyncRTC()
        {
            TimeService.SystemTimeChanged += delegate { Debug.Print("Synchronized: " + DateTime.Now.ToLocalTime()); };
            TimeServiceSettings settings = new TimeServiceSettings();
            settings.RefreshTime = 10;                      // every 10 seconds
            settings.ForceSyncAtWakeUp = true;

            //TimeService.SetTimeZoneOffset(60 * 2);          // Central Europe Timezone

            settings.PrimaryServer = new byte[] { 24, 56, 178, 140 };       // time.nist.gov
            settings.AlternateServer = new byte[] { 104, 209, 134, 106 };   // time.windows.com

            TimeService.Settings = settings;

            TimeService.Start();
            
        }

        void timer_Tick(GT.Timer timer)
        {
            if (TokenValidity <= 0)
            {
                SAS = SASTokenHelper(uri.OriginalString);
                TokenValidity = 19 * 60;
            }

            SendEvent(uri.OriginalString, SAS, CreateJSON((float)si70.TakeMeasurement().Temperature));
            TokenValidity -= timer.Interval.Seconds;

        }

        private string CreateJSON(float temperature)
        {
            dataPkt.timecreated = DateTime.UtcNow.ToString() + ".000000Z";
            dataPkt.value = temperature;
            string json = JsonSerializer.SerializeObject(dataPkt);
            return json;
        }

        private string SASTokenHelper(string strToSign)
        {
            TimeSpan ts = DateTime.UtcNow.AddMinutes(20).Subtract(new DateTime(1970, 1, 1));
            int expiry = (int)(ts.Ticks / 10000000);
            string stringToSign = Tools.RawUrlEncode(strToSign) + "\n" + expiry.ToString();
            string signature = getHmacSha256(Key.ToString(), stringToSign);
            string token = "SharedAccessSignature sr=" + Tools.RawUrlEncode(uri.OriginalString) + "&sig=" + Tools.RawUrlEncode(signature) + "&se=" + expiry + "&skn=" + KeyName.ToString();

            return token;
        }

        public string getHmacSha256(string key, string value)
        {
            UTF8Encoding  utf8 = new UTF8Encoding();
            byte[] keyStrm = utf8.GetBytes(key);
            byte[] valueStrm = utf8.GetBytes(value);

            byte[] hashmessage = HMAC.computeHMAC_SHA256(keyStrm, valueStrm);

            //string s = Convert.ToBase64String(hashmessage);     // NOT RFC4648Encoding!!! netmfc Bug in alphabet used
            string s = HttpUtility.ToBase64String(hashmessage);
            return s;
        }

        public static bool SendEvent(string requestUri, string authorizationToken, string eventData)
        {
            bool result = false;

            try
            {
                using (HttpWebRequest request = HttpWebRequest.Create(requestUri) as HttpWebRequest)
                {
                    request.Headers.Set("Authorization", authorizationToken);
                    request.Headers.Set("Content-Type", "application/atom+xml;type=entry;charset=utf-8");
                    request.Method = "POST";

                    byte[] eventDataBuffer = Encoding.UTF8.GetBytes(eventData);
                    request.ContentLength = eventDataBuffer.Length;

                    using (Stream requestStream = request.GetRequestStream())
                    {
                        requestStream.Write(eventDataBuffer, 0, eventDataBuffer.Length);
                    }

                    using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                    {
                        if (response.StatusCode == HttpStatusCode.Created)
                        {
                            result = true;
                        }
                        else
                        {
                            throw new ApplicationException("Unexpected failure; status code: " + response.StatusCode + "; description: " + response.StatusDescription);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Print("SendEvent(string, string, string) encountered an unexpected exception.  Details: " + ex.ToString());
            }

            return result;
        }
    }
}
