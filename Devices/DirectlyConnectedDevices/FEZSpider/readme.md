## Teoria di funzionamento ##
Il seguente esempio prevede l'utilizzo di una board **FEZ Spider** e del modulo **TempHumid Si70**.

Il progetto è stato creato con Visual Studio 2013 Community Edition e **.NET Micro Framework 4.3**. 

Il modulo per la misura della temperatura utilizza un sensore digitale di temperatura Si7020. Tale sensore utilizza il bus **I2C** per la comunicazione con il microprocessore.

Per una **descrizione dettagliata del sensore** utilizzato fare riferimento al sito ufficiale [[TempHumid Si70](https://www.ghielectronics.com/catalog/product/528)]

### Schema Logico ###
![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/FEZSpider/1.png) 

Data la presenza di librerie adatte all'utilizzo di una connessione HTTP sicura, i dati del sensore verranno inviati **direttamente** all'EventHub Azure.

L'applicazione di esempio provvede a leggere a intervalli regolari (5 sec.) tramite il bus I2C il valore di temperatura dal sensore, crea il messaggio JSON e lo invia tramite una chiamata **HTTPS POST** all'EventHub.

Per semplificare la generazione e la serializzazione del messaggio JSON è stata utilizzata la libreria Json.NETMF, scaricabile gratuitamente tramite NuGet Packet Manager di Visual Studio.

Inoltre, avendo a disposizione un RTC che gestisce l'orologio interno del microprocessore, è possibile includere direttamente nel pacchetto JSON il timestamp corrente. Nell'esempio è stata inclusa l'implementazione in C# dell'algoritmo di cifratura per la generazione del **SAS Token tramite HMAC-SHA256**, il quale viene aggiunto agli header standard della richiesta HTTPS.

> Per maggiori informazioni sulla generazione del **SAS Token** clicca [qui](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/SAS.md)

###### Esempio di HTTPS POST da FEZ Spider a EventHub ######
	POST /<eventHub>/publishers/<device>/messages HTTP/1.1
	Host: <eventHub>.servicebus.windows.net
	Content-Type: application/json; charset=UTF-8
	Content-Length: <body length>
	Authorization: SharedAccessSignature <sas string>
	
	{"guid": "<guid>","organization": "ISMB","displayname": "Arduino","location": "Torino","measurename": "Temperature","unitofmeasure": "C","timecreated": "2015-05-19T07:52:37.003712Z","value": 26.43 }

Nell'esempio soprastante è necessario sostituire tutte le stringhe comprese tra < e > con dei valori validi.

### Requisiti Hardware ###
- FEZ Spider Mainboard [[Specifiche tecniche](https://www.ghielectronics.com/catalog/product/269)]
- TempHumid Si70 Module [[Specifiche tecniche](https://www.ghielectronics.com/catalog/product/528)]
- USB Client DP Module [[Specifiche tecniche](https://www.ghielectronics.com/catalog/product/280)
- Ethernet J11D Module [[Specifiche tecniche](https://www.ghielectronics.com/catalog/product/284)]
- Connessione a internet tramite cavo ethernet

### Requisiti Software ###
- Visual Studio 2013 Community Edition
- Gadgeteer & .NET Micro Framework Toolchain 4.3 [[Guida all'installazione](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/FEZSpider/readme.md)]

