#include "EventHub.h"

void EventHubClass::init(EthernetClient c)
{
	client = c;
}

void EventHubClass::send_measure(float val) {
	IPAddress test(191, 235, 176, 12);
	char buffer[300];
	// if you get a connection, report back via serial:
	//int connRes = client.connect(test, 80);
	int connRes = client.connect(AzureMobileService, 80);
	if (connRes == 1) {
		Serial.println("connected");

		// POST URI
		sprintf(buffer, "POST /api/%s HTTP/1.1", AzureMobileServiceAPI);
		//Serial.println(buffer);
		client.println(buffer);

		// Host header
		sprintf(buffer, "Host: %s", AzureMobileService);
		//Serial.println(buffer);
		client.println(buffer);

		// Application key
		/*
		sprintf(buffer, "Authorization: %s", sas);
		Serial.println(buffer);
		client.println(buffer);
		*/

		// AMS Application Key 
		sprintf(buffer, "X-ZUMO-APPLICATION: %s", AzureMobileServiceKey);
		//Serial.println(buffer);
		client.println(buffer);

		// Cache control
		//Serial.println("Cache-Control: no-cache");
		client.println("Cache-Control: no-cache");

		// content type
		//Serial.println("Content-Type: text/plain");
		client.println("Content-Type: application/json");

		// POST body
		char value[10];
		//dtostrf(val, 2, 2, value);

		sprintf(buffer, json,
			"4198a348-e2f9-4438-ab23-82a3930662ab",
			"ISMB",
			"Galileo",
			"Torino",
			"Temperature",
			"C",
			(int)val
			);

		// Content length
		//Serial.print("Content-Length: ");
		//Serial.println(strlen(buffer));
		client.print("Content-Length: ");
		client.println(strlen(buffer));

		// End of headers
		//Serial.println();
		client.println();

		// Request body
		Serial.println(buffer);
		client.println(buffer);

		client.stop();
	}
	else {
		// kf you didn't get a connection to the server:
		Serial.print("connection failed: ");
		Serial.println(connRes);
	}
}

EventHubClass EventHub;

