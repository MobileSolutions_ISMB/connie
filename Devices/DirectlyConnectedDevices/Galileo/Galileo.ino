#include <Wire.h>
#include <SPI.h>
#include <Ethernet.h>
#include "Sensor.h"
#include "EventHub.h"

byte mac[] = { 0x98, 0x4F, 0xEE, 0x05, 0x13, 0x68 };	// GALILEO

EthernetClient client;
SensorClass sensor;
EventHubClass eventDispatcher;

void setup()
{
	// add setup code here

	// Initialize Serial Port
	Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for Leonardo only
	}

	// Initialize the Ethernet controller
	if (Ethernet.begin(mac) == 0) {
		Serial.println("Failed to configure Ethernet using DHCP");
		// no point in carrying on, so do nothing forevermore:
		while (1)
			;

	}
	else {
		Serial.println("Ethernet initialization done!");
	}

	delay(5000);

	// Initialize temperature sensor
	sensor.init();
	eventDispatcher.init(client);

}

void loop()
{
	// add main program code here

	// Read temperature from sensor and dispatch to Event Hub
	float a = sensor.getTemperature();
	Serial.println(a);
	eventDispatcher.send_measure(a);
	delay(2000);
}