/**
  ******************************************************************************
  * @file    hash_hw.c
  * @author  MCD Application Team
  * @version V2.1
  * @date    22-June-2012
  * @brief   Provides HW HASH functions
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hash_common.h"

/* This file needs compiler guards because it is can be used by MD5 and SHA-1 */
#ifndef __HASH_HW_C__
#define __HASH_HW_C__

/** @addtogroup HASHlowlevel HASH
  * @{
  */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Initialize the HW HASH engine
  * @note   Static function
  * @param[in,out] *P_pContext The context that will be initialized 
  * @param[in]      P_hashType Descriptor of the HASH functionalities to be used 
  */
static void HASHhwInit(HASHctx_stt *P_pContext, hashType_et P_hashType)
{
  HASH_InitTypeDef HASH_st;

  /* Enable Hash Engine */
  RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_HASH, ENABLE);
  /* Deinit */
  HASH_DeInit();
  P_pContext->amCount[0] = 0;
  P_pContext->amCount[1] = 0;

  /* initialize the HASH_AlgoMode member */
  HASH_st.HASH_AlgoMode = HASH_AlgoMode_HASH;
  /* initialize the CRYPTO_DataType member */
  HASH_st.HASH_DataType = HASH_DataType_8b;
  /* Initialize the HASH_KeyType member */
  HASH_st.HASH_HMACKeyType = HASH_HMACKeyType_ShortKey;
  switch (P_hashType)
  {
    case E_SHA1:
      HASH_st.HASH_AlgoSelection = HASH_AlgoSelection_SHA1;
      break;
    case E_MD5:
      HASH_st.HASH_AlgoSelection = HASH_AlgoSelection_MD5;
      break;
    default:
      break;
  }

  HASH_Init(&HASH_st);
  HASH_Reset();
}

/**
  * @brief  Process Data with Hardware HASH Engine
  * @note   Static function
  * @param[in,out]  *P_pContext The context that will be updated
  * @param[in]      *P_pInput Pointer to the input buffer
  * @param[in]       P_inputSize Size of input in bytes
  * @retval HASH_SUCCESS Operation Succefull
  * @retval DMA_BAD_ADDRESS   P_pInput must be word aligned to be used with DMA
  * @retval DMA_ERR_TRANSFER  Generic error in the DMA trnasfer
  */
static int32_t HASHhwAppend(HASHctx_stt *P_pContext, const uint8_t *P_pInput, int32_t P_inputSize )
{
  /* If DMA is requested */
  if ( (P_pContext->mFlags & E_HASH_USE_DMA) == E_HASH_USE_DMA )
  {
    uint32_t reg;
    DMA_InitTypeDef DMA_InitStructure;

    /* This is the one and only APPEND */
    P_pContext->mFlags |= E_HASH_NO_MORE_APPEND_ALLOWED;

    /* Set the Mask if byte-aligned */
    if ( (P_inputSize & 3) != 0 )
    {
      HASH->STR = ((P_inputSize & 3) * 8);
    }
    /* Check if input address is byte aligned */
    if ( ((uint32_t)&P_pInput[0] & 3 != 0))
    {
      return(DMA_BAD_ADDRESS);
    }
    /* Enable DMA Clock*/
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

    DMA_DeInit(DMA2_Stream7);
    DMA_InitStructure.DMA_Channel = DMA_Channel_2;
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) 0x50060404;
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)(P_pInput);
    DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    DMA_InitStructure.DMA_BufferSize = P_inputSize / 4 + ( ((P_inputSize % 4) == 0) ? 0 : 1)  ;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Enable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_INC4;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_INC4;

    DMA_Init(DMA2_Stream7, &DMA_InitStructure);

    HASH_DMACmd(ENABLE);
    /* DMA2_Stream5 enable */
    DMA_Cmd(DMA2_Stream7, ENABLE);
    /* Wait till its Busy */
    /* Wait till output transfer has completed or there is any error on I/O streams */
    do
    {
      reg = DMA2->HISR & (DMA_HISR_TEIF7 | DMA_HISR_TCIF7);
    }
    while ( reg == 0);
    /* If errors then return error */
    if ( (reg & DMA_HISR_TEIF7) == DMA_HISR_TEIF7 )
    {
      /* Error in DMA transfer */
      return(DMA_ERR_TRANSFER);
    }
    HASH_DMACmd(DISABLE);
    /* Disable DMA Clock */
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, DISABLE);
  }
  else
  {
    int32_t i = 0;
    /* If there's left an uncompleted word, try to complete it*/
    if ( P_pContext->amCount[0] != 0)
    {
      /* Write in the uncompleted word, till there's input or it's full */
      for ( ; (P_pContext->amCount[0] < 4) && ( i < P_inputSize); P_pContext->amCount[0] += 1)
      {
        P_pContext->amBuffer[P_pContext->amCount[0]] = P_pInput[i++];
      }
      /* If it's full then process it */
      if ( P_pContext->amCount[0] == 4)
      {
        HASH->DIN = ((uint32_t *)(P_pContext->amBuffer))[0];
        /* while((HASH->SR & HASH_FLAG_BUSY) != 0) {} */
        P_pContext->amCount[0] = 0;
      }
    }

    /* Loop on every full word and process it */
    for (; (i + 32) <= P_inputSize; i += 32)
    {
      HASH->DIN = ((uint32_t *)(P_pInput + i))[0];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[1];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[2];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[3];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[4];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[5];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[6];
      HASH->DIN = ((uint32_t *)(P_pInput + i))[7];
      /* while((HASH->SR & HASH_FLAG_BUSY) != 0) {} */
    }
    for (; (i + 4) <= P_inputSize; i += 4)
    {
      HASH->DIN = ((uint32_t *)(P_pInput + i))[0];
      /* while((HASH->SR & HASH_FLAG_BUSY) != 0) {} */
    }

    /* Save extra bytes in the buffer */
    for ( ; i < P_inputSize ; i++)
    {
      P_pContext->amBuffer[P_pContext->amCount[0]] = P_pInput[i];
      P_pContext->amCount[0] += 1;
    }
  } /* End DMA if */
  return(HASH_SUCCESS);
}

/**
  * @brief  Read the Digest from the HW core
  * @note   Static function
  * @param[in]  *P_pContext The context that will be initialized 
  * @param[out] *P_pDigest Where the 20 byte digest will be written 
  */
static void HASHhwFinish(HASHctx_stt *P_pContext, uint8_t *P_pDigest)
{
  /* Put last word and compute the difest*/
  int32_t i;

  /* In case there's no DMA check if there are bytes left */
  if ( (P_pContext->mFlags & E_HASH_USE_DMA) != E_HASH_USE_DMA )
  {
    if ( P_pContext->amCount[0] != 0 )
    {
      HASH->DIN = ((uint32_t *)(P_pContext->amBuffer))[0];
      HASH->STR = (P_pContext->amCount[0] * 8);
    }
    HASH->STR |= HASH_STR_DCAL;
  }
  while ((HASH->SR & HASH_FLAG_BUSY) != 0)
  {}

  for (i = 0 ; i < 5; i++)
  {
    ((uint32_t *)(P_pDigest))[i] =  __REV( HASH->HR[i] );
  }
  /* Disable Hash Engine */
  RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_HASH, DISABLE);
}
/**
  * @}
  */
#endif /* __HASH_HW_C__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
