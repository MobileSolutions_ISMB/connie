/**
  ******************************************************************************
  * @file    sha256_sha224_transform.c
  * @author  MCD Application Team
  * @version V2.1
  * @date    22-June-2012
  * @brief   SHA-224 and SHA-256 core function
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/ 
#include <string.h>
#include "sha256_sha224_transform.h"

/* This file needs compiler guards because it is included by SHA256 and by SHA224 */
#ifndef __SHA256_TRANSFORM_C__
#define __SHA256_TRANSFORM_C__

/** @addtogroup SHA256lowlevel
  * @{
  */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/

#define Ch(x,y,z) (z^(x&(y^z)))
#define Maj(x,y,z) ((x&y)|(z&(x|y)))
#define ROTR(n,X)  ROR(X,n)
#define SHR(n,X)   (X>>n)
#define SUM0(x)   (ROTR(2,x)^ROTR(13,x)^ROTR(22,x))
#define SUM1(x)   (ROTR(6,x)^ROTR(11,x)^ROTR(25,x))
#define SIG0(x)   (ROTR( 7,x)^ROTR(18,x)^SHR( 3,x))
#define SIG1(x)   (ROTR(17,x)^ROTR(19,x)^SHR(10,x))


/* Private variables ---------------------------------------------------------*/

/** SHA-256 Round Constants */
const uint32_t K256[64] =
  {
    0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
    0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
    0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
    0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
    0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
    0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
    0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
    0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
    0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
    0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
    0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
    0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
    0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
    0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
    0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
    0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
  };
/* Private function prototypes -----------------------------------------------*/

static void SHA256Transform(uint32_t* state, const uint8_t* buffer);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Core funcition to hash a 512-bit block
  * @note   Static function
  * @param[in,out]  *state  The array containing the SHA256 context 
  * @param[in]      *buffer The 512-bit data block that will be hashed 
  */
static void SHA256Transform(uint32_t* state, const uint8_t* buffer)
{
  uint32_t a, b, c, d, e, f, g, h, T1, T2;
  uint32_t Wi[64];
  int32_t t;


#ifdef LOCKING_MECHANISM 
  volatile uint32_t CryptoAlgo_Value = 0x00;
#endif

  Wi[0] =  LE_CONVERT_W32(*((const uint32_t *)     buffer));
  Wi[1] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 1));
  Wi[2] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 2));
  Wi[3] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 3));
  Wi[4] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 4));
  Wi[5] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 5));
  Wi[6] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 6));
  Wi[7] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 7));
  Wi[8] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 8));
  Wi[9] =  LE_CONVERT_W32(*((const uint32_t *)     buffer + 9));
  Wi[10] = LE_CONVERT_W32(*((const uint32_t *)     buffer + 10));
  Wi[11] = LE_CONVERT_W32(*((const uint32_t *)     buffer + 11));
  Wi[12] = LE_CONVERT_W32(*((const uint32_t *)     buffer + 12));
  Wi[13] = LE_CONVERT_W32(*((const uint32_t *)     buffer + 13));
  Wi[14] = LE_CONVERT_W32(*((const uint32_t *)     buffer + 14));
  Wi[15] = LE_CONVERT_W32(*((const uint32_t *)     buffer + 15));

  for (t = 16;t < 64;t += 2)
  {
    Wi[t] = SIG1(Wi[t-2]) + Wi[t-7] + SIG0(Wi[t-15]) + Wi[t-16];
    Wi[t+1] = SIG1(Wi[t-1]) + Wi[t-6] + SIG0(Wi[t-14]) + Wi[t-15];
  }

  a = state[0];
  e = state[4];
  b = state[1];
  f = state[5];
  c = state[2];
  g = state[6];
  d = state[3];
  h = state[7];

#ifdef LOCKING_MECHANISM
  * (volatile unsigned int *) 0x40023008 = 0x01  ;
  CryptoAlgo_Value = * (volatile unsigned int *) 0x40023008;

  CryptoAlgo_Value = * (volatile uint32_t *) 0x40023000;

  if (CryptoAlgo_Value == 0xFFFFFFFF)
  {
    *(volatile uint32_t *) 0x40023000 = CryptoAlgo_Value;

    CryptoAlgo_Value = * (volatile uint32_t *) 0x40023000;

    if (CryptoAlgo_Value == 0x00000000)
    {}
    else
    {
      state[0] = state[0] ^ 0xA5A5F0F0 ;
      state[4] = state[4] ^ 0xA5A5F0F0 ;
      a = state[7] ^ state[4];
      b = state[6] ^ state[3];
      c = state[5] ^ state[2];
      d = state[4] ^ state[1];
      e = state[3] ^ state[0];
      f = state[2] ^ state[7];
      g = state[1] ^ state[6];
      h = state[0] ^ state[5];

    }
  }
  else
  {
    state[0] = state[0] ^ 0xA5A5F0F0 ;
    state[4] = state[4] ^ 0xA5A5F0F0 ;
    a = state[7] ^ state[4];
    b = state[6] ^ state[3];
    c = state[5] ^ state[2];
    d = state[4] ^ state[1];
    e = state[3] ^ state[0];
    f = state[2] ^ state[7];
    g = state[1] ^ state[6];
    h = state[0] ^ state[5];

  }
#endif

  for (t = 0; t < 64; t += 2)
  {
    T1 = h + SUM1(e) + Ch(e, f, g) + K256[t] + Wi[t];
    T2 = SUM0(a) + Maj(a, b, c);
    h = g;
    g = f;
    f = e;
    e = d + T1;
    d = c;
    c = b;
    b = a;
    a = T1 + T2;
    T1 = h + SUM1(e) + Ch(e, f, g) + K256[t+1] + Wi[t+1];
    T2 = SUM0(a) + Maj(a, b, c);
    h = g;
    g = f;
    f = e;
    e = d + T1;
    d = c;
    c = b;
    b = a;
    a = T1 + T2;
  }

  state[0] = state[0] + a;
  state[1] = state[1] + b;
  state[2] = state[2] + c;
  state[3] = state[3] + d;
  state[4] = state[4] + e;
  state[5] = state[5] + f;
  state[6] = state[6] + g;
  state[7] = state[7] + h;
}

/**
  * @brief  Data process function using SHA-256 (or SHA-244), updates the HASHcontext
  * @note   The data processing function is the same for SHA-224 and SHA-256
  * @param[in,out]  *P_pSHA256ctx     The SHA256ctx_stt context that will be updated 
  * @param[in]  *P_pInput       The input data that will be hashed 
  * @param[in]  P_inputSize    The length in bytes of P_pInput 
  */
void SHA256Update(HASHctx_stt* P_pSHA256ctx, const uint8_t* P_pInput, uint32_t P_inputSize)
{
  uint32_t i, j;
  j = (P_pSHA256ctx->amCount[0] >> 3) & 63;      /*63 = 111111*/
  /* Adds to total bit count, the value here processed */
  P_pSHA256ctx->amCount[0] += P_inputSize << 3;
  /* check it didn't overflow */
  if ( P_pSHA256ctx->amCount[0] < (P_inputSize << 3))
  {
    P_pSHA256ctx->amCount[1]++;
  }
  P_pSHA256ctx->amCount[1] += (P_inputSize >> 29);
  if ((j + P_inputSize) > 63u)
  {
    memcpy(&P_pSHA256ctx->amBuffer[j], P_pInput, (i = 64u - j));
    SHA256Transform(P_pSHA256ctx->amState, P_pSHA256ctx->amBuffer);
    for ( ; i + 63u < P_inputSize; i += 64u)
    {
      /* This trick is done to check if the buffer is addressable */
      /* The minus zero is done to avoid problems with the const */
      if ( ( (&P_pInput[i] - (const uint8_t *) 0) & 3) != 0 )
      {
        /* data are properly aligned */
        SHA256Transform(P_pSHA256ctx->amState, &P_pInput[i]);
      }
      else
      {
        /* not aligned */
        memcpy(&P_pSHA256ctx->amBuffer, &P_pInput[i], 64u);
        SHA256Transform(P_pSHA256ctx->amState, P_pSHA256ctx->amBuffer);
      }
    }
    j = 0u;
  }
  else
  {
    i = 0u;
  }
  memcpy(&P_pSHA256ctx->amBuffer[j], &P_pInput[i], (size_t) (P_inputSize - i));
}


#undef ROTR
#undef SHR
#undef SUM0
#undef SUM1
#undef SIG0
#undef SIG1
#undef Ch
#undef Maj

#endif /* __SHA256_TRANSFORM_C__ */

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
