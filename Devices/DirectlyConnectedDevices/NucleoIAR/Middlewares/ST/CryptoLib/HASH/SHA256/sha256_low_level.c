/**
  ******************************************************************************
  * @file    sha256_low_level.c
  * @author  MCD Application Team
  * @version V2.1
  * @date    22-June-2012
  * @brief   SHA-256 core functions
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/ 
#include <string.h>
#include "sha256_low_level.h"

/** @ingroup InternalAPI
  * @{
  */

/** @addtogroup HASHlowlevel HASH
  * @{
  */

/** @defgroup SHA256lowlevel SHA256
  * @{
  */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Initialize a new SHA-256 context
  * @param[out]  *P_pSHA256ctx The SHA256ctx_stt context that will be initialized 
  */
void SHA256Init(SHA256ctx_stt* P_pSHA256ctx)
{
  /* SHA256 initialization constants */
  P_pSHA256ctx->amState[0] = 0x6a09e667u;
  P_pSHA256ctx->amState[1] = 0xbb67ae85u;
  P_pSHA256ctx->amState[2] = 0x3c6ef372u;
  P_pSHA256ctx->amState[3] = 0xa54ff53au;
  P_pSHA256ctx->amState[4] = 0x510e527fu;
  P_pSHA256ctx->amState[5] = 0x9b05688cu;
  P_pSHA256ctx->amState[6] = 0x1f83d9abu;
  P_pSHA256ctx->amState[7] = 0x5be0cd19u;
  P_pSHA256ctx->amCount[0] = 0u;
  P_pSHA256ctx->amCount[1] = 0u;
}

/**
  * @brief  Finalize the hash and return the SHA-256 digest
  * @param[out] *P_pDigest   The output SHA-256 digest 
  * @param[in]  *P_pSHA256ctx  The SHA256ctx_stt context that will be used to finalize 
  *         the hash operation 
  */
void SHA256Final(SHA256ctx_stt* P_pSHA256ctx, uint8_t *P_pDigest)
{ /*(Change1) */
  int32_t i;
  uint8_t finalcount[8];
  uint64_t tot_bytes;

  for (i = 0; i < 8; i++)
  {
    /* Endian independent */
    finalcount[i] = (uint8_t)((P_pSHA256ctx->amCount[(i >= 4 ? 0 : 1)] >> ((3 - (i & 3)) * 8) ) & 0xFFu);
  }


  /* Counts the message byte length */
  tot_bytes = (0x0000000000000000) | (((uint64_t)(P_pSHA256ctx->amCount[1])) << 32) | (uint64_t)(P_pSHA256ctx->amCount[0]);
  tot_bytes = tot_bytes / (uint64_t)8;

  if ( (tot_bytes & 63u) < 56u )
  { /* Only one final block; padding in this final block */

    P_pSHA256ctx->amBuffer[(tot_bytes & 63u)] = 0x80u;
    for (i = (((int32_t)(tot_bytes & 63u)) + 1); i < 56; i++)
    {
      P_pSHA256ctx->amBuffer[i] = 0x00u;
    }

    for (i = 0; i < 8; i++)  /* Add message bit length */
    {
      P_pSHA256ctx->amBuffer[56 + i] = finalcount[i];
    }
    SHA256Transform(P_pSHA256ctx->amState, P_pSHA256ctx->amBuffer);
  }
  else
  { /* Two final blocks; padding goes through this two blocks */
    P_pSHA256ctx->amBuffer[(tot_bytes & 63u)] = 0x80u;
    for (i = (((int32_t)(tot_bytes & 63u)) + 1); i < 64; i++)
    {
      P_pSHA256ctx->amBuffer[i] = 0x00u;
    }

    SHA256Transform(P_pSHA256ctx->amState, P_pSHA256ctx->amBuffer);

    for (i = 0; i < 56; i++)
    {
      P_pSHA256ctx->amBuffer[i] = 0x00u;
    }

    for (i = 0; i < 8; i++)  /* Add message bit length */
    {
      P_pSHA256ctx->amBuffer[56 + i] = finalcount[i];
    }
    SHA256Transform(P_pSHA256ctx->amState, P_pSHA256ctx->amBuffer);
  }

  /* Copies the final P_pDigest  */
  for (i = 0; i < 8; ++i)
  {
    W32_2_BUFFER(P_pDigest, i*4, P_pSHA256ctx->amState[i]);
  }


  /* Wipe variables */
  i = 0;
  memset(P_pSHA256ctx->amBuffer, 0, (size_t) 64);
  memset(P_pSHA256ctx->amState,  0, (size_t) 32);
  memset(P_pSHA256ctx->amCount,  0, (size_t) 8);
  memset(&finalcount   ,  0, (size_t) 8);
}

/**
  * @}
  */

/**
  * @}
  */
/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
