/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFIACKME_H__
#define __WIFIACKME_H__  
  
#include "cube_hal.h"
#include "serial_protocol.h"
#include "WiFiACKMe_CMD.h"

#define SSID    "h2u_Guest"
#define PASSKEY "h2uguest2015"

typedef enum {STARTING, SET_SSID, SET_PASSKEY, SET_NETWORK_UP, 
  #ifdef TLS
    GET_TIME, GET_TIME_UTC, 
  #endif
    XEND_DATA} WiFiState;
typedef enum {TIME_TICKS, TIME_UTC} TimeFormat;

typedef struct {
  uint8_t Header;
  uint8_t Result;
  uint16_t Len;
  uint8_t Data[TMsg_MaxLen];
} ACKMeMsg;

uint8_t startsWith(const char *pre, const char *str);
  
void WiFiACKMe_Init(void);
void WiFiACKMe_Reset(GPIO_PinState reset);

int WiFiACKMe_HandleMSG(TMsg *Msg, ACKMeMsg *AckMsg);

uint8_t WiFiACKMe_SetSSID(ACKMeMsg *AckMsg);
uint8_t WiFiACKMe_SetPASSKEY(ACKMeMsg *AckMsg);
uint8_t WiFiACKMe_SetNetworkUP(ACKMeMsg *AckMsg);
uint8_t WiFiACKMe_GetUTC(ACKMeMsg *AckMsg, TimeFormat format);
uint8_t WiFiACKMe_PrepareHttpPost(ACKMeMsg *AckMsg, char *uri, char *type);
uint8_t WiFiACKMe_TcpClient(ACKMeMsg *AckMsg, char *ip, int port);
uint8_t WiFiACKMe_TlsClient(ACKMeMsg *AckMsg, char *ip, int port);
uint8_t WiFiACKMe_StreamWrite(ACKMeMsg *AckMsg, int stream, char *data);
uint8_t WiFiACKMe_StreamClose(ACKMeMsg *AckMsg, int stream);

#endif /* __WIFIACKME_H__ */
