/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFIACKME_CMD_H
#define __WIFIACKME_CMD_H

/* Exported constants --------------------------------------------------------*/   
#define START_MSG_STR       "WiConnect-"

/**********  RESPONSE  **********/
#define RSP_CMD_OK                                                              0x00
    
/**********  WLAN COMMANDS **********/
#define STR_SetSSID								"set wl s"
#define CMD_SetSSID                                                             0x01        
#define STR_SetPASSKEY  						        "set wl p"
#define CMD_SetPASSKEY                                                          0x02
#define STR_SetNetworkUP							"nup"
#define CMD_SetNetworkUP                                                        0x03
#define STR_HttpPOST                                                            "hpo"
#define CMD_PrepHttpPOST                                                        0x04
#define STR_TcpClient                                                           "tcpc"
#define CMD_TcpClient                                                           0x05
#define STR_TlsClient                                                           "tlsc"
#define CMD_TlsClient                                                           0x06
#define STR_StreamWrite                                                         "write"
#define CMD_StreamWrite                                                         0x07
#define STR_StreamClose                                                         "close"
#define CMD_StreamClose                                                         0x08
#define STR_GetTIMETICKS                                                        "get ti r"
#define CMD_GetTIMETICKS                                                        0x09
#define STR_GetTIMEUTC                                                          "get ti r utc"
#define CMD_GetTIMEUTC                                                          0x0a
/****************************************************/

#endif /* __WIFIACKME_CMD_H */