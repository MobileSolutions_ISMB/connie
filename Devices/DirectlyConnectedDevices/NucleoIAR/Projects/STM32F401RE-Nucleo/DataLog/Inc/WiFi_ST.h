/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFI_ST_H__
#define __WIFI_ST_H__  

#include "cube_hal.h"
#include "serial_protocol.h"

#define SSID    "sensortocloud"
#define PASSKEY "stm32mcu"

#define RSP_CMD_OK                                                              0x00
#define RSP_CMD_ERR                                                             0x01
#define RSP_CMD_WIND                                                            0x02        

typedef enum {STARTING, ASSOCIATING, WIFI_UP, 
    GET_TIME, GET_TIME_BYTES, 
    XEND_DATA, DATA_SENT} WiFiState;

typedef struct {
  uint8_t wind_type;
  uint8_t message[128];
  uint8_t extMessage[128];
} WindMsg;

void WiFi_ST_Init(void);
void WiFi_ST_Reset(GPIO_PinState reset);
uint8_t startsWith(const char *pre, const char *str);
int8_t GetResponse(TMsg *RetMsg);
int8_t ParseWindMsg(TMsg *Msg, WindMsg *WMsg);

void WiFi_ST_Associate(char *ssid, char *pass);
uint8_t WiFi_ST_OpenSocket(char *address, uint16_t port, uint8_t secure_mode, int8_t *sockId);
uint8_t WiFi_ST_CloseSocket(uint8_t sockId);
uint8_t WiFi_ST_WriteSocket(uint8_t sockId, uint16_t length, char *toSend);
uint8_t WiFi_ST_ReadSocket(uint8_t sockId, uint16_t length, TMsg *RetMsg);
uint8_t WiFi_ST_QueryPending(uint8_t sockId, uint16_t *queuedBytes);

#endif