/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __WIFIACKME_CMD_H
#define __WIFIACKME_CMD_H

/* Exported constants --------------------------------------------------------*/   
#define WIND_CONSOLE_ACTIVE             0
#define WIND_POWERON                    1
#define WIND_RESET                      2
#define WIND_WATCHDOG_RUNNING           3
#define WIND_HEAP_SMALL                 4
#define WIND_WIFI_HARD_FAIL             5
#define WIND_WATCHDOG_TERMINATED        6
#define WIND_SYSTICKCONF                7
#define WIND_HARD_FAULT                 8
#define WIND_STACK_OVERFLOW             9
#define WIND_MALLOC_FAIL                10
#define WIND_RADIO_ERROR                11
#define WIND_PS_FAIL                    12
#define WIND_COPYRIGHT                  13
#define WIND_BSS_REGAINED               14
#define WIND_SIGNAL_LOW                 15
#define WIND_SIGNAL_OK                  16
#define WIND_FW_UPDATE                  17
#define WIND_KEYTYPE_NIMPL              18
#define WIND_WIFI_JOIN                  19
#define WIND_JOIN_FAIL                  20
#define WIND_SCANNING                   21
#define WIND_SCANBLEWUP                 22
#define WIND_SCAN_FAIL                  23
#define WIND_WIFI_UP                    24
#define WIND_WIFI_ASSOCIATED            25
//...
#define WIND_HW_STARTED                 32
#define WIND_NET_LOST                   33
#define WIND_UNHANDLED_EVENT            34
#define WIND_SCAN_COMPLETE              35     
//...
#define WIND_CRUNCH_WPA                 46
//...
#define WIND_PENDING_DATA               55
#define WIND_INSERT_MESSAGE             56
#define WIND_DATA                       57
#define WIND_SOCKET_CLOSED              58

/**********  RESPONSE  **********/
#define RSP_CMD_OK                                                              0x00
    
/**********  WLAN COMMANDS **********/
#define STR_SetSSID								"set wl s"
#define CMD_SetSSID                                                             0x01        
#define STR_SetPASSKEY  						        "set wl p"
#define CMD_SetPASSKEY                                                          0x02
#define STR_SetNetworkUP							"nup"
#define CMD_SetNetworkUP                                                        0x03
#define STR_HttpPOST                                                            "hpo"
#define CMD_PrepHttpPOST                                                        0x04
#define STR_TcpClient                                                           "tcpc"
#define CMD_TcpClient                                                           0x05
#define STR_TlsClient                                                           "tlsc"
#define CMD_TlsClient                                                           0x06
#define STR_StreamWrite                                                         "write"
#define CMD_StreamWrite                                                         0x07
#define STR_StreamClose                                                         "close"
#define CMD_StreamClose                                                         0x08
#define STR_GetTIMETICKS                                                        "get ti r"
#define CMD_GetTIMETICKS                                                        0x09
#define STR_GetTIMEUTC                                                          "get ti r utc"
#define CMD_GetTIMEUTC                                                          0x0a
/****************************************************/

#endif /* __WIFIACKME_CMD_H */