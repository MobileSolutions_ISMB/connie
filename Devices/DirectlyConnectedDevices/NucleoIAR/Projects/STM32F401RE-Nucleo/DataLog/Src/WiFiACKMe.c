/* Includes ------------------------------------------------------------------*/
#include "WiFiACKMe.h"
#include "WiFIACKMe_CMD.h"
#include "main.h"
#include "com.h"
#include <string.h>
#include <stdlib.h>

/* Extern variables ----------------------------------------------------------*/
extern volatile uint32_t DataTxPeriod;

/* Private variables ---------------------------------------------------------*/
static GPIO_InitTypeDef  GPIO_InitStruct;

TMsg Msg;
char buffer[1024];

/* Private prototypes ---------------------------------------------------------*/
uint8_t getResponse(ACKMeMsg *Msg);

void WiFiACKMe_Init() {
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct); 
}

void WiFiACKMe_Reset(GPIO_PinState reset) {
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, reset);
}

uint8_t startsWith(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre);
    size_t lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}

/**
  * @brief  Build Set SSID Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_SetSSID(ACKMeMsg *AckMsg)
{
    sprintf(buffer, "%s \"%s\"\r\n", STR_SetSSID, SSID);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Set PASSKEY Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_SetPASSKEY(ACKMeMsg *AckMsg)
{
    sprintf(buffer, "%s \"%s\"\r\n", STR_SetPASSKEY, PASSKEY);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Network UP Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_SetNetworkUP(ACKMeMsg *AckMsg)
{
    sprintf(buffer, "%s\r\n", STR_SetNetworkUP);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Get UTC From Internet Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_GetUTC(ACKMeMsg *AckMsg, TimeFormat format)
{
    switch(format) {
        case TIME_TICKS:  
            sprintf(buffer, "%s\r\n", STR_GetTIMETICKS);
            break;
        case TIME_UTC:
          sprintf(buffer, "%s\r\n", STR_GetTIMEUTC);
            break;
    }
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Http Post Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_PrepareHttpPost(ACKMeMsg *AckMsg, char *uri, char *type)
{
    sprintf(buffer, "%s -o %s %s\r\n", STR_HttpPOST, uri, type);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Tcp Client Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_TcpClient(ACKMeMsg *AckMsg, char *ip, int port)
{
    sprintf(buffer, "%s %s %d\r\n", STR_TcpClient, ip, port);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    //HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Tls Client Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_TlsClient(ACKMeMsg *AckMsg, char *ip, int port)
{
    sprintf(buffer, "%s %s %d\r\n", STR_TlsClient, ip, port);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    //HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Stream_write Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_StreamWrite(ACKMeMsg *AckMsg, int stream, char *data)
{
    sprintf(buffer, "%s %d %d\r\n", STR_StreamWrite, stream, strlen(data));
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    //HAL_Delay(DataTxPeriod);
    strcpy((char *)Msg.Data, data);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    //HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

/**
  * @brief  Build Stream_write Command
  * @param  Msg the pointer to the message to be built
  * @retval None 
  */
uint8_t WiFiACKMe_StreamClose(ACKMeMsg *AckMsg, int stream)
{
    sprintf(buffer, "%s %d\r\n", STR_StreamClose, stream);
    strcpy((char *)Msg.Data, buffer);
    Msg.Len = strlen((char *)Msg.Data);
    UART_SendMsg(&Msg);
    //HAL_Delay(DataTxPeriod);
    return getResponse(AckMsg);
}

uint8_t getResponse(ACKMeMsg *AckMsg) {
  uint8_t res;
  do {
    res = UART_ReceivedMSG(&Msg);
    if(res == 1) {          // Read Serial for messages
      if(WiFiACKMe_HandleMSG(&Msg, AckMsg) != -1) {
        if(AckMsg->Len != 0) {                      // There is a Response data to read
          while(UART_ReceivedMSG(&Msg)!= 1)
            ;
          strcpy((char *)AckMsg->Data, (char *)Msg.Data);
        }
        return AckMsg->Result;
      }
    }
  } while ( res != 1 );
  return -1;
}

/**
  * @brief  Handle a message 
  * @param  Msg the pointer to the message to be handled
  * @retval 0 if the message is correctly handled, -1 otherwise
  */
int WiFiACKMe_HandleMSG(TMsg *Msg, ACKMeMsg *AckMsg)
{    
    if (Msg->Len==7 && Msg->Data[0] == 'R') {
        // Return Code
        AckMsg->Header = 'R';
        AckMsg->Result = Msg->Data[1]- '0';
        AckMsg->Len = atoi((char *)&Msg->Data[2])-2;
        return RSP_CMD_OK;
    }
    return -1;
}