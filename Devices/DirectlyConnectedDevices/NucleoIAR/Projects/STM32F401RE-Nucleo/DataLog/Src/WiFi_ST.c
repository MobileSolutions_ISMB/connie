/* Includes ------------------------------------------------------------------*/
#include "WiFi_ST.h"
#include "WiFi_ST_CMD.h"
#include "main.h"
#include "com.h"
#include <string.h>
#include <stdlib.h>

/* Extern variables ----------------------------------------------------------*/
extern volatile uint32_t DataTxPeriod;

/* Private variables ---------------------------------------------------------*/
static GPIO_InitTypeDef  GPIO_InitStruct;

char buffer[1024];

void WiFi_ST_Init() {
    __HAL_RCC_GPIOC_CLK_ENABLE();

    GPIO_InitStruct.Pin = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct); 
}

void WiFi_ST_Reset(GPIO_PinState reset) {
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_7, reset);
}

uint8_t startsWith(const char *pre, const char *str)
{
    size_t lenpre = strlen(pre);
    size_t lenstr = strlen(str);
    return lenstr < lenpre ? 0 : strncmp(pre, str, lenpre) == 0;
}
    
int8_t GetResponse(TMsg *RetMsg) {
  TMsg Msg;
  do {
    if(UART_ReceivedMSG((TMsg*) &Msg) && Msg.Len > 0) {
      // Partial Response received:
      if(startsWith(" ID:", (char *)Msg.Data) || startsWith(" DATALEN:", (char *)Msg.Data)) {
        strcpy((char *)(RetMsg->Data), (char *)Msg.Data); RetMsg->Len = strlen((char *)Msg.Data);
      }
      else if(startsWith("OK", (char *)Msg.Data)) {
        return RSP_CMD_OK;
      }
      else if(startsWith("ERROR: Illegal Socket ID", (char *)Msg.Data)) {
        return RSP_CMD_ERR;
      }
      else if(startsWith("ERROR", (char *)Msg.Data)) {
        strcpy((char *)(RetMsg->Data), (char *)Msg.Data); RetMsg->Len = strlen((char *)Msg.Data);
        return RSP_CMD_ERR;
      }
      else if(startsWith("+WIND", (char *)Msg.Data)) {
        WindMsg WMsg;
        ParseWindMsg(&Msg, &WMsg);
        sprintf((char *) (RetMsg->Data), "%d", WMsg.wind_type); RetMsg->Len = strlen((char *)RetMsg->Data);
        return RSP_CMD_WIND;
      }
    }
  } while(1);
}
    
int8_t ParseWindMsg(TMsg *Msg, WindMsg *WMsg) {
  // Check if message received is a valid WIND:
  if(Msg->Data[0] == '+') {
    char *token;
    token = strtok((char *)(Msg->Data), ":");
    if(strcmp("+WIND", token)==0) {
      token = strtok(NULL, ":");        // Get WIND Number
      uint8_t windCode = atoi(token);
      WMsg->wind_type = windCode;
      token = strtok(NULL, ":");        // Get WIND Message        
      strcpy((char *)(WMsg->message), token);
      token = strtok(NULL, "\r");        // Get Extended WIND Message
      if(token != NULL) {
        strcpy((char *)(WMsg->extMessage), token);
      } else {
        WMsg->extMessage[0] = 0;
      }
      return windCode;
    }    
  } 
  else if(startsWith("ERROR", (char *)(Msg->Data))) {
    strcpy((char *)(WMsg->message), "ERROR");
    strcpy((char *)(WMsg->extMessage), (char *)(Msg->Data+ strlen("ERROR: ")));   
    return -1;
  }
  
  return -2;
}

void WiFi_ST_Associate(char *ssid, char *pass) {
  TMsg Msg;
  
  // AT+S.SCFG=wifi_mode,0
  strcpy((char *)Msg.Data, "AT+S.SCFG=wifi_mode,0\r"); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  
  // AT+S.SCFG=wifi_priv_mode,2
  strcpy((char *)Msg.Data, "AT+S.SCFG=wifi_priv_mode,2\r"); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  
  // AT+S.SSIDTXT=<SSID>
  sprintf((char *)Msg.Data, "AT+S.SSIDTXT=%s\r", ssid); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  
  // AT+S.SCFG=wifi_wpa_psk_text,<WPA PSK passphrase>
  sprintf((char *)Msg.Data, "AT+S.SCFG=wifi_wpa_psk_text,%s\r", pass); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  
  // AT+S.SCFG=wifi_mode,1
  strcpy((char *)Msg.Data, "AT+S.SCFG=wifi_mode,1\r"); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  
  // AT&W
  strcpy((char *)Msg.Data, "AT&W\r"); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  
  // AT+CFUN=1
  strcpy((char *)Msg.Data, "AT+CFUN=1\r"); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
}

uint8_t WiFi_ST_OpenSocket(char *address, uint16_t port, uint8_t secure_mode, int8_t *sockId) {
  TMsg Msg;
  uint8_t retValue;
  
  sprintf((char *)Msg.Data,"AT+S.SOCKON=%s,%d,%c,ind\r",address,port, secure_mode); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  HAL_Delay(DataTxPeriod);
  
  retValue = GetResponse(&Msg);
  if(retValue == RSP_CMD_OK) {
    *sockId = atoi((char *)(Msg.Data+strlen(" ID: ")));
  }
  else if(retValue == RSP_CMD_WIND) {
    *sockId = atoi((char *)(Msg.Data));
  }

  return retValue;
}

uint8_t WiFi_ST_CloseSocket(uint8_t sockId) {
  TMsg Msg;

  sprintf((char *)Msg.Data,"AT+S.SOCKC=%d\r",sockId); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  HAL_Delay(DataTxPeriod);
  
  return GetResponse(&Msg);
}

uint8_t WiFi_ST_WriteSocket(uint8_t sockId, uint16_t length, char *toSend) {
  TMsg Msg;
  uint8_t retValue;
  
  sprintf((char *)Msg.Data,"AT+S.SOCKW=%d,%d\r", sockId, length); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  HAL_Delay(DataTxPeriod);
  
  strcpy((char *)Msg.Data, toSend); Msg.Len = length;
  UART_SendMsg(&Msg);
  HAL_Delay(DataTxPeriod);
  
  retValue = GetResponse(&Msg);
  return retValue;
}

uint8_t WiFi_ST_ReadSocket(uint8_t sockId, uint16_t length, TMsg *RetMsg) {
  TMsg Msg;
  
  sprintf((char *)Msg.Data,"AT+S.SOCKR=%d,%d\r",sockId,length); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  HAL_Delay(DataTxPeriod);
  
  // Read Raw length Bytes 
  UART_ReceiveRAW(length, RetMsg);
  
  do {
    UART_ReceivedMSG(&Msg);
  }while(!(Msg.Len ==2  && strcmp("OK", (char *)Msg.Data) == 0));

  return RSP_CMD_OK;
}

uint8_t WiFi_ST_QueryPending(uint8_t sockId, uint16_t *queuedBytes) {
  TMsg Msg;
  uint8_t retValue;
  
  sprintf((char *)Msg.Data,"AT+S.SOCKQ=%d\r",sockId); Msg.Len = strlen((char *)Msg.Data);
  UART_SendMsg(&Msg);
  HAL_Delay(DataTxPeriod);
  
  retValue = GetResponse(&Msg);
  *queuedBytes = atoi((char *)(Msg.Data+strlen(" DATALEN: ")));
  return retValue;
}