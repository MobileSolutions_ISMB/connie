/**
  ******************************************************************************
  * @file    Projects/Multi/Examples/DataLog/Src/main.c
  * @author  MEMS Application Team
  * @version V1.1.0
  * @date    30-July-2014
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/**
  * @mainpage Documentation for X-CUBE-MEMS1 Software for STM32, Expansion for STM32Cube
  *
  * @image html st_logo.png
  *
  * <b>Introduction</b>
  *
  * This software is gathering Temperature, Humidity, Pressure and Motion sensors drivers for the HTS221,
  * LPS25H, LPS25HB, LSM6DS0, LSM6DS3 and LIS3MDL devices, running on STM32. It is built on top of STM32Cube software
  * technology that ease portability across different STM32 microcontrollers.
  * 
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "com.h"
#include <string.h> // strlen
#include <stdio.h>  // sprintf
#include <math.h>   // trunc
#include <time.h>   // time functions
#include "WiFi_ST.h"
#include "WiFi_ST_CMD.h"
#include "EventHub.h"

typedef struct {
  char strTemp[10];
  char strHum[10];
} EnvSens;

/* Extern variables ----------------------------------------------------------*/
extern UART_HandleTypeDef UartHandle;

/* Private variables ---------------------------------------------------------*/
static WiFiState wifi_state = STARTING;
static WindMsg WMsg;
static EnvSens SensReading;

static int8_t TimeSocket;
static int8_t TlsSocket;

RTC_HandleTypeDef RtcHandle;
volatile uint32_t DataTxPeriod = 50;
volatile uint32_t Int_Current_Time1 = 0;
volatile uint32_t Int_Current_Time2 = 0;
static int32_t last_ticks=0;
static int32_t ticks1 = 0;
static uint8_t requesting_time = 0;

volatile float HUMIDITY_Value;
volatile float TEMPERATURE_Value;
char CurrTime[28];

/* Private function prototypes -----------------------------------------------*/
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec);
static void HumTemp_Sensor_Handler(EnvSens *sens);
static void RTC_Config(void);
static void RTC_TimeStampConfig(int32_t timeNow);
static void RTC_GetTimeStamp(char *timestr);

/* Private functions ---------------------------------------------------------*/
/**
 * @brief   Main function is to show how to use X_NUCLEO_IKS01A1 expansion board to send data from a Nucleo board
  *         using UART to a connected PC or Desktop and display it on generic applications like
  *         TeraTerm and specific application like Sensors_DataLog, which is developed by STMicroelectronics
  *         and provided with this package.
  * 
  *         After connection has been established:
  *         - the user can view the data from various on-board environment sensors like Temperature, Humidity, and Pressure
  *         - the user can also view data from various on-board MEMS sensors as well like Accelerometer, Gyrometer, and Magnetometer
  *         - the user can also visualize this data as graphs using Sensors_DataLog application provided with this package
 * @param  None
 * @retval None
 */
int main(void)
{
  
    uint8_t timeBytes;
    /* STM32F4xx HAL library initialization:
  - Configure the Flash prefetch, instruction and Data caches
  - Configure the Systick to generate an interrupt each 1 msec
  - Set NVIC Group Priority to 4
  - Global MSP (MCU Support Package) initialization
     */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();
       
    /* Init structures for Event Hub Authentication */
    EventHub_Init();
    
    /* Initialize WiFi Module Reset GPIO */
    WiFi_ST_Init();
    WiFi_ST_Reset(GPIO_PIN_RESET);    
        
    /* Initialize LEDs */
    BSP_LED_Init(LED2);
    BSP_LED_Off(LED2);

    /* Initialize Button */
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_EXTI);

    /* Initialize UART linked to WIFI*/
    USARTConfig();
    
    /* Initialize RTC */
    RTC_Config();
    
    /* Initialize Temp Sensor */
    BSP_HUM_TEMP_Init();

    /*********************************** INITIALIZATION DONE! */
    WiFi_ST_Reset(GPIO_PIN_SET);
    
    while(1) {
      TMsg Msg;
      
      if(getElapsedMSFromLastGetTime() > ((EXPIRE_GAP -1) * 60 * 1000) && !requesting_time) {
        wifi_state = WIFI_UP;
      }
      
      if(UART_ReceivedMSG((TMsg*) &Msg) && ParseWindMsg(&Msg, &WMsg) >=0) {
        switch(wifi_state) {
          case STARTING:
            if(WMsg.wind_type == WIND_SCAN_COMPLETE) {
              WiFi_ST_Associate(SSID, PASSKEY);
              wifi_state = ASSOCIATING;
            } 
            else if(WMsg.wind_type == WIND_WIFI_JOIN) {
              wifi_state = ASSOCIATING;
            }
              
            break;
            
          case ASSOCIATING:
            if(WMsg.wind_type == WIND_WIFI_UP) {
              wifi_state = WIFI_UP;
            }
            break;
            
          case GET_TIME:
            if(WMsg.wind_type == WIND_PENDING_DATA) {
              timeBytes = atoi((char *)WMsg.extMessage+2);
              wifi_state = GET_TIME_BYTES;
            }
            break;
          case GET_TIME_BYTES:
            if(WMsg.wind_type == WIND_SOCKET_CLOSED) {
              if(WiFi_ST_ReadSocket(TimeSocket, timeBytes, &Msg)== RSP_CMD_OK) {
                // Time received;
                last_ticks = ((Msg.Data[0]<<24 )|(Msg.Data[1]<<16)|(Msg.Data[2]<<8)| Msg.Data[3]) - 2208988800ul;
                RTC_TimeStampConfig(last_ticks);
                SASTokenHelper(last_ticks);
                wifi_state = XEND_DATA;
              }
            }
            break;
            
          case DATA_SENT:
            if(WMsg.wind_type == WIND_PENDING_DATA) {
              TMsg Msg;
              uint16_t queuedBytes, remaining;
              if(WiFi_ST_QueryPending(TlsSocket, &queuedBytes) == RSP_CMD_OK) {
                remaining = queuedBytes;
                
                // Read response
                do {
                  int16_t toRead = (int16_t) fmin(TMsg_MaxLen-1, queuedBytes);
                  WiFi_ST_ReadSocket(TlsSocket, toRead, &Msg);
                  remaining-=toRead;
                }while(remaining > 0);
                
                if(WiFi_ST_CloseSocket(TlsSocket)!= RSP_CMD_OK) {
                  // Unexpected response, reboot module and restart
                  strcpy((char *)Msg.Data, "AT+CFUN=1\r"); Msg.Len = strlen((char *)Msg.Data);
                  UART_SendMsg(&Msg);
                  wifi_state = STARTING;
                }
                wifi_state = XEND_DATA;                  
              }
            } else if(WMsg.wind_type == WIND_WIFI_UP) {
              wifi_state = WIFI_UP;
            } else {
              // Unexpected response, reboot module and restart
              strcpy((char *)Msg.Data, "AT+CFUN=1\r"); Msg.Len = strlen((char *)Msg.Data);
              UART_SendMsg(&Msg);
              wifi_state = STARTING;
            }
            break;
        }
      }
      else {
        switch(wifi_state) {
          case WIFI_UP:
            if(WiFi_ST_OpenSocket("time-d.nist.gov", 37, 't', &TimeSocket) == RSP_CMD_OK) {
              wifi_state = GET_TIME;
              requesting_time = 1;
            } else {
              // Unexpected response, reboot module and restart
              TMsg Msg;
              strcpy((char *)Msg.Data, "AT+CFUN=1\r"); Msg.Len = strlen((char *)Msg.Data);
              UART_SendMsg(&Msg);
              wifi_state = STARTING;
            }
            break;
            
          case XEND_DATA: {
              uint8_t retVal;
              HumTemp_Sensor_Handler((EnvSens *) &SensReading);
              
              retVal = WiFi_ST_OpenSocket(ServiceBus, ServiceBusPort, 's', &TlsSocket);
              if( retVal == RSP_CMD_OK){
                RTC_GetTimeStamp(CurrTime);
                char *body = PrepareBody(CurrTime, SensReading.strTemp);
                char *post = PreparePost(strlen(body));
                char *postSas = PrepareSAS();
                WiFi_ST_WriteSocket(TlsSocket, strlen(post), post);
                WiFi_ST_WriteSocket(TlsSocket, strlen(postSas), postSas);
                WiFi_ST_WriteSocket(TlsSocket, strlen(body), body);
                free(postSas);
                wifi_state = DATA_SENT;
              }
              else {
                // Unexpected response, reboot module and restart
                TMsg Msg;
                strcpy((char *)Msg.Data, "AT+CFUN=1\r"); Msg.Len = strlen((char *)Msg.Data);
                UART_SendMsg(&Msg);
                wifi_state = STARTING;
              }
            }
            break;
        }
      }
    }
}

/**
 * @brief  Splits a float into two integer values.
 * @param  in the float value as input
 * @param  out_int the pointer to the integer part as output
 * @param  out_dec the pointer to the decimal part as output
 * @param  dec_prec the decimal precision to be used
 * @retval None
 */
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec)
{
    *out_int = (int32_t)in;
    in = in - (float)(*out_int);
    *out_dec = (int32_t)trunc(in*pow(10,dec_prec));
}

/**
 * @brief  Handles the HUM+TEMP axes data getting/sending
 * @param  Msg - HUM+TEMP part of the stream
 * @retval None
 */
static void HumTemp_Sensor_Handler(EnvSens *sens)
{
    int32_t d1, d2, d3, d4;

    if(BSP_HUM_TEMP_isInitialized()) {
        BSP_HUM_TEMP_GetHumidity((float *)&HUMIDITY_Value);
        BSP_HUM_TEMP_GetTemperature((float *)&TEMPERATURE_Value);
        floatToInt(HUMIDITY_Value, &d1, &d2, 2);
        floatToInt(TEMPERATURE_Value, &d3, &d4, 2);
        sprintf(sens->strHum, "%d.%d", d1,d2);
        sprintf(sens->strTemp, "%d.%d", d3,d4);
        /*
        if ( DataLoggerActive ) {
            if(Sensors_Enabled & TEMPERATURE_SENSOR){
                Serialize(&Msg->Data[11], d3, 1);
                Serialize(&Msg->Data[12], d4, 1);
            }
            if(Sensors_Enabled & HUMIDITY_SENSOR){
                Serialize(&Msg->Data[13], d1, 1);
                Serialize(&Msg->Data[14], d2, 1);
            }
        }
        
        else if ( AutoInit ) {
            sprintf(dataOut, "HUM: %d.%d     TEMP: %d.%d\n", (int)d1, (int)d2, (int)d3, (int)d4);
            HAL_UART_Transmit(&UartHandle, (uint8_t*)dataOut, strlen(dataOut), 5000);
        }
        */
    }

    else {
        BSP_HUM_TEMP_Init();
    }
}

/**
 * @brief  Configures the RTC
 * @param  None
 * @retval None
 */
static void RTC_Config(void)
{
    /*##-1- Configure the RTC peripheral #######################################*/
    RtcHandle.Instance = RTC;

    /* Configure RTC prescaler and RTC data registers */
    /* RTC configured as follow:
  - Hour Format    = Format 12
  - Asynch Prediv  = Value according to source clock
  - Synch Prediv   = Value according to source clock
  - OutPut         = Output Disable
  - OutPutPolarity = High Polarity
  - OutPutType     = Open Drain */
    RtcHandle.Init.HourFormat = RTC_HOURFORMAT_12;
    RtcHandle.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
    RtcHandle.Init.SynchPrediv = RTC_SYNCH_PREDIV;
    RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
    RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

    if(HAL_RTC_Init(&RtcHandle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }
}

/**
 * @brief  Configures the current time and date.
 * @param  None
 * @retval None
 */
static void RTC_TimeStampConfig(int32_t timeNow)
{
    RTC_DateTypeDef sdatestructure;
    RTC_TimeTypeDef stimestructure;

    time_t now = timeNow;
    struct tm *calendar = gmtime(&now);
    
    /*##-3- Configure the Date #################################################*/
    sdatestructure.Year = calendar->tm_year - 100;
    sdatestructure.Month = calendar->tm_mon + 1;
    sdatestructure.Date = calendar->tm_mday;
    sdatestructure.WeekDay = calendar->tm_wday + 1;

    if(HAL_RTC_SetDate(&RtcHandle,&sdatestructure,FORMAT_BIN) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }

    /*##-4- Configure the Time #################################################*/
    stimestructure.Hours = calendar->tm_hour;
    stimestructure.Minutes = calendar->tm_min;
    stimestructure.Seconds = calendar->tm_sec;
    stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
    stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
    stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

    if(HAL_RTC_SetTime(&RtcHandle,&stimestructure,FORMAT_BIN) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }
}

static void RTC_GetTimeStamp(char *timestr) {
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructure;
  
  HAL_RTC_GetTime(&RtcHandle, &stimestructure, FORMAT_BIN);
  HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, FORMAT_BIN);
  
  sprintf(timestr, "20%02d-%02d-%02dT%02d:%02d:%02d.000000Z",
     sdatestructureget.Year, sdatestructureget.Month, sdatestructureget.Date,
     stimestructure.Hours, stimestructure.Minutes, stimestructure.Seconds);
}

void HAL_GPIO_EXTI_Callback( uint16_t GPIO_Pin )
{                       
    if(GPIO_Pin == KEY_BUTTON_PIN) {
        /* Manage software debouncing*/
        int doOperation = 0;
       
        if(Int_Current_Time1 == 0 && Int_Current_Time2 == 0)
        {
            Int_Current_Time1 = user_currentTimeGetTick();
            doOperation = 1;
        }else
        {
            int i2;
            Int_Current_Time2 = user_currentTimeGetTick();
            i2 = Int_Current_Time2;
            
            /* If I receive a button interrupt after more than 300 ms from the first one I get it, otherwise I discard it */
            if((i2 - Int_Current_Time1)  > 300)
            {
                Int_Current_Time1 = Int_Current_Time2;
                doOperation = 1;
            }
        }
        
        if(doOperation)
        {
          // DO SOMETHING WHEN BUTTON PRESSED
        }
    }
}

/**
 * @brief  This function is executed in case of error occurrence
 * @param  None
 * @retval None
 */
void Error_Handler(void)
{
    while(1)
    {}
}

/**
 * @brief  Get the current tick value in millisecond
 * @param  None
 * @retval The tick value
 */
uint32_t user_currentTimeGetTick(void)
{
    return HAL_GetTick();
}

/**
 * @brief  Get the delta tick value in millisecond from Tick1 to the current tick
 * @param  Tick1 the reference tick used to compute the delta
 * @retval The delta tick value
 */
uint32_t getElapsedMSFromLastGetTime()
{
    volatile uint32_t Delta, now;

    now = HAL_GetTick();

    /* Capture computation */
    Delta = now - ticks1;
    return Delta;
}


#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *   where the assert_param error has occurred.0
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
  ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {}
}
#endif
 
/**
 * @}
 */

/**
 * @}
 */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
