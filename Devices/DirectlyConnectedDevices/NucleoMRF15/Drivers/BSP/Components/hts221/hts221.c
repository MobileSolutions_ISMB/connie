/**
 ******************************************************************************
 * @file    hts221.c
 * @author  MEMS Application Team
 * @version V1.0.0
 * @date    30-July-2014
 * @brief   This file provides a set of functions needed to manage the hts221.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "hts221.h"
#include <math.h>

/** @addtogroup BSP
 * @{
 */

/** @addtogroup STM32F439_SENSITRON
 * @{
 */

/** @addtogroup HTS221
 * @{
 */


/** @defgroup HTS221_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup HTS221_Private_Defines
 * @{
 */

/**
 * @}
 */

/** @defgroup HTS221_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup HTS221_Private_Variables
 * @{
 */

HUM_TEMP_DrvTypeDef Hts221Drv =
{
 HTS221_Init,
 HTS221_Power_OFF,
 HTS221_ReadID,
 HTS221_RebootCmd,
 0,
 0,
 0,
 0,
 0,
 HTS221_GetHumidity,
 HTS221_GetTemperature
};


/* Temperature in degree for calibration  */
float T0_degC, T1_degC;

/* Output temperature value for calibration */
int16_t T0_out, T1_out;


/* Humidity for calibration  */
float H0_rh, H1_rh;

/* Output Humidity value for calibration */
int16_t H0_T0_out, H1_T0_out;


/**
 * @}
 */

/** @defgroup HTS221_Private_FunctionPrototypes
 * @{
 */
static void HTS221_Power_On(void);

static void HTS221_Calibration(void);
/**
 * @}
 */

/** @defgroup HTS221_Private_Functions
 * @{
 */



/**
 * @brief  HTS221 Calibration procedure.
 * @param  None
 * @retval None
 */
static void HTS221_Calibration(void)
{
    /* Temperature Calibration */
    /* Temperature in degree for calibration ( "/8" to obtain float) */
    uint16_t T0_degC_x8_L, T0_degC_x8_H, T1_degC_x8_L, T1_degC_x8_H;
    uint8_t H0_rh_x2, H1_rh_x2;
    uint8_t tempReg[2] = {0,0};

    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T0_degC_X8_ADDR, 1);
    T0_degC_x8_L = (uint16_t)tempReg[0];

    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_T0_MSB_X8_ADDR, 1);
    T0_degC_x8_H = (uint16_t) (tempReg[0] & 0x03);

    T0_degC = ((float)((T0_degC_x8_H<<8) | (T0_degC_x8_L)))/8;

    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_degC_X8_ADDR, 1);
    T1_degC_x8_L = (uint16_t)tempReg[0];

    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_T0_MSB_X8_ADDR, 1);
    T1_degC_x8_H = (uint16_t) (tempReg[0] & 0x0C);
    T1_degC_x8_H = T1_degC_x8_H >> 2;

    T1_degC = ((float)((T1_degC_x8_H<<8) | (T1_degC_x8_L)))/8;

    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T0_OUT_L_ADDR + 0x80, 2);
    T0_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);

    HUM_TEMP_IO_Read(tempReg, HTS221_ADDRESS, HTS221_T1_OUT_L_ADDR + 0x80, 2);
    T1_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);

    /* Humidity Calibration */
    /* Humidity in degree for calibration ( "/2" to obtain float) */

    HUM_TEMP_IO_Read(&H0_rh_x2, HTS221_ADDRESS, HTS221_H0_RH_X2_ADDR, 1);

    HUM_TEMP_IO_Read(&H1_rh_x2, HTS221_ADDRESS, HTS221_H1_RH_X2_ADDR, 1);

    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_H0_T0_OUT_L_ADDR + 0x80, 2);
    H0_T0_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);

    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_H1_T0_OUT_L_ADDR  + 0x80, 2);
    H1_T0_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);

    H0_rh = ((float)H0_rh_x2)/2;
    H1_rh = ((float)H1_rh_x2)/2;
}


/**
 * @brief  Set HTS221 Initialization.
 * @param  InitStruct: it contains the configuration setting for the HTS221.
 * @retval None
 */
void HTS221_Init(HUM_TEMP_InitTypeDef *HTS221_Init)
{  
    uint8_t tmp = 0x00;

    /* Configure the low level interface ---------------------------------------*/
    HUM_TEMP_IO_Init();

    HTS221_Power_On();

    HTS221_Calibration();

    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);

    /* Output Data Rate selection */
    tmp &= ~(HTS221_ODR_MASK);
    tmp |= HTS221_Init->OutputDataRate;

    HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
}

/**
 * @brief  Read ID address of HTS221
 * @param  Device ID address
 * @retval ID name
 */
uint8_t HTS221_ReadID(void)
{
    uint8_t tmp;

    /* Read WHO I AM register */
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_WHO_AM_I_ADDR, 1);

    /* Return the ID */
    return (uint8_t)tmp;
}

/**
 * @brief  Reboot memory content of HTS221
 * @param  None
 * @retval None
 */
void HTS221_RebootCmd(void)
{
    uint8_t tmpreg;

    /* Read CTRL_REG2 register */
    HUM_TEMP_IO_Read(&tmpreg, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);

    /* Enable or Disable the reboot memory */
    tmpreg |= HTS221_BOOT_REBOOTMEMORY;

    /* Write value to MEMS CTRL_REG2 regsister */
    HUM_TEMP_IO_Write(&tmpreg, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
}


/**
 * @brief  Read HTS221 output register, and calculate the humidity.
 * @param  pfData : Data out pointer
 * @retval None
 */
void HTS221_GetHumidity(float* pfData)
{
    int16_t H_T_out, humidity_t;
    uint8_t tempReg[2] = {0,0};
    uint8_t tmp = 0x00;
    float H_rh;
    
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);

    /* Output Data Rate selection */
    tmp &= (HTS221_ODR_MASK);
    
    if(tmp == 0x00){
    
      HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);

      /* Serial Interface Mode selection */
      tmp &= ~(HTS221_ONE_SHOT_MASK);
      tmp |= HTS221_ONE_SHOT_START;

      HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
    
      do{
      
        HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_STATUS_REG_ADDR, 1);
         
      }while(!(tmp&&0x02));
    
    }
    
    
    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_HUMIDITY_OUT_L_ADDR + 0x80, 2);
    H_T_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);

    H_rh = ((float)(H_T_out - H0_T0_out))/(H1_T0_out - H0_T0_out) * (H1_rh - H0_rh) + H0_rh;

    humidity_t = (uint16_t)(H_rh * pow(10,HUM_DECIMAL_DIGITS));

    *pfData = ((float)humidity_t)/pow(10,HUM_DECIMAL_DIGITS);
}

/**
 * @brief  Read HTS221 output register, and calculate the temperature.
 * @param  pfData : Data out pointer
 * @retval None
 */
void HTS221_GetTemperature(float* pfData)
{
    int16_t T_out, temperature_t;
    uint8_t tempReg[2] = {0,0};
    uint8_t tmp = 0x00;
    float T_degC;
    
    HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);

    /* Output Data Rate selection */
    tmp &= (HTS221_ODR_MASK);
    
    if(tmp == 0x00){
    
      HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);

      /* Serial Interface Mode selection */
      tmp &= ~(HTS221_ONE_SHOT_MASK);
      tmp |= HTS221_ONE_SHOT_START;

      HUM_TEMP_IO_Write(&tmp, HTS221_ADDRESS, HTS221_CTRL_REG2_ADDR, 1);
    
      do{
      
        HUM_TEMP_IO_Read(&tmp, HTS221_ADDRESS, HTS221_STATUS_REG_ADDR, 1);
       
      }while(!(tmp&&0x01));
    
    }

    HUM_TEMP_IO_Read(&tempReg[0], HTS221_ADDRESS, HTS221_TEMP_OUT_L_ADDR + 0x80, 2);
    T_out = ((((int16_t)tempReg[1]) << 8)+(int16_t)tempReg[0]);
    
 //   T_out = tempReg[1] << 8 | tempReg[0];

    T_degC = ((float)(T_out - T0_out))/(T1_out - T0_out) * (T1_degC - T0_degC) + T0_degC;

    temperature_t = (int16_t)(T_degC * pow(10,TEMP_DECIMAL_DIGITS));

    *pfData = ((float)temperature_t)/pow(10,TEMP_DECIMAL_DIGITS);
}


/**
 * @brief  Exit the shutdown mode for HTS221.
 * @retval None
 */
static void HTS221_Power_On()
{
    uint8_t tmpReg;

    /* Read the register content */
    HUM_TEMP_IO_Read(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);

    /* Set the power down bit */
    tmpReg |= HTS221_MODE_ACTIVE;

    /* Write register */
    HUM_TEMP_IO_Write(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
}

/**
 * @brief  Enter the shutdown mode for HTS221.
 * @retval None
 */
void HTS221_Power_OFF()
{
    uint8_t tmpReg;

    /* Read the register content */
    HUM_TEMP_IO_Read(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);

    /* Reset the power down bit */
    tmpReg &= ~(HTS221_MODE_ACTIVE);

    /* Write register */
    HUM_TEMP_IO_Write(&tmpReg, HTS221_ADDRESS, HTS221_CTRL_REG1_ADDR, 1);
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/     
