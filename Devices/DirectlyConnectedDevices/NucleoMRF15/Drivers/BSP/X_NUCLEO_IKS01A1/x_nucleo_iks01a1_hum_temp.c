/**
 ******************************************************************************
 * @file    x_nucleo_iks01a1_hum_temp.c
 * @author  MEMS Application Team
 * @version V1.0.0
 * @date    30-July-2014
 * @brief   This file provides a set of functions needed to manage the hts221 sensor.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "x_nucleo_iks01a1_hum_temp.h"

/** @addtogroup BSP
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1_HUM_TEMP
 * @{
 */


/** @defgroup X_NUCLEO_IKS01A1_HUM_TEMP_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_HUM_TEMP_Private_Defines
 * @{
 */
#ifndef NULL
  #define NULL      (void *) 0
#endif
/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_HUM_TEMP_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_HUM_TEMP_Private_Variables
 * @{
 */
static HUM_TEMP_DrvTypeDef *Hum_tempDrv;
static uint8_t HumTempInitialized = 0;
/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_HUM_TEMP_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_HUM_TEMP_Private_Functions
 * @{
 */

/**
 * @brief  Set HUM_TEMP Initialization.
 * @param  None
 * @retval HUM_TEMP_OK if no problem during initialization
 */
HUM_TEMP_StatusTypeDef BSP_HUM_TEMP_Init(void)
{  
    HUM_TEMP_StatusTypeDef ret = HUM_TEMP_ERROR;
//    uint16_t ctrl = 0x0000;
    HUM_TEMP_InitTypeDef HTS221_InitStructure;

    /* Initialize the hum_temp driver structure */
    Hum_tempDrv = &Hts221Drv;

    /* Configure sensor */
    HTS221_InitStructure.OutputDataRate = HTS221_ODR_12_5Hz;

    /* Hts221 Init */   
    Hum_tempDrv->Init(&HTS221_InitStructure);

    if(Hum_tempDrv->ReadID() == I_AM_HTS221)
    {
        HumTempInitialized = 1;
        ret = HUM_TEMP_OK;
    }

    return ret;
}

uint8_t BSP_HUM_TEMP_isInitialized(void)
{
    return HumTempInitialized;
}

/**
 * @brief  Read ID of hum_temp component
 * @param  None
 * @retval ID
 */
uint8_t BSP_HUM_TEMP_ReadID(void)
{
    uint8_t id = 0x00;

    if(Hum_tempDrv->ReadID != NULL)
    {
        id = Hum_tempDrv->ReadID();
    }
    return id;
}


/**
 * @brief  Check ID of HTS221 HumTemp sensor
 * @param  None
 * @retval Test status
 */
HUM_TEMP_StatusTypeDef BSP_HUM_TEMP_CheckID(void) {
    if (BSP_HUM_TEMP_ReadID() == I_AM_HTS221) {
        return HUM_TEMP_OK;
    } else {
        return HUM_TEMP_ERROR;
    }
}


/**
 * @brief  Reboot memory content of hum_temp
 * @param  None
 * @retval None
 */
void BSP_HUM_TEMP_Reset(void)
{  
    if(Hum_tempDrv->Reset != NULL)
    {
        Hum_tempDrv->Reset();
    }
}


/**
 * @brief Power OFF HUM_TEMP sensor.
 * @param pfData: pointer on floating data
 * @retval None
 */
void BSP_HUM_TEMP_PowerOFF()
{
    if(Hum_tempDrv->PowerOFF!= NULL)
    {
        Hum_tempDrv->PowerOFF();
    }
}


/**
 * @brief  Get Humidity value
 * @param pfData: pointer on floating data
 * @retval None
 */
void BSP_HUM_TEMP_GetHumidity(float* pfData)
{
    if(Hum_tempDrv->GetHumidity!= NULL)
    {
        Hum_tempDrv->GetHumidity(pfData);
    }
}

/**
 * @brief  Get Temperature value
 * @param pfData: pointer on floating data
 * @retval None
 */
void BSP_HUM_TEMP_GetTemperature(float* pfData)
{
    if(Hum_tempDrv->GetTemperature!= NULL)
    {
        Hum_tempDrv->GetTemperature(pfData);
    }
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/     
