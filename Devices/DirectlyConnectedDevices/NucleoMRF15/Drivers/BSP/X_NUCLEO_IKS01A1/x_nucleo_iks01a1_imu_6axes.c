/**
 ******************************************************************************
 * @file    x_nucleo_iks01a1_imu_6axes.c
 * @author  MEMS Application Team
 * @version V1.0.0
 * @date    30-July-2014
 * @brief   This file provides a set of functions needed to manage the lsm6ds0 sensor.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "x_nucleo_iks01a1_imu_6axes.h"

/** @addtogroup BSP
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1_IMU_6AXES
 * @{
 */


/** @defgroup X_NUCLEO_IKS01A1_IMU_6AXES_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_IMU_6AXES_Private_Defines
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_IMU_6AXES_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_IMU_6AXES_Private_Variables
 * @{
 */
static IMU_6AXES_DrvTypeDef *Imu6AxesDrv;
static uint8_t Imu6AxesInitialized = 0;

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_IMU_6AXES_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_IMU_6AXES_Private_Functions
 * @{
 */

/**
 * @brief  Initialization 6 axes sensor.
 * @param  None
 * @retval IMU_6AXES_OK if no problem during initialization
 */
IMU_6AXES_StatusTypeDef BSP_IMU_6AXES_Init(void)
{
    IMU_6AXES_StatusTypeDef ret = IMU_6AXES_ERROR;
    IMU_6AXES_InitTypeDef LSM6DS0_InitStructure;

    /* Initialize the six axes driver structure */
    Imu6AxesDrv = &LSM6DS0Drv;

    /* Configure sensor */
    LSM6DS0_InitStructure.G_FullScale = LSM6DS0_G_FS_2000;
    LSM6DS0_InitStructure.G_OutputDataRate = LSM6DS0_G_ODR_119HZ;
    LSM6DS0_InitStructure.G_X_Axis = LSM6DS0_G_XEN_ENABLE;
    LSM6DS0_InitStructure.G_Y_Axis = LSM6DS0_G_YEN_ENABLE;
    LSM6DS0_InitStructure.G_Z_Axis = LSM6DS0_G_ZEN_ENABLE;

    LSM6DS0_InitStructure.X_FullScale = LSM6DS0_XL_FS_2G;
    LSM6DS0_InitStructure.X_OutputDataRate = LSM6DS0_XL_ODR_119HZ;
    LSM6DS0_InitStructure.X_X_Axis = LSM6DS0_XL_XEN_ENABLE;
    LSM6DS0_InitStructure.X_Y_Axis = LSM6DS0_XL_YEN_ENABLE;
    LSM6DS0_InitStructure.X_Z_Axis = LSM6DS0_XL_ZEN_ENABLE;

    /* six axes sensor init */
    Imu6AxesDrv->Init(&LSM6DS0_InitStructure);

    if(Imu6AxesDrv->Read_XG_ID() == I_AM_LSM6DS0_XG)
    {
        Imu6AxesInitialized = 1;
        ret = IMU_6AXES_OK;
    }

    return ret;
}


uint8_t BSP_IMU_6AXES_isInitialized(void)
{
    return Imu6AxesInitialized;
}


/**
 * @brief  Read ID of LSM6DS0 Accelerometer and Gyroscope
 * @param  None
 * @retval ID
 */
uint8_t BSP_IMU_6AXES_Read_XG_ID(void)
{
    uint8_t id = 0x00;

    if(Imu6AxesDrv->Read_XG_ID != NULL)
    {
        id = Imu6AxesDrv->Read_XG_ID();
    }
    return id;
}


/**
 * @brief  Check ID of LSM6DS0 Accelerometer and Gyroscope sensor
 * @param  None
 * @retval Test status
 */
IMU_6AXES_StatusTypeDef BSP_IMU_6AXES_Check_XG_ID(void) {
    if (BSP_IMU_6AXES_Read_XG_ID() == I_AM_LSM6DS0_XG) {
        return IMU_6AXES_OK;
    } else {
        return IMU_6AXES_ERROR;
    }
}


/**
 * @brief  Get Accelerometer raw axes
 * @param pData: pointer on AxesRaw_TypeDef data
 * @retval None
 */
void BSP_IMU_6AXES_X_GetAxesRaw(AxesRaw_TypeDef *pData)
{
    if(Imu6AxesDrv->Get_X_Axes!= NULL)
    {
      Imu6AxesDrv->Get_X_Axes((int32_t *)pData);
    }
}


/**
 * @brief  Get Gyroscope raw axes
 * @param pData: pointer on AxesRaw_TypeDef data
 * @retval None
 */
void BSP_IMU_6AXES_G_GetAxesRaw(AxesRaw_TypeDef *pData)
{
    if(Imu6AxesDrv->Get_G_Axes!= NULL)
    {
      Imu6AxesDrv->Get_G_Axes((int32_t *)pData);
    }
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
