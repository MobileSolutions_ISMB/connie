/**
 ******************************************************************************
 * @file    x_nucleo_iks01a1_magneto.c
 * @author  MEMS Application Team
 * @version V1.0.0
 * @date    30-July-2014
 * @brief   This file provides a set of functions needed to manage the lis3mdl sensor.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "x_nucleo_iks01a1_magneto.h"

/** @addtogroup BSP
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1_MAGNETO
 * @{
 */

/** @defgroup X_NUCLEO_IKS01A1_MAGNETO_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_MAGNETO_Private_Defines
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_MAGNETO_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_MAGNETO_Private_Variables
 * @{
 */
static MAGNETO_DrvTypeDef *MagnetoDrv;
static uint8_t MagnetoInitialized = 0;

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_MAGNETO_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_MAGNETO_Private_Functions
 * @{
 */

/**
 * @brief  Initialization magneto sensor.
 * @param  None
 * @retval MAGNETO_OK if no problem during initialization
 */
MAGNETO_StatusTypeDef BSP_MAGNETO_Init(void)
{
    MAGNETO_StatusTypeDef ret = MAGNETO_ERROR;
    MAGNETO_InitTypeDef LIS3MDL_InitStructure;

    /* Initialize the magneto driver structure */
    MagnetoDrv = &LIS3MDLDrv;

    /* Configure sensor */
    LIS3MDL_InitStructure.M_FullScale = LIS3MDL_M_FS_4;
    LIS3MDL_InitStructure.M_OperatingMode = LIS3MDL_M_MD_CONTINUOUS;
    LIS3MDL_InitStructure.M_XYOperativeMode = LIS3MDL_M_OM_HP;
    LIS3MDL_InitStructure.M_OutputDataRate = LIS3MDL_M_DO_80;

    /* magneto sensor init */
    MagnetoDrv->Init(&LIS3MDL_InitStructure);

    if(MagnetoDrv->Read_M_ID() == I_AM_LIS3MDL_M)
    {
        MagnetoInitialized = 1;
        ret = MAGNETO_OK;
    }

    return ret;
}


uint8_t BSP_MAGNETO_isInitialized(void)
{
    return MagnetoInitialized;
}


/**
 * @brief  Read ID of LIS3MDL Magnetic sensor
 * @param  None
 * @retval ID
 */
uint8_t BSP_MAGNETO_Read_M_ID(void)
{
    uint8_t id = 0x00;

    if(MagnetoDrv->Read_M_ID != NULL)
    {
        id = MagnetoDrv->Read_M_ID();
    }
    return id;
}


/**
 * @brief  Check ID of LIS3MDL Magnetic sensor
 * @param  None
 * @retval Test status
 */
MAGNETO_StatusTypeDef BSP_MAGNETO_Check_M_ID(void) {
    if (BSP_MAGNETO_Read_M_ID() == I_AM_LIS3MDL_M) {
        return MAGNETO_OK;
    } else {
        return MAGNETO_ERROR;
    }
}


/**
 * @brief  Get Magnetic sensor raw axes
 * @param pData: pointer on AxesRaw_TypeDef data
 * @retval None
 */
void BSP_MAGNETO_M_GetAxesRaw(AxesRaw_TypeDef *pData)
{
    if(MagnetoDrv->Get_M_Axes!= NULL)
    {
      MagnetoDrv->Get_M_Axes((int32_t *)pData);
    }
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
