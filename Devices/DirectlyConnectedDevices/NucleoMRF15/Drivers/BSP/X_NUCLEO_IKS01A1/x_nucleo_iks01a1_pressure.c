/**
 ******************************************************************************
 * @file    x_nucleo_iks01a1_pressure.c
 * @author  MEMS Application Team
 * @version V1.0.0
 * @date    30-July-2014
 * @brief   This file provides a set of functions needed to manage the lps25h sensor.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "x_nucleo_iks01a1_pressure.h"

/** @addtogroup BSP
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1
 * @{
 */

/** @addtogroup X_NUCLEO_IKS01A1_PRESSURE
 * @{
 */


/** @defgroup X_NUCLEO_IKS01A1_PRESSURE_Private_TypesDefinitions
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_PRESSURE_Private_Defines
 * @{
 */
#ifndef NULL
  #define NULL      (void *) 0
#endif
/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_PRESSURE_Private_Macros
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_PRESSURE_Private_Variables
 * @{
 */
static PRESSURE_DrvTypeDef *PressureDrv;
static uint8_t PressureInitialized = 0;

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_PRESSURE_Private_FunctionPrototypes
 * @{
 */

/**
 * @}
 */

/** @defgroup X_NUCLEO_IKS01A1_PRESSURE_Private_Functions
 * @{
 */

/**
 * @brief  Initialization pressure sensor.
 * @param  None
 * @retval PRESSURE_OK if no problem during initialization
 */
PRESSURE_StatusTypeDef BSP_PRESSURE_Init(void)
{
    PRESSURE_StatusTypeDef ret = PRESSURE_ERROR;
    PRESSURE_InitTypeDef LPS25H_InitStructure;

//    LPS25H_SlaveAddrRemap(LPS25H_SA0_HIGH);

    /* Initialize the uv driver structure */
    PressureDrv = &LPS25HDrv;

    /* Configure sensor */
    LPS25H_InitStructure.OutputDataRate = LPS25H_ODR_1Hz;
    LPS25H_InitStructure.BlockDataUpdate = LPS25H_BDU_CONT;
    LPS25H_InitStructure.DiffEnable = LPS25H_DIFF_ENABLE;
    LPS25H_InitStructure.SPIMode = LPS25H_SPI_SIM_3W;
    LPS25H_InitStructure.PressureResolution = LPS25H_P_RES_AVG_32;
    LPS25H_InitStructure.TemperatureResolution = LPS25H_T_RES_AVG_16;

    /* Pressure sensor init */
    PressureDrv->Init(&LPS25H_InitStructure);

    if(PressureDrv->ReadID() == I_AM_LPS25H)
    {
        PressureInitialized = 1;
        ret = PRESSURE_OK;
    }

    return ret;
}

uint8_t BSP_PRESSURE_isInitialized(void)
{
    return PressureInitialized;
}

/**
 * @brief  Read ID of Uv component
 * @param  None
 * @retval ID
 */
uint8_t BSP_PRESSURE_ReadID(void)
{
    uint8_t id = 0x00;

    if(PressureDrv->ReadID != NULL)
    {
        id = PressureDrv->ReadID();
    }
    return id;
}


/**
 * @brief  Check ID of LPS25H Pressure sensor
 * @param  None
 * @retval Test status
 */
PRESSURE_StatusTypeDef BSP_PRESSURE_CheckID(void) {
    if (BSP_PRESSURE_ReadID() == I_AM_LPS25H) {
        return PRESSURE_OK;
    } else {
        return PRESSURE_ERROR;
    }
}


/**
 * @brief  Reboot memory content of LPS25H Pressure sensor
 * @param  None
 * @retval None
 */
void BSP_PRESSURE_Reset(void)
{
    if(PressureDrv->Reset != NULL)
    {
        PressureDrv->Reset();
    }
}


/**
 * @brief  Get Pressure
 * @param pfData: pointer on floating data
 * @retval None
 */
void BSP_PRESSURE_GetPressure(float* pfData)
{
    if(PressureDrv->GetPressure!= NULL)
    {
        PressureDrv->GetPressure(pfData);
    }
}

/**
 * @brief  Get Temperature
 * @param pfData: pointer on floating data
 * @retval None
 */
void BSP_PRESSURE_GetTemperature(float* pfData)
{
    if(PressureDrv->GetTemperature!= NULL)
    {
        PressureDrv->GetTemperature(pfData);
    }
}

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
