/**
  ******************************************************************************
  * @file    hmac_hw.c
  * @author  MCD Application Team
  * @version V2.1
  * @date    22-June-2012
  * @brief   Provides HW HMAC functions
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hash_common.h"

/* This file needs compiler guards because it is can be used by HMAC-MD5 and HMAC-SHA-1 */
#ifndef __HMAC_HW_C__
#define __HMAC_HW_C__

/** @addtogroup HASHlowlevel HASH
  * @{
  */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Initialize the HW HMAC engine
  * @note   Static function
  * @param[in,out]  *P_pContext The context that will be initialized 
  * @param[in]  P_hashType Descriptor of the HASH functionalities to be used 
  */
static void HMAChwInit(HMACctx_stt *P_pContext, hashType_et P_hashType)
{
  HASH_InitTypeDef HASH_st;
  int32_t i;

  /* Enable Hash Engine */
  RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_HASH, ENABLE);
  HASH_DeInit();
  /* Set the value of count to zero, these will be used to store partial bytes */
  P_pContext->mHASHctx_st.amCount[0] = 0;
  P_pContext->mHASHctx_st.amCount[1] = 0;
  /* initialize the HASH_AlgoMode member */
  HASH_st.HASH_AlgoMode = HASH_AlgoMode_HMAC;
  /* initialize the CRYPTO_DataType member */
  HASH_st.HASH_DataType = HASH_DataType_8b;
  /* Initialize the HASH_KeyType member */
  if ( P_pContext->mKeySize < 65 )
  {
    HASH_st.HASH_HMACKeyType = HASH_HMACKeyType_ShortKey;
  }
  else
  {
    HASH_st.HASH_HMACKeyType = HASH_HMACKeyType_LongKey;
  }
  switch (P_hashType)
  {
    case E_SHA1:
      HASH_st.HASH_AlgoSelection = HASH_AlgoSelection_SHA1;
      break;
    case E_MD5:
      HASH_st.HASH_AlgoSelection = HASH_AlgoSelection_MD5;
      break;
    default:
      break;
  }
  HASH_Init(&HASH_st);
  HASH_Reset();

  /* Loop on every full word of the key and process it */
  for (i = 0; (i + 4) <= P_pContext->mKeySize; i += 4)
  {
    HASH->DIN = ((uint32_t *)(P_pContext->pmKey + i))[0];
    while ((HASH->SR & HASH_FLAG_BUSY) != 0)
    {}
  }
  /* Do the final, partial word */
  if ( i < P_pContext->mKeySize )
  {
    HASH->DIN = ((uint32_t *)(P_pContext->pmKey + i))[0];
    HASH->STR |= ((P_pContext->mKeySize - i) * 8);
  }
  /* Start computing */
  HASH->STR |= HASH_STR_DCAL;
  /* Wait till it's done */
  while ((HASH->SR & HASH_FLAG_BUSY) != 0)
  {}
}

/**
  * @brief  Start HW HMAC engine
  * @note   Static function
  * @param[in]   *P_pContext The context that will be initialized 
  * @param[out]  *P_pDigest Where the 20 byte digest will be written 
  */
static void HMAChwFinish(HMACctx_stt *P_pContext, uint8_t *P_pDigest)
{
  int32_t i;

  /* In case there's no DMA check if there are bytes left */
  if ( (P_pContext->mFlags & E_HASH_USE_DMA) != E_HASH_USE_DMA )
  {
    /* Do the final hash word */
    if ( P_pContext->mHASHctx_st.amCount[0] != 0 )
    {
      HASH->DIN = ((uint32_t *)(P_pContext->mHASHctx_st.amBuffer))[0];
      HASH->STR = (P_pContext->mHASHctx_st.amCount[0] * 8);
    }
    HASH->STR |= HASH_STR_DCAL;
  }
  /* Wait */
  while ((HASH->SR & HASH_FLAG_BUSY) != 0)
  {}

  /* Reprocess the key */
  for (i = 0; (i + 4) <=  P_pContext->mKeySize; i += 4)
  {
    HASH->DIN = ((uint32_t *)( P_pContext->pmKey + i))[0];
    while ((HASH->SR & HASH_FLAG_BUSY) != 0)
    {}
  }
  /* Do the final, partial, word */
  if ( i < P_pContext->mKeySize )
  {
    HASH->DIN = ((uint32_t *)(P_pContext->pmKey + i))[0];
  }
  HASH->STR = (P_pContext->mKeySize - i) * 8;
  HASH->STR |= HASH_STR_DCAL;
  /* Wait till TAG is ready */
  while ((HASH->SR & HASH_FLAG_BUSY) != 0)
  {}

  /* Read output */
  for (i = 0 ; i < 5; i++)
  {
    ((uint32_t *)(P_pDigest))[i] =  __REV( HASH->HR[i] );
  }
  /* Disable Hash Engine */
  RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_HASH, DISABLE);
}
/**
  * @}
  */
  
 #endif /* __HMAC_HW_C__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
