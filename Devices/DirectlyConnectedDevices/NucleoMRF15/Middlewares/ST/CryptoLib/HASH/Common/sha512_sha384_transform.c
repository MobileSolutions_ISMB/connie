/**
  ******************************************************************************
  * @file    sha512_sha384_transform.c
  * @author  MCD Application Team
  * @version V2.2
  * @date    27-January-2014
  * @brief   SHA-512 Update function
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
  
/* Includes ------------------------------------------------------------------*/ 
#include <string.h>
#include "sha512_sha384_transform.h"

#ifndef __SHA512_TRANSFORM_C__
#define __SHA512_TRANSFORM_C__

/** @addtogroup SHA512lowlevel
  * @{
  */

/* External variables --------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private defines -----------------------------------------------------------*/
/* Private macros ------------------------------------------------------------*/

#define Ch(x,y,z)   (((x) & (y)) ^ (~(x) & (z)))
#define Maj(x,y,z)  (((x) & (y)) ^ ( (x) & (z)) ^ ((y) & (z)))
#define RORu64(x,y) ( ((x) >> (y)) | ((x) << (64 - (y))) )

#define e0(x)       (RORu64((x),28) ^ RORu64((x),34) ^ RORu64((x),39))
#define e1(x)       (RORu64((x),14) ^ RORu64((x),18) ^ RORu64((x),41))
#define s0(x)       (RORu64((x), 1) ^ RORu64((x), 8) ^ ((x) >> 7))
#define s1(x)       (RORu64((x),19) ^ RORu64((x),61) ^ ((x) >> 6))

/* Private variables ---------------------------------------------------------*/

/** SHA-512 Round Constants */
static const uint64_t sha512_K[80] = {
	0x428a2f98d728ae22ULL, 0x7137449123ef65cdULL, 0xb5c0fbcfec4d3b2fULL,
	0xe9b5dba58189dbbcULL, 0x3956c25bf348b538ULL, 0x59f111f1b605d019ULL,
	0x923f82a4af194f9bULL, 0xab1c5ed5da6d8118ULL, 0xd807aa98a3030242ULL,
	0x12835b0145706fbeULL, 0x243185be4ee4b28cULL, 0x550c7dc3d5ffb4e2ULL,
	0x72be5d74f27b896fULL, 0x80deb1fe3b1696b1ULL, 0x9bdc06a725c71235ULL,
	0xc19bf174cf692694ULL, 0xe49b69c19ef14ad2ULL, 0xefbe4786384f25e3ULL,
	0x0fc19dc68b8cd5b5ULL, 0x240ca1cc77ac9c65ULL, 0x2de92c6f592b0275ULL,
	0x4a7484aa6ea6e483ULL, 0x5cb0a9dcbd41fbd4ULL, 0x76f988da831153b5ULL,
	0x983e5152ee66dfabULL, 0xa831c66d2db43210ULL, 0xb00327c898fb213fULL,
	0xbf597fc7beef0ee4ULL, 0xc6e00bf33da88fc2ULL, 0xd5a79147930aa725ULL,
	0x06ca6351e003826fULL, 0x142929670a0e6e70ULL, 0x27b70a8546d22ffcULL,
	0x2e1b21385c26c926ULL, 0x4d2c6dfc5ac42aedULL, 0x53380d139d95b3dfULL,
	0x650a73548baf63deULL, 0x766a0abb3c77b2a8ULL, 0x81c2c92e47edaee6ULL,
	0x92722c851482353bULL, 0xa2bfe8a14cf10364ULL, 0xa81a664bbc423001ULL,
	0xc24b8b70d0f89791ULL, 0xc76c51a30654be30ULL, 0xd192e819d6ef5218ULL,
	0xd69906245565a910ULL, 0xf40e35855771202aULL, 0x106aa07032bbd1b8ULL,
	0x19a4c116b8d2d0c8ULL, 0x1e376c085141ab53ULL, 0x2748774cdf8eeb99ULL,
	0x34b0bcb5e19b48a8ULL, 0x391c0cb3c5c95a63ULL, 0x4ed8aa4ae3418acbULL,
	0x5b9cca4f7763e373ULL, 0x682e6ff3d6b2b8a3ULL, 0x748f82ee5defb2fcULL,
	0x78a5636f43172f60ULL, 0x84c87814a1f0ab72ULL, 0x8cc702081a6439ecULL,
	0x90befffa23631e28ULL, 0xa4506cebde82bde9ULL, 0xbef9a3f7b2c67915ULL,
	0xc67178f2e372532bULL, 0xca273eceea26619cULL, 0xd186b8c721c0c207ULL,
	0xeada7dd6cde0eb1eULL, 0xf57d4f7fee6ed178ULL, 0x06f067aa72176fbaULL,
	0x0a637dc5a2c898a6ULL, 0x113f9804bef90daeULL, 0x1b710b35131c471bULL,
	0x28db77f523047d84ULL, 0x32caab7b40c72493ULL, 0x3c9ebe0a15c9bebcULL,
	0x431d67c49c100d4cULL, 0x4cc5d4becb3e42b6ULL, 0x597f299cfc657e2aULL,
	0x5fcb6fab3ad6faecULL, 0x6c44198c4a475817ULL,
};
/* Private function prototypes -----------------------------------------------*/

static void SHA512Transform(uint64_t* state, const uint8_t* buffer);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Core funcition to hash a 512-bit block
  * @note   Static function
  * @param[in,out]  *state  The array containing the SHA512 context 
  * @param[in]      *buffer The 1024-bit data block that will be hashed 
  */
static void SHA512Transform(uint64_t* state, const uint8_t* buffer)
{
	uint64_t a, b, c, d, e, f, g, h, t1, t2;
	uint64_t W[80];
	int i;
#ifdef LOCKING_MECHANISM 
  volatile uint32_t CryptoAlgo_Value = 0x00;
#endif

  /* load the input */
	for (i = 0; i < 16; i++) 
  {
    W[i] =  (uint64_t) (LE_CONVERT_W32(*((const uint32_t *)  buffer + i*2))) << 32 ;
    W[i] |= (uint64_t) (LE_CONVERT_W32(*((const uint32_t *)  buffer + i*2 + 1)));
  }
	
	/* now blend */
	for (i = 16; i < 80; i++) 
  {
    W[i] = s1(W[i-2]) + W[i-7] + s0(W[i-15]) + W[i-16];
  }
	
	/* load the state into our registers */
	a = state[0]; 
  b = state[1]; 
  c = state[2]; 
  d = state[3];  
	e = state[4]; 
  f = state[5]; 
  g = state[6]; 
  h = state[7];  


#ifdef LOCKING_MECHANISM
  * (volatile unsigned int *) 0x40023008 = 0x01  ;
  CryptoAlgo_Value = * (volatile unsigned int *) 0x40023008;

  CryptoAlgo_Value = * (volatile uint32_t *) 0x40023000;

  if (CryptoAlgo_Value == 0xFFFFFFFF)
  {
    *(volatile uint32_t *) 0x40023000 = CryptoAlgo_Value;

    CryptoAlgo_Value = * (volatile uint32_t *) 0x40023000;

    if (CryptoAlgo_Value == 0x00000000)
    {}
    else
    {
      state[0] = state[0] ^ 0x34705604 ;
      state[4] = state[4] ^ 0x89563478 ;
      a = state[7] ^ state[4];
      b = state[6] ^ state[3];
      c = state[5] ^ state[2];
      d = state[4] ^ state[1];
      e = state[3] ^ state[0];
      f = state[2] ^ state[7];
      g = state[1] ^ state[6];
      h = state[0] ^ state[5];

    }
  }
  else
  {
    state[0] = state[0] ^ 0x34705604 ;
    state[4] = state[4] ^ 0x89563478 ;
    a = state[7] ^ state[4];
    b = state[6] ^ state[3];
    c = state[5] ^ state[2];
    d = state[4] ^ state[1];
    e = state[3] ^ state[0];
    f = state[2] ^ state[7];
    g = state[1] ^ state[6];
    h = state[0] ^ state[5];

  }
#endif

	/* now iterate */
	for (i = 0; i < 80; i++) 
  {
		t1 = h + e1(e) + Ch(e,f,g) + sha512_K[i] + W[i];
		t2 = e0(a) + Maj(a,b,c);
		h = g; 
    g = f; 
    f = e; 
    e = d + t1;
		d = c; 
    c = b; 
    b = a; 
    a = t1 + t2;
	}
  
	state[0] += a; 
  state[1] += b; 
  state[2] += c; 
  state[3] += d;  
	state[4] += e; 
  state[5] += f; 
  state[6] += g; 
  state[7] += h;  
}

/**
  * @brief  Data process function using SHA-512 (or SHA-384), updates the HASHcontext
  * @note   The data processing function is the same for SHA-384 and SHA-512
  * @param[in,out]  *P_pSHA512ctx     The SHA512ctx_stt context that will be updated 
  * @param[in]  *P_pInput       The input data that will be hashed 
  * @param[in]  P_inputSize    The length in bytes of P_pInput 
  */
void SHA512Update(HASHLctx_stt* P_pSHA512ctx, const uint8_t* P_pInput, uint32_t P_inputSize)
{
  uint32_t i, j;
  j = (P_pSHA512ctx->amCount[0] >> 3) & 127;      /*63 = 111111*/
  /* Adds to total bit count, the value here processed */
  P_pSHA512ctx->amCount[0] += P_inputSize << 3;
  /* check it didn't overflow */
  if ( P_pSHA512ctx->amCount[0] < (P_inputSize << 3))
  {
    P_pSHA512ctx->amCount[1]++;
  }
  P_pSHA512ctx->amCount[1] += (P_inputSize >> 29);
  if ((j + P_inputSize) > 127u)
  {
    memcpy(&P_pSHA512ctx->amBuffer[j], P_pInput, (i = 128u - j));
    SHA512Transform(P_pSHA512ctx->amState, P_pSHA512ctx->amBuffer);
    for ( ; i + 127u < P_inputSize; i += 128u)
    {
      /* This trick is done to check if the buffer is addressable */
      /* The minus zero is done to avoid problems with the const */
      if ( ( (&P_pInput[i] - (const uint8_t *) 0) & 3) != 0 )
      {
        /* data are properly aligned */
        SHA512Transform(P_pSHA512ctx->amState, &P_pInput[i]);
      }
      else
      {
        /* not aligned */
        memcpy(&P_pSHA512ctx->amBuffer, &P_pInput[i], 128u);
        SHA512Transform(P_pSHA512ctx->amState, P_pSHA512ctx->amBuffer);
      }
    }
    j = 0u;
  }
  else
  {
    i = 0u;
  }
  memcpy(&P_pSHA512ctx->amBuffer[j], &P_pInput[i], (size_t) (P_inputSize - i));
}



/* Undef Macros */
#undef s1
#undef s0
#undef e1
#undef e0
#undef RORu64
#undef Ch
#undef Maj

#endif /* __SHA512_TRANSFORM_C__ */

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
