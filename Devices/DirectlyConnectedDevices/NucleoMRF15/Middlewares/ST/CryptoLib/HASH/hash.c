/**
  ******************************************************************************
  * @file    hash.c
  * @author  MCD Application Team
  * @version V2.1
  * @date    22-June-2012
  * @brief   Container for the HASH functionalities
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */
#include "hash.h"

/** @addtogroup UserAPI User Level API
  * @{
  */

/** @defgroup HASH Hash/HMAC Functions
  * @brief Container for all the Hash and HMAC Algorithms
  * @{
  */


#ifdef INCLUDE_MD5
#ifdef USE_HW_MD5
#include "Common/hash_hw.c"
#endif
#include "MD5/md5.c"
#ifdef USE_SW_MD5
#include "MD5/md5_low_level.c"
#endif
#ifdef INCLUDE_HMAC
#ifdef USE_HW_MD5
#include "Common/hmac_hw.c"
#endif
#include "MD5/hmac_md5.c"
#endif
#endif

#ifdef INCLUDE_SHA1
#ifdef USE_HW_SHA1
#include "Common/hash_hw.c"
#endif
#include "SHA1/sha1.c"
#ifdef USE_SW_SHA1
#include "SHA1/sha1_low_level.c"
#endif
#ifdef INCLUDE_HMAC
#ifdef USE_HW_SHA1
#include "Common/hmac_hw.c"
#endif
#include "SHA1/hmac_sha1.c"
#endif
#endif

#ifdef INCLUDE_SHA224
#include "SHA224/sha224.c"
#include "Common/sha256_sha224_transform.c"
#include "SHA224/sha224_low_level.c"
#ifdef INCLUDE_HMAC
#include "SHA224/hmac_sha224.c"
#endif
#endif

#ifdef INCLUDE_SHA256
#include "SHA256/sha256.c"
#include "Common/sha256_sha224_transform.c"
#include "SHA256/sha256_low_level.c"
#ifdef INCLUDE_HMAC
#include "SHA256/hmac_sha256.c"
#endif
#endif

#ifdef INCLUDE_SHA384
#include "SHA384/sha384.c"
#include "Common/sha512_sha384_transform.c"
#include "SHA384/sha384_low_level.c"
#ifdef INCLUDE_HMAC
#include "SHA384/hmac_sha384.c"
#endif
#endif

#ifdef INCLUDE_SHA512
#include "SHA512/sha512.c"
#include "Common/sha512_sha384_transform.c"
#include "SHA512/sha512_low_level.c"
#ifdef INCLUDE_HMAC
#include "SHA512/hmac_sha512.c"
#endif
#endif

 /**
  * @} HASH
  */

 /**
  * @} UserAPI
  */
  
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
