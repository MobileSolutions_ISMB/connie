/**
  ******************************************************************************
  * @file    crypto.c
  * @author  MCD Application Team
  * @version V2.1
  * @date    22-June-2012
  * @brief   Container for the crypto library
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2012 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

#include "crypto.h"

#ifdef INCLUDE_AES
#include "AES/aes.c"
#endif

#ifdef INCLUDE_DES
#include "DES/des.c"
#endif

#ifdef INCLUDE_TDES
#include "TDES/tdes.c"
#endif

#ifdef INCLUDE_ARC4
#include "ARC4/arc4.c"
#endif

#ifdef INCLUDE_HASH
#include "HASH/hash.c"
#endif

#ifdef INCLUDE_MATH
#include "MATH/math.c"
#endif

#ifdef INCLUDE_RSA
#include "RSA/rsa.c"
#endif

#ifdef INCLUDE_RNG
#include "RNG/rng.c"
#endif

#ifdef INCLUDE_ECC
#include "ECC/ecc.c"
#endif

/**
  * @brief Initialize the Crypto Library
  * @remark This function should be called to initialize the crypto library before performing any other operation
  */
void Crypto_DeInit(void)
{
#if defined(STM32F2XX) || defined(STM32F4XX) || defined (STM32F40XX) || defined(STM32F427X)
  /* Enable CRC clock to activate Cryptographic algorithm */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_CRC, ENABLE);
#elif defined(CL_ON_STM32)
  /* Enable CRC clock to activate Cryptographic algorithm */ 
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);
#endif /* defined(STM32F2XX) || defined(STM32F4XX) */ 
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
