/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EVENTHUB_H__
#define __EVENTHUB_H__  

#ifdef __cplusplus
 extern "C" {
#endif

#define NEW_CTD
   
#include "crypto.h"
#include "base64.h"
#include <string.h>
#include <stdio.h>   
#include <stdlib.h>
#include <ctype.h>

 
typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

  #define EXPIRE_GAP      60                      // Minutes of validity of token
  #define HMAC_LENGTH     32

#ifdef NEW_CTD
  #define ServiceBus     	"ctd-maker-fair.servicebus.windows.net"
  #define ServiceBusPort  	443
  #define EventHubName   	"ehdevices"
  #define KeyName        	"D1"
  #define Key                   "+TJcXijDiC0OOtPP2WQ6gnLhyIqWO5iFW44Z1eKgiGw="

  #define json_start "{\"id\": \""
  #define json_end "\","\
                "\"name\": \"NucleoF4-%s\","\
                "\"ts\": \"%s\","\
                "\"mtype\": \"ins\","\
                "\"temp\": %s,"\
                "\"hum\": %s,"\
                "\"accX\": %s,"\
                "\"accY\": %s,"\
                "\"accZ\": %s,"\
                "\"gyrX\": %s,"\
                "\"gyrY\": %s,"\
                "\"gyrZ\": %s }\n"
#else
  #define ServiceBus            "micto-ehctd.servicebus.windows.net"
  #define ServiceBusPort        443
  #define EventHubName          "ehdevices"
  #define KeyName               "D1"
  #define Key                   "pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ="

  #define json_start "{\"guid\": \"4139a248-e2f9-4438-ab23-"
  #define json_end "\","\
                "\"organization\": \"ISMB\","\
                "\"displayname\": \"%s\","\
                "\"location\": \"Torino\","\
                "\"measurename\": \"Temperature\","\
                "\"unitofmeasure\": \"C\","\
                "\"timecreated\": \"%s\","\
                "\"value\": %s }\n"
                  
#endif
                  
  #define str_post "POST %s HTTP/1.1\n"\
                 "Host: %s\n"\
                 "Cache-Control: no-cache\n"\
                 "Content-Type: application/json; charset=UTF-8\n"\
                 "Content-Length: %d\n"
                 
  #define str_sas  "Authorization: SharedAccessSignature %s\n"\
                   "\n"

void EventHub_Init(void);
char *PreparePost(int bodyLen);

#ifdef NEW_CTD
  char *PrepareBody(char *date, char *temp, char *hum, char *accX, char *accY, char *accZ, char *gyrX, char *gyrY, char *gyrZ);
#else
  char *PrepareBody(char *date, char *value);
#endif
  
char *PrepareSAS(void);
int32_t SASTokenHelper(uint32_t timeNow);

#ifdef __cplusplus
 }
#endif

#endif
