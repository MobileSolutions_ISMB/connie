/* Includes ------------------------------------------------------------------*/
#include "EventHub.h"
#include "wifi_interface.h"
#include "wifi_module.h"
#include "ring_buffer.h"
#include "main.h"
#include "stm32_spwf_wifi.h"
#include "wifi_const.h"

/* Exported variables --------------------------------------------------------*/
static char *postUri;

extern UART_HandleTypeDef UartHandle,UartMsgHandle;
extern char print_msg_buff[512];

#ifdef USART_PRINT_MSG
#define printf(arg)    {sprintf((char*)print_msg_buff,arg);   \
HAL_UART_Transmit(&UartMsgHandle, (uint8_t*)print_msg_buff, strlen(print_msg_buff), 1000);}
#endif 


static char *stringToSign;
static char signature[45];
static char *SAS;

static char post[450];
static char jsonBuilder[350];

static uint8_t DisplayName[17];
static char json[400]; 


/* Private variables ---------------------------------------------------------*/

/* Private prototypes --------------------------------------------------------*/
int32_t STM32_SHA256_HMAC_Compute(uint8_t* InputMessage,
                          uint32_t InputMessageLength,
                          uint8_t *HMAC_key,
                          uint32_t HMAC_keyLength,
                          uint8_t *MessageDigest,
                          int32_t *MessageDigestLength);
//static char a2i(char ch);
static char i2a(char code);
char *urlencode( char *plain );

void EventHub_Init(void) {
  int i;
  int j=0;
  uint8_t macaddstart[64];
        
  GET_Configuration_Value("nv_wifi_macaddr",macaddstart);
  for(i=0;i<17;i++){
    if(macaddstart[i]!=':'){
      DisplayName[j]=macaddstart[i];
      j++;  
    } 
  }
  
  printf("[D] MAC Address is:");
  printf((char*)DisplayName);
  printf("\r\n");
  sprintf(json, "%s%s%s",json_start,DisplayName,json_end);

  // build postUri and stringToSign once and for all
  postUri = (char *) malloc((strlen(EventHubName) + strlen((char*)DisplayName) + strlen("//publishers//messages"))*sizeof(char));

  // Post Uri is in the form
  // "/eventhubname/publishers/displayname/messages"
  sprintf(postUri, "/%s/publishers/%s/messages", EventHubName, DisplayName);

  // String to sign is in the form
  // "https://service-bus/eventhubname/publishers/displayname/messages"
  stringToSign = (char *) malloc((strlen(ServiceBus) + strlen(postUri)+ strlen("https://"))*sizeof(char));
  sprintf(stringToSign, "https://%s%s", ServiceBus, postUri);
}

int32_t SASTokenHelper(uint32_t timeNow) {
  char extStrToSign[150];
  uint8_t MAC[CRL_SHA256_SIZE];
  int32_t MACLength = 0;
  int32_t expire = timeNow + (EXPIRE_GAP * 60);

  // Perform the URL Encode of the string to sign
  // For more information on URL Encode look here:
  // https://en.wikipedia.org/wiki/Percent-encoding
  char *enc = urlencode(stringToSign);

  // The complete string to generate the SAS Token is in the form
  // stringToSign + '\n' + expiryDate
  // For more information on SAS Token look here:
  // https://azure.microsoft.com/en-us/documentation/articles/service-bus-sas-overview/
  sprintf(extStrToSign, "%s\n%d", enc, expire); 
  
  int32_t status = STM32_SHA256_HMAC_Compute((uint8_t*)extStrToSign,
                               strlen(extStrToSign),
                               (uint8_t*)Key,
                               strlen(Key),
                               (uint8_t*)MAC,
                               &MACLength);
  
  Base64encode(signature, (char *)MAC, MACLength);
  
  if(status == HASH_SUCCESS) {
    char *enc2 = urlencode(signature);
    SAS = (char *) malloc((strlen(enc)+strlen(enc2)+10+3)* sizeof(char));
    sprintf(SAS, "sr=%s&sig=%s&se=%d&skn=%s", enc, enc2, expire, KeyName); 
    free(enc);
    free(enc2);
    return 0;
  }
  else {
    free(enc);
    return -1;
  }
}

char *PreparePost(int bodyLen) {
  sprintf(post, str_post, 
          postUri, 
          ServiceBus,
          bodyLen);
  
  return post;
}

#ifdef NEW_CTD
char *PrepareBody(char *date, char *temp, char *hum, char *accX, char *accY, char *accZ, char *gyrX, char *gyrY, char *gyrZ) {
    sprintf(jsonBuilder, json, DisplayName, 
            date, 
            temp, hum,
            accX, accY, accZ,
            gyrX, gyrY, gyrZ);
    return jsonBuilder;
}
#else
char *PrepareBody(char *date, char *value) {
    sprintf(jsonBuilder, json, DisplayName, date, value);
    return jsonBuilder;
}
#endif

char *PrepareSAS() {
    char *postSas = (char *) malloc((strlen(SAS) + strlen(str_sas))*sizeof(char)); 
    sprintf(postSas, str_sas, SAS);

    return postSas;
}


/* Private Functions ---------------------------------------------------------*/

int32_t STM32_SHA256_HMAC_Compute(uint8_t* InputMessage,
                          uint32_t InputMessageLength,
                          uint8_t *HMAC_key,
                          uint32_t HMAC_keyLength,
                          uint8_t *MessageDigest,
                          int32_t *MessageDigestLength)
{
  HMAC_SHA256ctx_stt HMAC_SHA256ctx;
  uint32_t error_status = HASH_SUCCESS;

  /* Set the size of the desired MAC*/
  HMAC_SHA256ctx.mTagSize = CRL_SHA256_SIZE;

  /* Set flag field to default value */
  HMAC_SHA256ctx.mFlags = E_HASH_DEFAULT;

  /* Set the key pointer in the context*/
  HMAC_SHA256ctx.pmKey = HMAC_key;

  /* Set the size of the key */
  HMAC_SHA256ctx.mKeySize = HMAC_keyLength;

  /* Initialize the context */
  error_status = HMAC_SHA256_Init(&HMAC_SHA256ctx);

  /* check for initialization errors */
  if (error_status == HASH_SUCCESS)
  {
    /* Add data to be hashed */
    error_status = HMAC_SHA256_Append(&HMAC_SHA256ctx,
                                    InputMessage,
                                    InputMessageLength);

    if (error_status == HASH_SUCCESS)
    {
      /* retrieve */
      error_status = HMAC_SHA256_Finish(&HMAC_SHA256ctx, MessageDigest, MessageDigestLength);
    }
  }

  return error_status;
}

/* URL ENCODE UTILITIES ------------------------------------------------------*/
/*
static char a2i(char ch) 
{
	return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}
*/
static char i2a(char code) 
{
	static char hex[] = "0123456789ABCDEF";
	
	return hex[code & 15];
}

char *urlencode( char *plain )
{
    char *pstr,
         *buf,
         *pbuf;

	pstr = plain;
    pbuf = buf = (char *)malloc( strlen(pstr) * 3 + 1 );

    while(*pstr)
    {
        if( isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~' )
        {
            *pbuf++ = *pstr;
        }
        else if( *pstr == ' ' )
        {
            *pbuf++ = '+';
        }
        else
        {
            *pbuf++ = '%',
            *pbuf++ = i2a(*pstr >> 4),
            *pbuf++ = i2a(*pstr & 15);
        }
		
        ++pstr;
    }
	
    *pbuf = '\0';

    return buf;
}
