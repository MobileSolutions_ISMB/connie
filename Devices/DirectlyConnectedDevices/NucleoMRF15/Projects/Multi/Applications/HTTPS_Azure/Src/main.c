  /**
  ******************************************************************************
  * @file    main.c
  * @author  Central LAB
  * @version V1.0.0
  * @date    17-May-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdio.h"
#include "string.h"
#include <math.h>
#include <time.h>   // time functions
#include <stdint.h>
  
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
#define WIFI_SCAN_BUFFER_SIZE           512
#define TIME            1
#define TIME_CLOSE      3
#define AZURE           2   
#define APPLICATION_DEBUG_MSG 1
#define EXPAND_POST


typedef struct {
  char strTemp[10];
  char strHum[10];
} EnvSens;

typedef struct {
  char accX[10];
  char accY[10];
  char accZ[10];
  char gyrX[10];
  char gyrY[10];
  char gyrZ[10];
} ImuSens;

void SystemClock_Config(void);

extern UART_HandleTypeDef UartHandle,UartMsgHandle;
extern char print_msg_buff[1024];

static uint8_t TimeSocket;
static uint8_t TlsSocket;

RTC_HandleTypeDef RtcHandle;
static int32_t last_ticks=0;
static int32_t ticks1 = 0;
static uint8_t request_type = 0;

static EnvSens HTReading;
static ImuSens IMUReading;

/* Private function prototypes -----------------------------------------------*/
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec);
static void RTC_Config(void);
static void RTC_TimeStampConfig(int32_t timeNow);
static void RTC_GetTimeStamp(char *timestr);
static void HumTemp_Sensor_Handler(EnvSens *sens);
static void AccGyro_Sensor_Handler(ImuSens *sens);

volatile float HUMIDITY_Value;
volatile float TEMPERATURE_Value;
volatile AxesRaw_TypeDef ACC_Value;
volatile AxesRaw_TypeDef GYR_Value;
char CurrTime[28];

/* Private functions ---------------------------------------------------------*/
#ifdef USART_PRINT_MSG
#define printf(arg)    sprintf((char*)print_msg_buff,arg);   \
                       HAL_UART_Transmit(&UartMsgHandle, (uint8_t*)print_msg_buff, strlen(print_msg_buff), 1000);
#endif                       

                                              
typedef enum {
  wifi_state_reset = 0,
  wifi_state_ready,
  wifi_state_idle,
  wifi_state_connected,
  wifi_state_connecting,
  wifi_state_disconnected,
  wifi_state_time_socket,
  wifi_open_tls,
  wifi_send_json,
  wifi_state_socket,
  wifi_state_error,
  wifi_undefine_state       = 0xFF,  
} wifi_state_t;

wifi_state_t wifi_state;
wifi_config config;

uint8_t * ssid = "sensortocloud";
uint8_t * seckey = "stm32mcu";

WiFi_Priv_Mode mode = WPA_Personal;

 /**
  * @brief  Main program
  * @param  None
  * @retval None
  */
 int main(void)
{    
  WiFi_Status_t status = WiFi_MODULE_SUCCESS;   
  uint32_t cnt=0;
  
  __GPIOA_CLK_ENABLE();
  HAL_Init();
  
  /* Configure the system clock to 64 MHz */
  SystemClock_Config();
  
  /* configure the timers  */
  Timer_Config( ); 
  
  UART_Configuration();
  
  #ifdef USART_PRINT_MSG
  UART_Msg_Gpio_Init();
  USART_PRINT_MSG_Configuration();
  #endif      
  
  printf("[Sensors to Cloud Demo]\r\n");
 
  /* Initialize RTC */
  RTC_Config();
  
  /* Initialize Temp Sensor */
  BSP_HUM_TEMP_Init();
  BSP_IMU_6AXES_Init();
  
  printf("[D] Initializing WiFi...");
 
  config.power=active;
  config.power_level=high;
  config.dhcp=on;               //use DHCP IP address
  config.web_server=TRUE;  
 
  wifi_state = wifi_undefine_state;
  
  /* Init the wi-fi module */  
  status = wifi_init(&config);
  
  if(status!=WiFi_MODULE_SUCCESS)
  {
    printf("[ERROR]\r\n");
    return 0;
  } else {
    printf("[OK]\r\n");
  }
  
  while (1)
  {
    switch (wifi_state) 
    {      
      case wifi_state_reset:
        printf("[D] Resetting WiFi\r\n");
        break;
      
      case wifi_state_ready:
        // Init structures for Event Hub Authentication
        EventHub_Init();
        
        printf("[D] Connecting to AP...");
        wifi_connect(ssid, seckey, mode);
        wifi_state = wifi_state_idle;
        break;

      case wifi_state_connected:
        printf("[OK]\r\n");        
        wifi_state = wifi_state_time_socket;
        break;
      
      case wifi_state_disconnected:
        printf("[D] AP connection lost\r\n");
        wifi_state = wifi_state_reset;
        break;
   
      case wifi_state_time_socket:
        printf("[D] Connecting to NTP server...");
        status = WiFi_MODULE_SUCCESS;
        
        // Get current TIME from NTP Server
        status = wifi_socket_client_open("time-d.nist.gov", 37, "t", &TimeSocket);
        if(status == WiFi_MODULE_SUCCESS) {  
          printf("[OK]\r\n");
          request_type = TIME;
          
          // Wait for NTP response
          printf("[D] Waiting for TIME response...");
          wifi_state = wifi_state_idle;
        } else {
          printf("[ERROR]\r\n");
        }
        break;
           
      case wifi_open_tls:
        printf("[D] Connecting to Azure Service Bus...");
        status = WiFi_MODULE_SUCCESS;
        
        // Open Secure Socket (TLS) towards Service Bus
        status = wifi_socket_client_open(ServiceBus, ServiceBusPort, "s", &TlsSocket);
        
        if(status == WiFi_MODULE_SUCCESS) {
          printf("[OK]\r\n");
          #ifdef NEW_CTD
            printf("Go to http://s2c.azurewebsites.net/ to visualize real-time data\r\n\r\n");
          #else
            printf("Go to http://mictoctd.azurewebsites.net to visualize real-time data\r\n\r\n");
          #endif
          wifi_state = wifi_send_json;
        } else {
          printf("[ERROR]\r\n");
        }
        break;
        
    case wifi_send_json: 
        {
          // Read sensors values from MEMS Shield
          HumTemp_Sensor_Handler((EnvSens *) &HTReading);
          AccGyro_Sensor_Handler((ImuSens *) &IMUReading);
          
          HAL_Delay(1000);
          
          // Get current timestamp from internal RTC
          RTC_GetTimeStamp(CurrTime);       
        
          #ifdef NEW_CTD
          // Prepare BODY for http post with parameters, in order:
          // date, temperature, humidity, 
          // accelerometer X, accelerometer Y, accelerometer Z,
          // gyroscope X, gyroscope Y, gyroscope Z
          char *body = PrepareBody(CurrTime, 
                                   HTReading.strTemp, HTReading.strHum, 
                                   IMUReading.accX, IMUReading.accY, IMUReading.accZ,
                                   IMUReading.gyrX, IMUReading.gyrY, IMUReading.gyrZ);
          #else
          char *body = PrepareBody(CurrTime, HTReading.strTemp);
          #endif
          
          // Prepare HTTP POST message
          char *post = PreparePost(strlen(body));
          
          // Prepare SAS HEADER in http post
          char *postSas = PrepareSAS();
        
          printf("[D] Send data...");
          #ifdef EXPAND_POST
            printf("\r\n");
          #endif
          // Send HTTP POST message
          status = wifi_socket_client_write(TlsSocket, strlen(post), post);
          #ifdef EXPAND_POST
            printf(post);
          #else
            printf("[POST]");
          #endif
        
          // Send SAS Token Header
          status = wifi_socket_client_write(TlsSocket, strlen(postSas), postSas);
          #ifdef EXPAND_POST
            printf(postSas);
          #else
            printf("[SAS]");
          #endif

          // Send BODY
          status = wifi_socket_client_write(TlsSocket, strlen(body), body);
          #ifdef EXPAND_POST
            printf(body);
            printf("\r\n");
          #else
            printf("[BODY]\r\n");
          #endif
        
          free(postSas);
          request_type = AZURE;       
          
          // Wait for Azure HTTP Response
          printf("[D] Waiting response...");
          wifi_state = wifi_state_idle;
          
        }
        break;

    case wifi_state_idle:
      printf(".");
      
      if(request_type == TIME_CLOSE){
        cnt++;
        // Timeout 5 sec
        if(cnt>10)
        {
          cnt = 0;
          printf("[TIMEOUT]\r\n    --> Forcibly close socket\r\n");
          wifi_state = wifi_open_tls;          
        }
      }
      HAL_Delay(500);
      break;    
        
    default:
      break;
    } 
    
  }
  
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSI)
  *            SYSCLK(Hz)                     = 64000000
  *            HCLK(Hz)                       = 64000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            PLLMUL                         = 16
  *            Flash Latency(WS)              = 2
  * @param  None
  * @retval None
  */

#ifdef USE_STM32F1xx_NUCLEO

void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef clkinitstruct = {0};
  RCC_OscInitTypeDef oscinitstruct = {0};
  
  /* Configure PLL ------------------------------------------------------*/
  /* PLL configuration: PLLCLK = (HSI / 2) * PLLMUL = (8 / 2) * 16 = 64 MHz */
  /* PREDIV1 configuration: PREDIV1CLK = PLLCLK / HSEPredivValue = 64 / 1 = 64 MHz */
  /* Enable HSI and activate PLL with HSi_DIV2 as source */
  oscinitstruct.OscillatorType  = RCC_OSCILLATORTYPE_HSI;
  oscinitstruct.HSEState        = RCC_HSE_OFF;
  oscinitstruct.LSEState        = RCC_LSE_OFF;
  oscinitstruct.HSIState        = RCC_HSI_ON;
  oscinitstruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  oscinitstruct.HSEPredivValue    = RCC_HSE_PREDIV_DIV1;
  oscinitstruct.PLL.PLLState    = RCC_PLL_ON;
  oscinitstruct.PLL.PLLSource   = RCC_PLLSOURCE_HSI_DIV2;
  oscinitstruct.PLL.PLLMUL      = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&oscinitstruct)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
  clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_2)!= HAL_OK)
  {
    /* Initialization Error */
    while(1); 
  }
}
#endif

#ifdef USE_STM32F4XX_NUCLEO

void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable Power Control clock */
    __PWR_CLK_ENABLE();

    /* The voltage scaling allows optimizing the power consumption when the device is
  clocked below the maximum system frequency, to update the voltage scaling value
  regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

    /* Enable HSE Oscillator and activate PLL with HSI as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = 16;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = 16;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    HAL_RCC_OscConfig(&RCC_OscInitStruct);

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
      clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
    
    /* Enable CRC clock */
    __CRC_CLK_ENABLE();
}
#endif

#ifdef USE_STM32L0XX_NUCLEO


/**
 * @brief  System Clock Configuration
 * @param  None
 * @retval None
 */
  void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  __PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);//RCC_CLOCKTYPE_SYSCLK;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;//RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1);

  __SYSCFG_CLK_ENABLE(); 
}
#endif


#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
 * @brief  Configures the RTC
 * @param  None
 * @retval None
 */
static void RTC_Config(void)
{
    /*##-1- Configure the RTC peripheral #######################################*/
    RtcHandle.Instance = RTC;

    /* Configure RTC prescaler and RTC data registers */
    /* RTC configured as follow:
  - Hour Format    = Format 12
  - Asynch Prediv  = Value according to source clock
  - Synch Prediv   = Value according to source clock
  - OutPut         = Output Disable
  - OutPutPolarity = High Polarity
  - OutPutType     = Open Drain */
    RtcHandle.Init.HourFormat = RTC_HOURFORMAT_12;
    RtcHandle.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
    RtcHandle.Init.SynchPrediv = RTC_SYNCH_PREDIV;
    RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
    RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

    if(HAL_RTC_Init(&RtcHandle) != HAL_OK)
    {
        /* Initialization Error */
        while(1);
    }
}

/**
 * @brief  Configures the current time and date.
 * @param  None
 * @retval None
 */
static void RTC_TimeStampConfig(int32_t timeNow)
{
    RTC_DateTypeDef sdatestructure;
    RTC_TimeTypeDef stimestructure;

    time_t now = timeNow;
    struct tm *calendar = gmtime(&now);
    
    /*##-3- Configure the Date #################################################*/
    sdatestructure.Year = calendar->tm_year - 100;
    sdatestructure.Month = calendar->tm_mon + 1;
    sdatestructure.Date = calendar->tm_mday;
    sdatestructure.WeekDay = calendar->tm_wday + 1;

    if(HAL_RTC_SetDate(&RtcHandle,&sdatestructure,FORMAT_BIN) != HAL_OK)
    {
        /* Initialization Error */
        while(1) ;
    }

    /*##-4- Configure the Time #################################################*/
    stimestructure.Hours = calendar->tm_hour;
    stimestructure.Minutes = calendar->tm_min;
    stimestructure.Seconds = calendar->tm_sec;
    stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
    stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE ;
    stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

    if(HAL_RTC_SetTime(&RtcHandle,&stimestructure,FORMAT_BIN) != HAL_OK)
    {
        /* Initialization Error */
        while(1) ;
    }
}


/**
 * @brief  Handles the HUM+TEMP axes data getting/sending
 * @param  Msg - HUM+TEMP part of the stream
 * @retval None
 */
static void HumTemp_Sensor_Handler(EnvSens *sens)
{
    int32_t d1, d2;

    if(BSP_HUM_TEMP_isInitialized()) {
        BSP_HUM_TEMP_GetHumidity((float *)&HUMIDITY_Value);
        BSP_HUM_TEMP_GetTemperature((float *)&TEMPERATURE_Value);
//        sprintf(sens->strHum, "%.2f", HUMIDITY_Value);
//        sprintf(sens->strTemp, "%.2f", TEMPERATURE_Value);
        
        floatToInt(HUMIDITY_Value, &d1, &d2, 2);
        sprintf(sens->strHum, "%d.%d", d1,d2);
        floatToInt(TEMPERATURE_Value, &d1, &d2, 2);
        sprintf(sens->strTemp, "%d.%d", d1,d2);
        
    }
    else {
        BSP_HUM_TEMP_Init();
    }
}

/**
 * @brief  Handles the ACC+GYRO axes data getting/sending
 * @param  Msg - ACC+GYRO part of the stream
 * @retval None
 */
static void AccGyro_Sensor_Handler(ImuSens *sens)
{
    if(BSP_IMU_6AXES_isInitialized()) {
        BSP_IMU_6AXES_X_GetAxesRaw((AxesRaw_TypeDef *) &ACC_Value);
        BSP_IMU_6AXES_G_GetAxesRaw((AxesRaw_TypeDef *) &GYR_Value);
        
        sprintf(sens->accX, "%d", ACC_Value.AXIS_X);
        sprintf(sens->accY, "%d", ACC_Value.AXIS_Y);
        sprintf(sens->accZ, "%d", ACC_Value.AXIS_Z);
        sprintf(sens->gyrX, "%d", GYR_Value.AXIS_X);
        sprintf(sens->gyrY, "%d", GYR_Value.AXIS_Y);
        sprintf(sens->gyrZ, "%d", GYR_Value.AXIS_Z);
    }
    else {
        BSP_IMU_6AXES_Init();
    }
}

static void RTC_GetTimeStamp(char *timestr) {
  RTC_DateTypeDef sdatestructureget;
  RTC_TimeTypeDef stimestructure;
  
  HAL_RTC_GetTime(&RtcHandle, &stimestructure, FORMAT_BIN);
  HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, FORMAT_BIN);
  
  sprintf(timestr, "20%02d-%02d-%02dT%02d:%02d:%02d.000000Z",
     sdatestructureget.Year, sdatestructureget.Month, sdatestructureget.Date,
     stimestructure.Hours+2, stimestructure.Minutes, stimestructure.Seconds);
}

/**
 * @brief  Get the current tick value in millisecond
 * @param  None
 * @retval The tick value
 */
uint32_t user_currentTimeGetTick(void)
{
    return HAL_GetTick();
}

/**
 * @brief  Get the delta tick value in millisecond from Tick1 to the current tick
 * @param  Tick1 the reference tick used to compute the delta
 * @retval The delta tick value
 */
uint32_t getElapsedMSFromLastGetTime()
{
    volatile uint32_t Delta, now;

    now = HAL_GetTick();

    /* Capture computation */
    Delta = now - ticks1;
    return Delta;
}

/**
 * @brief  Splits a float into two integer values.
 * @param  in the float value as input
 * @param  out_int the pointer to the integer part as output
 * @param  out_dec the pointer to the decimal part as output
 * @param  dec_prec the decimal precision to be used
 * @retval None
 */

static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec)
{
    *out_int = (int32_t)in;
    in = in - (float)(*out_int);
    *out_dec = (int32_t)trunc(in*pow(10,dec_prec));
}

/******** Wi-Fi Indication User Callback *********/

void ind_wifi_socket_data_received(uint8_t * data_ptr, uint32_t message_size, uint32_t chunck_size)
{ 
  if(request_type == TIME) {
    // NTP server response received
    last_ticks = ((data_ptr[0]<<24 )|(data_ptr[1]<<16)|(data_ptr[2]<<8)| data_ptr[3]) - 2208988800ul;
    RTC_TimeStampConfig(last_ticks);
    SASTokenHelper(last_ticks);
    
    printf("[OK]\r\n    --> SAS Token Generated\r\n");
    printf("[D] Waiting NTP socket close...");
    request_type = TIME_CLOSE;
    
  } else if(request_type == AZURE) {
    // HTTP Response received
    request_type = 0;
    
    printf("[OK]\r\n");
    printf((char *)data_ptr);
    
    wifi_state = wifi_send_json;
  }
}

void ind_wifi_socket_client_remote_server_closed(uint8_t * socket_closed_id)
{
  //uint8_t id = *socket_closed_id;
  if(request_type == TIME) {
    printf("[OK]\r\n");
    
    // Start procedure to open TLS socket
    wifi_state = wifi_open_tls;
    
  } else if(request_type == AZURE) {
    wifi_state = wifi_open_tls;
  }
}

void ind_wifi_on()
{
  //printf("\r\nwifi_ready callback\r\n");
  wifi_state = wifi_state_ready;
}

void ind_wifi_connected()
{
  wifi_state = wifi_state_connected;
}

void ind_wifi_resuming()
{
  printf("\r\nwifi resuming from sleep user callback... \r\n");
  //Change the state to connect to socket if not connected
  wifi_state = wifi_state_socket;
}

/**
  * @}
  */
  
/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
