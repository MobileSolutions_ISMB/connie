/**
  @page Wi-Fi Expansion Board for STM32 Nucleo Boards Applications
  
  @verbatim
  ******************** (C) COPYRIGHT 2015 STMicroelectronics *******************
  * @file    readme.txt  
  * @version V0.0.1
  * @date    08-05-2015
  * @brief   This application contains an example which shows how to use the 
			 client socket APIs available with the WIFI1 firmware. The APIs are used
			 to configure and use the Wi-Fi module in the following ways but not
			 limited to:
			 - Abstraction APIs to configure the module in STA, MiniAP and IBSS
			   mode
			 - Abstraction APIs to open/close, read/write sockets/socket servers
     		   in TCP/UDP mode
			 - Connecting to an AP in STA mode
			 - Restful APIs like HTTPGET and HTTPPUT
			 - File operations on webserver
			 - Reading and Configuring of module variables

  ******************************************************************************
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  @endverbatim

-----------
Readme.txt
-----------
This Readme.txt file describes some steps needed to overcome known bugs/issues.

---------
Contents
---------
In this folder there are several applications tested on both the STM32L053R8-Nucleo 
RevC, STM32F401RE-Nucleo RevC and the STM32F103RB-Nucleo RevC boards:
	- Client_Socket
	- HTTP_Request	
	- Server_Socket
	- WiFi_VCOM
	
To run these applications, the Wi-Fi expansion board (X-NUCLEO-IDW01M1) plugged
on the STM32L053R8-Nucleo/STM32F103RB-Nucleo is needed.

Please read the respective readme.txt file within the application folders to
understand each application and it's usage.

-----------
Known-Bugs
-----------

- None

Applications in folder Projects\Multi\Applications:
	-	Client_Socket
		Client scans the network and connects to desired AP if present. Client connects to server and sends and receives data. During connecting the client goes to sleep mode. Needs to be woken up by GPIO6 at the moment.
	-	Server_Socket
		Module is in mini-AP mode. PC application is client. Module opens a server socket and PC application connects to the server socket. PC application sends data and then closes the socket connection. Client receives callback for data reception and closed remote socket message.
	-	HTTP_Request
		Does a HTTP-GET request on local Apache webserver. User gets the data through callback. After HTTP-GET, the module performs a HTTP-POST request on http://posttestserver.com
	-	WiFi_VCOM
		AT command application directly to module through virtual serial com port


* <h3><center>&copy; COPYRIGHT STMicroelectronics</center></h3>
 */
