#include "dma.h"

/**
 * @brief  Get the DMA Stream pending flags
 * @param  handle_dma DMA handle
 * @retval The state of FLAG (SET or RESET)
 */
uint32_t Get_DMA_Flag_Status(DMA_HandleTypeDef *handle_dma)
{
    return (__HAL_DMA_GET_FLAG(handle_dma, __HAL_DMA_GET_FE_FLAG_INDEX(handle_dma)));
}

/**
 * @brief  Returns the number of remaining data units in the current DMAy Streamx transfer
 * @param  handle_dma DMA handle
 * @retval The number of remaining data units in the current DMA Stream transfer
 */
uint32_t Get_DMA_Counter(DMA_HandleTypeDef *handle_dma)
{
    return (__HAL_DMA_GET_COUNTER(handle_dma));
}

/**
 * @brief  Configure the DMA handler for transmission process
 * @param  handle_dma DMA handle
 * @retval None
 */
void Config_DMA_Handler(DMA_HandleTypeDef *handle_dma)
{
    handle_dma->Instance                 = USARTx_RX_DMA_STREAM;

    handle_dma->Init.Channel             = USARTx_RX_DMA_CHANNEL;
    handle_dma->Init.Direction           = DMA_PERIPH_TO_MEMORY;
    handle_dma->Init.PeriphInc           = DMA_PINC_DISABLE;
    handle_dma->Init.MemInc              = DMA_MINC_ENABLE;
    handle_dma->Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    handle_dma->Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    handle_dma->Init.Mode                = DMA_CIRCULAR;//DMA_CIRCULAR;
    handle_dma->Init.Priority            = DMA_PRIORITY_MEDIUM;
    handle_dma->Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
    handle_dma->Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
    handle_dma->Init.MemBurst            = DMA_MBURST_INC4;
    handle_dma->Init.PeriphBurst         = DMA_MBURST_INC4;
}