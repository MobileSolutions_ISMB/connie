#include <stm32f4xx_hal.h>
#include "commonIncludes.h"
#include "com.h"
#include "WiFiACKMe.h"
#include "EventHub.h"
#include "string.h"
#include "stdio.h"
#include "math.h"

#ifdef __cplusplus
extern "C"
#endif

typedef struct {
	char strTemp[10];
	char strHum[10];
} EnvSens;

/* Extern variables ----------------------------------------------------------*/
extern UART_HandleTypeDef UartHandle;

/* Private variables ---------------------------------------------------------*/
static WiFiState wifi_state = STARTING;
static ACKMeMsg AckMsg;
static EnvSens SensReading;

RTC_HandleTypeDef RtcHandle;
volatile uint32_t DataTxPeriod = 1000;
volatile uint32_t Int_Current_Time1 = 0;
volatile uint32_t Int_Current_Time2 = 0;
volatile float HUMIDITY_Value;
volatile float TEMPERATURE_Value;
char CurrTime[28];

/* Private function prototypes -----------------------------------------------*/
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec);
static void HumTemp_Sensor_Handler(EnvSens *sens);
static void RTC_Config(void);
static void RTC_TimeStampConfig(int32_t timeNow);
static char *RTC_GetTimeStamp(void);
uint32_t user_currentTimeGetTick(void);
uint32_t user_currentTimeGetElapsedMS(uint32_t Tick1);

void Error_Handler(void);
void SystemClock_Config(void);

void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
}

int main(void)
{
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Init structures for Event Hub Authentication */
	EventHub_Init();

	/* Initialize WiFi Module Reset GPIO */
	WiFiACKMe_Init();
	WiFiACKMe_Reset(GPIO_PIN_RESET);

	/* Initialize UART */
	USARTConfig();

	/* Initialize Temp Sensor */
	BSP_HUM_TEMP_Init();

	/*********************************** INITIALIZATION DONE! */
	WiFiACKMe_Reset(GPIO_PIN_SET);
	while (1) {
		switch (wifi_state) {
		case STARTING:
		{
			TMsg Msg;
			if (UART_ReceivedMSG((TMsg*)&Msg)) {          // Read Serial for messages
				if (startsWith(START_MSG_STR, (char *)(Msg.Data)))
					wifi_state = SET_SSID;
			}
			else {
				strcpy((char *)Msg.Data, "get system.version\n");
				Msg.Len = strlen((char *)Msg.Data);
				UART_SendMsg(&Msg);
				HAL_Delay(500);
			}
		}
		break;

		case SET_SSID:
			if (WiFiACKMe_SetSSID((ACKMeMsg *)&AckMsg) == RSP_CMD_OK)
				wifi_state = SET_PASSKEY;
			break;

		case SET_PASSKEY:
			if (WiFiACKMe_SetPASSKEY((ACKMeMsg *)&AckMsg) == RSP_CMD_OK)
				wifi_state = SET_NETWORK_UP;
			break;

		case SET_NETWORK_UP:
			if (WiFiACKMe_SetNetworkUP((ACKMeMsg *)&AckMsg) == RSP_CMD_OK)
				#ifdef TLS
				wifi_state = GET_TIME;
				#else
				wifi_state = XEND_DATA;
				#endif
			break;

		#ifdef TLS
		case GET_TIME:
			if (WiFiACKMe_GetUTC((ACKMeMsg *)&AckMsg, TIME_TICKS) == RSP_CMD_OK && AckMsg.Len >= 10) {
				SASTokenHelper(atoi((char *)AckMsg.Data));
				wifi_state = GET_TIME_UTC;
			}
			else
				HAL_Delay(500);
			break;

		case GET_TIME_UTC:
			if (WiFiACKMe_GetUTC((ACKMeMsg *)&AckMsg, TIME_UTC) == RSP_CMD_OK && AckMsg.Len >= 10) {
				strcpy(CurrTime, (char *)AckMsg.Data);
				wifi_state = XEND_DATA;
			}
			else
				HAL_Delay(500);
			break;
		#endif

		case XEND_DATA:
			#ifdef TLS
			if (WiFiACKMe_TlsClient((ACKMeMsg *)&AckMsg, (char *)ServiceBus, ServiceBusPort) == RSP_CMD_OK)
			{
				uint8_t stream = AckMsg.Data[0] - '0';
				char *body = PrepareBody(CurrTime, SensReading.strTemp);
				char *post = PreparePost(strlen(body));
				char *postSas = PrepareSAS();
				WiFiACKMe_StreamWrite((ACKMeMsg *)&AckMsg, stream, post);
				WiFiACKMe_StreamWrite((ACKMeMsg *)&AckMsg, stream, postSas);
				WiFiACKMe_StreamWrite((ACKMeMsg *)&AckMsg, stream, body);

				free(postSas);
				WiFiACKMe_StreamClose((ACKMeMsg *)&AckMsg, stream);

			}
			wifi_state = GET_TIME_UTC;
			#else 
			if (WiFiACKMe_TcpClient((ACKMeMsg *)&AckMsg, (char *)AmsProxy, 80) == RSP_CMD_OK)
			{
				uint8_t stream = AckMsg.Data[0] - '0';
				char *body = PrepareBody(SensReading.strTemp);
				char *post = PreparePost(strlen(body));
				WiFiACKMe_StreamWrite((ACKMeMsg *)&AckMsg, stream, post);
				WiFiACKMe_StreamWrite((ACKMeMsg *)&AckMsg, stream, body);
				WiFiACKMe_StreamClose((ACKMeMsg *)&AckMsg, stream);
			}
			#endif
			HAL_Delay(1000);
			break;

		default:
			break;


		}
	}
}

/**
* @brief  Splits a float into two integer values.
* @param  in the float value as input
* @param  out_int the pointer to the integer part as output
* @param  out_dec the pointer to the decimal part as output
* @param  dec_prec the decimal precision to be used
* @retval None
*/
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec)
{
	*out_int = (int32_t)in;
	in = in - (float)(*out_int);
	*out_dec = (int32_t)trunc(in*pow(10, dec_prec));
}

/**
* @brief  Handles the HUM+TEMP axes data getting/sending
* @param  Msg - HUM+TEMP part of the stream
* @retval None
*/
static void HumTemp_Sensor_Handler(EnvSens *sens)
{
	int32_t d1, d2, d3, d4;

	if (BSP_HUM_TEMP_isInitialized()) {
		BSP_HUM_TEMP_GetHumidity((float *)&HUMIDITY_Value);
		BSP_HUM_TEMP_GetTemperature((float *)&TEMPERATURE_Value);
		floatToInt(HUMIDITY_Value, &d1, &d2, 2);
		floatToInt(TEMPERATURE_Value, &d3, &d4, 2);
		sprintf(sens->strHum, "%d.%d", d1, d2);
		sprintf(sens->strTemp, "%d.%d", d3, d4);
	}

	else {
		BSP_HUM_TEMP_Init();
	}
}

void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable Power Control clock */
	__PWR_CLK_ENABLE();

	/* The voltage scaling allows optimizing the power consumption when the device is
	clocked below the maximum system frequency, to update the voltage scaling value
	regarding system frequency refer to product datasheet.  */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

	/* Enable HSE Oscillator and activate PLL with HSI as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 16;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
	clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

	/* Enable CRC clock */
	__CRC_CLK_ENABLE();
}

static void RTC_Config(void)
{
	/*##-1- Configure the RTC peripheral #######################################*/
	RtcHandle.Instance = RTC;

	/* Configure RTC prescaler and RTC data registers */
	/* RTC configured as follow:
	- Hour Format    = Format 12
	- Asynch Prediv  = Value according to source clock
	- Synch Prediv   = Value according to source clock
	- OutPut         = Output Disable
	- OutPutPolarity = High Polarity
	- OutPutType     = Open Drain */
	RtcHandle.Init.HourFormat = RTC_HOURFORMAT_12;
	RtcHandle.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
	RtcHandle.Init.SynchPrediv = RTC_SYNCH_PREDIV;
	RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
	RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

	if (HAL_RTC_Init(&RtcHandle) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}
}


/**
* @brief  Configures the current time and date.
* @param  None
* @retval None
*/
static void RTC_TimeStampConfig(int32_t timeNow)
{
	RTC_DateTypeDef sdatestructure;
	RTC_TimeTypeDef stimestructure;

	/*##-3- Configure the Date #################################################*/
	/* Set Date: Tuesday February 18th 2014 */
	sdatestructure.Year = 0x14;
	sdatestructure.Month = RTC_MONTH_FEBRUARY;
	sdatestructure.Date = 0x18;
	sdatestructure.WeekDay = RTC_WEEKDAY_TUESDAY;

	if (HAL_RTC_SetDate(&RtcHandle, &sdatestructure, FORMAT_BCD) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}

	/*##-4- Configure the Time #################################################*/
	/* Set Time: 08:10:00 */
	stimestructure.Hours = 0x08;
	stimestructure.Minutes = 0x10;
	stimestructure.Seconds = 0x00;
	stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
	stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

	if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, FORMAT_BCD) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}
}

static char *RTC_GetTimeStamp() {
	RTC_DateTypeDef sdatestructureget;
	RTC_TimeTypeDef stimestructure;
}

void Error_Handler(void)
{
	while (1)
	{
	}
}

uint32_t user_currentTimeGetTick(void)
{
	return HAL_GetTick();
}

uint32_t user_currentTimeGetElapsedMS(uint32_t Tick1)
{
	volatile uint32_t Delta, Tick2;

	Tick2 = HAL_GetTick();

	/* Capture computation */
	Delta = Tick2 - Tick1;
	return Delta;
}