#include <stm32f4xx_hal.h>
#include "commonIncludes.h"
#include "com.h"
#include "WiFi_ST.h"
#include "EventHub.h"
#include "string.h"
#include "stdio.h"
#include "math.h"
#include "time.h"

#ifdef __cplusplus
extern "C"
#endif

typedef struct {
	char strTemp[10];
	char strHum[10];
} EnvSens;

/* Extern variables ----------------------------------------------------------*/
extern UART_HandleTypeDef UartHandle;

/* Private variables ---------------------------------------------------------*/
static WiFiState wifi_state = STARTING;
static WindMsg WMsg;
static EnvSens SensReading;

static int8_t TimeSocket;
static int8_t TlsSocket;

RTC_HandleTypeDef RtcHandle;
volatile uint32_t DataTxPeriod = 50;
volatile uint32_t Int_Current_Time1 = 0;
volatile uint32_t Int_Current_Time2 = 0;
static int32_t last_ticks = 0;
static int32_t ticks1 = 0;
static uint8_t requesting_time = 0;

volatile float HUMIDITY_Value;
volatile float TEMPERATURE_Value;
char CurrTime[28];

/* Private function prototypes -----------------------------------------------*/
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec);
static void HumTemp_Sensor_Handler(EnvSens *sens);
static void RTC_Config(void);
static void RTC_TimeStampConfig(int32_t timeNow);
static void RTC_GetTimeStamp(char *timestr);
uint32_t user_currentTimeGetTick(void);
uint32_t getElapsedMSFromLastGetTime(void);

void Error_Handler(void);
void SystemClock_Config(void);

void SysTick_Handler(void)
{
	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
}

int main(void)
{
	uint8_t timeBytes;

	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Init structures for Event Hub Authentication */
	EventHub_Init();

	/* Initialize WiFi Module Reset GPIO */
	WiFi_ST_Init();
	WiFi_ST_Reset(GPIO_PIN_RESET);

	/* Initialize UART */
	USARTConfig();

	/* Initialize RTC */
	RTC_Config();

	/* Initialize Temp Sensor */
	BSP_HUM_TEMP_Init();

	/*********************************** INITIALIZATION DONE! */
	WiFi_ST_Reset(GPIO_PIN_SET);
	while (1) {
		TMsg Msg;

		if (getElapsedMSFromLastGetTime() > ((EXPIRE_GAP - 1) * 60 * 1000) && !requesting_time) {
			wifi_state = WIFI_UP;
		}

		if (UART_ReceivedMSG((TMsg*)&Msg) && ParseWindMsg(&Msg, &WMsg) >= 0) {
			switch (wifi_state) {
			case STARTING:
				if (WMsg.wind_type == WIND_SCAN_COMPLETE) {
					WiFi_ST_Associate(SSID, PASSKEY);
					wifi_state = ASSOCIATING;
				}
				else if (WMsg.wind_type == WIND_WIFI_JOIN) {
					wifi_state = ASSOCIATING;
				}

				break;

			case ASSOCIATING:
				if (WMsg.wind_type == WIND_WIFI_UP) {
					wifi_state = WIFI_UP;
				}
				break;

			case GET_TIME:
				if(WMsg.wind_type == WIND_PENDING_DATA) {
					timeBytes = atoi((char *)WMsg.extMessage + 2);
					wifi_state = GET_TIME_BYTES;
			}
				break;
			case GET_TIME_BYTES:
				if(WMsg.wind_type == WIND_SOCKET_CLOSED) {
					if(WiFi_ST_ReadSocket(TimeSocket, timeBytes, &Msg)== RSP_CMD_OK) {
						// Time received;
						last_ticks = ((Msg.Data[0]<<24 )|(Msg.Data[1]<<16)|(Msg.Data[2]<<8)| Msg.Data[3]) - 2208988800ul;
						RTC_TimeStampConfig(last_ticks);
						SASTokenHelper(last_ticks);
						wifi_state = XEND_DATA;
					}
				}
				break;

			case DATA_SENT:
				if(WMsg.wind_type == WIND_PENDING_DATA) {
					TMsg Msg;
					uint16_t queuedBytes, remaining;
					if(WiFi_ST_QueryPending(TlsSocket, &queuedBytes) == RSP_CMD_OK) {
						remaining = queuedBytes;

						// Read response
						do {
							int16_t toRead = (int16_t) fmin(TMsg_MaxLen-1, queuedBytes);
							WiFi_ST_ReadSocket(TlsSocket, toRead, &Msg);
							remaining-=toRead;
						}while(remaining > 0);

						WiFi_ST_CloseSocket(TlsSocket);
						wifi_state = XEND_DATA;                  
					}
				} else if(WMsg.wind_type == WIND_WIFI_UP) {
					wifi_state = WIFI_UP;
				}
				break;
			}
		}

		switch (wifi_state) {
			case WIFI_UP:
				if (WiFi_ST_OpenSocket("time-d.nist.gov", 37, 't', &TimeSocket) == RSP_CMD_OK) {
					wifi_state = GET_TIME;
					requesting_time = 1;
				}
				break;

			case XEND_DATA: {
					uint8_t retVal;
					HumTemp_Sensor_Handler((EnvSens *)&SensReading);

					retVal = WiFi_ST_OpenSocket(ServiceBus, ServiceBusPort, 's', &TlsSocket);
					if (retVal == RSP_CMD_OK){
						RTC_GetTimeStamp(CurrTime);
						char *body = PrepareBody(CurrTime, SensReading.strTemp);
						char *post = PreparePost(strlen(body));
						char *postSas = PrepareSAS();
						WiFi_ST_WriteSocket(TlsSocket, strlen(post), post);
						WiFi_ST_WriteSocket(TlsSocket, strlen(postSas), postSas);
						WiFi_ST_WriteSocket(TlsSocket, strlen(body), body);
						free(postSas);
						wifi_state = DATA_SENT;
					}
					else if (retVal == RSP_CMD_WIND) {
						wifi_state = STARTING;
					}
				}
				break;
		}
	}
}

/**
* @brief  Splits a float into two integer values.
* @param  in the float value as input
* @param  out_int the pointer to the integer part as output
* @param  out_dec the pointer to the decimal part as output
* @param  dec_prec the decimal precision to be used
* @retval None
*/
static void floatToInt(float in, int32_t *out_int, int32_t *out_dec, int32_t dec_prec)
{
	*out_int = (int32_t)in;
	in = in - (float)(*out_int);
	*out_dec = (int32_t)trunc(in*pow(10, dec_prec));
}

/**
* @brief  Handles the HUM+TEMP axes data getting/sending
* @param  Msg - HUM+TEMP part of the stream
* @retval None
*/
static void HumTemp_Sensor_Handler(EnvSens *sens)
{
	int32_t d1, d2, d3, d4;

	if (BSP_HUM_TEMP_isInitialized()) {
		BSP_HUM_TEMP_GetHumidity((float *)&HUMIDITY_Value);
		BSP_HUM_TEMP_GetTemperature((float *)&TEMPERATURE_Value);
		floatToInt(HUMIDITY_Value, &d1, &d2, 2);
		floatToInt(TEMPERATURE_Value, &d3, &d4, 2);
		sprintf(sens->strHum, "%d.%d", d1, d2);
		sprintf(sens->strTemp, "%d.%d", d3, d4);
	}

	else {
		BSP_HUM_TEMP_Init();
	}
}

void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable Power Control clock */
	__PWR_CLK_ENABLE();

	/* The voltage scaling allows optimizing the power consumption when the device is
	clocked below the maximum system frequency, to update the voltage scaling value
	regarding system frequency refer to product datasheet.  */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);

	/* Enable HSE Oscillator and activate PLL with HSI as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 16;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
	clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
	HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);

	/* Enable CRC clock */
	__CRC_CLK_ENABLE();
}

static void RTC_Config(void)
{
	/*##-1- Configure the RTC peripheral #######################################*/
	RtcHandle.Instance = RTC;

	/* Configure RTC prescaler and RTC data registers */
	/* RTC configured as follow:
	- Hour Format    = Format 12
	- Asynch Prediv  = Value according to source clock
	- Synch Prediv   = Value according to source clock
	- OutPut         = Output Disable
	- OutPutPolarity = High Polarity
	- OutPutType     = Open Drain */
	RtcHandle.Init.HourFormat = RTC_HOURFORMAT_12;
	RtcHandle.Init.AsynchPrediv = RTC_ASYNCH_PREDIV;
	RtcHandle.Init.SynchPrediv = RTC_SYNCH_PREDIV;
	RtcHandle.Init.OutPut = RTC_OUTPUT_DISABLE;
	RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	RtcHandle.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;

	if (HAL_RTC_Init(&RtcHandle) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}
}

static void RTC_TimeStampConfig(int32_t timeNow)
{
	RTC_DateTypeDef sdatestructure;
	RTC_TimeTypeDef stimestructure;

	time_t now = timeNow;
	struct tm *calendar = gmtime(&now);

	/*##-3- Configure the Date #################################################*/
	sdatestructure.Year = calendar->tm_year - 100;
	sdatestructure.Month = calendar->tm_mon + 1;
	sdatestructure.Date = calendar->tm_mday;
	sdatestructure.WeekDay = calendar->tm_wday + 1;

	if (HAL_RTC_SetDate(&RtcHandle, &sdatestructure, FORMAT_BIN) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}

	/*##-4- Configure the Time #################################################*/
	stimestructure.Hours = calendar->tm_hour;
	stimestructure.Minutes = calendar->tm_min;
	stimestructure.Seconds = calendar->tm_sec;
	stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
	stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

	if (HAL_RTC_SetTime(&RtcHandle, &stimestructure, FORMAT_BIN) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}
}

static void RTC_GetTimeStamp(char *timestr) {
	RTC_DateTypeDef sdatestructureget;
	RTC_TimeTypeDef stimestructure;

	HAL_RTC_GetTime(&RtcHandle, &stimestructure, FORMAT_BIN);
	HAL_RTC_GetDate(&RtcHandle, &sdatestructureget, FORMAT_BIN);

	sprintf(timestr, "20%02d-%02d-%02dT%02d:%02d:%02d.000000Z",
		sdatestructureget.Year, sdatestructureget.Month, sdatestructureget.Date,
		stimestructure.Hours, stimestructure.Minutes, stimestructure.Seconds);
}

void Error_Handler(void)
{
	while (1)
	{
	}
}

uint32_t user_currentTimeGetTick(void)
{
	return HAL_GetTick();
}

uint32_t getElapsedMSFromLastGetTime()
{
	volatile uint32_t Delta, now;

	now = HAL_GetTick();

	/* Capture computation */
	Delta = now - ticks1;
	return Delta;
}