/* Includes ------------------------------------------------------------------*/
#include "EventHub.h"

/* Exported variables --------------------------------------------------------*/
char *postUri;

#ifdef TLS
  char *stringToSign;
  char signature[45];
  char *SAS;
#endif

char post[450];
char jsonBuilder[350];

/* Private variables ---------------------------------------------------------*/

/* Private prototypes --------------------------------------------------------*/
#ifdef TLS
int32_t STM32_SHA256_HMAC_Compute(uint8_t* InputMessage,
                          uint32_t InputMessageLength,
                          uint8_t *HMAC_key,
                          uint32_t HMAC_keyLength,
                          uint8_t *MessageDigest,
                          int32_t *MessageDigestLength);
char a2i(char ch);
char i2a(char code);
char *urlencode( char *plain );
#endif

void EventHub_Init(void) {
#ifdef TLS
  // build postUri and stringToSign once and for all
  postUri = (char *) malloc((strlen(EventHubName) + strlen(DisplayName) + strlen("//publishers//messages"))*sizeof(char));
  sprintf(postUri, "/%s/publishers/%s/messages", EventHubName, DisplayName);
  stringToSign = (char *) malloc((strlen(ServiceBus) + strlen(postUri)+ strlen("https://"))*sizeof(char));
  sprintf(stringToSign, "https://%s%s", ServiceBus, postUri);
#else
  postUri = (char *) malloc((1+ strlen(AmsApi))*sizeof(char));  // +1 because of the trailing '/'
  sprintf(postUri, "/%s", AmsApi);
#endif
  
}

#ifdef TLS
int32_t SASTokenHelper(uint32_t timeNow) {
  char extStrToSign[150];
  uint8_t MAC[CRL_SHA256_SIZE];
  int32_t MACLength = 0;
  int32_t expire = timeNow + (EXPIRE_GAP * 60);
  char *enc = urlencode(stringToSign);
  sprintf(extStrToSign, "%s\n%d", enc, expire); 
  
  //Crypto_DeInit();
  
  int32_t status = STM32_SHA256_HMAC_Compute((uint8_t*)extStrToSign,
                               strlen(extStrToSign),
                               (uint8_t*)Key,
                               strlen(Key),
                               (uint8_t*)MAC,
                               &MACLength);
  
  Base64encode(signature, (char *)MAC, MACLength);
  
  if(status == HASH_SUCCESS) {
    char *enc2 = urlencode(signature);
    SAS = (char *) malloc((strlen(enc)+strlen(enc2)+10+3)* sizeof(char));
    sprintf(SAS, "sr=%s&sig=%s&se=%d&skn=%s", enc, enc2, expire, KeyName); 
    free(enc);
    free(enc2);
    return 0;
  }
  else {
    free(enc);
    return -1;
  }
}
#endif

char *PreparePost(int bodyLen) {
#ifdef TLS
  sprintf(post, str_post, 
          postUri, 
          ServiceBus,
          bodyLen);
#else
  sprintf(post, str_post, 
          postUri, 
          AmsProxy,
          AmsKey,
          bodyLen);
#endif
  
  return post;
}

char *PrepareBody(
          #ifdef TLS                  
                  char *date, 
          #endif
                  char *value) {
    //int length = strlen(json)+strlen(DisplayName)+strlen(date) + strlen(value); 
    //char *jsonBuilder = (char *) malloc(length * sizeof(char));
    sprintf(jsonBuilder, json, DisplayName, 
          #ifdef TLS
            date, 
          #endif
            value);
    return jsonBuilder;
}

#ifdef TLS
char *PrepareSAS() {
    char *postSas = (char *) malloc((strlen(SAS) + strlen(str_sas))*sizeof(char));
    sprintf(postSas, str_sas, SAS);
    return postSas;
}
#endif


/* Private Functions ---------------------------------------------------------*/

#ifdef TLS
int32_t STM32_SHA256_HMAC_Compute(uint8_t* InputMessage,
                          uint32_t InputMessageLength,
                          uint8_t *HMAC_key,
                          uint32_t HMAC_keyLength,
                          uint8_t *MessageDigest,
                          int32_t *MessageDigestLength)
{
  HMAC_SHA256ctx_stt HMAC_SHA256ctx;
  uint32_t error_status = HASH_SUCCESS;

  /* Set the size of the desired MAC*/
  HMAC_SHA256ctx.mTagSize = CRL_SHA256_SIZE;

  /* Set flag field to default value */
  HMAC_SHA256ctx.mFlags = E_HASH_DEFAULT;

  /* Set the key pointer in the context*/
  HMAC_SHA256ctx.pmKey = HMAC_key;

  /* Set the size of the key */
  HMAC_SHA256ctx.mKeySize = HMAC_keyLength;

  /* Initialize the context */
  error_status = HMAC_SHA256_Init(&HMAC_SHA256ctx);

  /* check for initialization errors */
  if (error_status == HASH_SUCCESS)
  {
    /* Add data to be hashed */
    error_status = HMAC_SHA256_Append(&HMAC_SHA256ctx,
                                    InputMessage,
                                    InputMessageLength);

    if (error_status == HASH_SUCCESS)
    {
      /* retrieve */
      error_status = HMAC_SHA256_Finish(&HMAC_SHA256ctx, MessageDigest, MessageDigestLength);
    }
  }

  return error_status;
}

/* URL ENCODE UTILITIES ------------------------------------------------------*/
char a2i(char ch) 
{
	return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

char i2a(char code) 
{
	static char hex[] = "0123456789ABCDEF";
	
	return hex[code & 15];
}

char *urlencode( char *plain )
{
    char *pstr,
         *buf,
         *pbuf;

	pstr = plain;
    pbuf = buf = (char *)malloc( strlen(pstr) * 3 + 1 );

    while(*pstr)
    {
        if( isalnum(*pstr) || *pstr == '-' || *pstr == '_' || *pstr == '.' || *pstr == '~' )
        {
            *pbuf++ = *pstr;
        }
        else if( *pstr == ' ' )
        {
            *pbuf++ = '+';
        }
        else
        {
            *pbuf++ = '%',
            *pbuf++ = i2a(*pstr >> 4),
            *pbuf++ = i2a(*pstr & 15);
        }
		
        ++pstr;
    }
	
    *pbuf = '\0';

    return buf;
}
#endif