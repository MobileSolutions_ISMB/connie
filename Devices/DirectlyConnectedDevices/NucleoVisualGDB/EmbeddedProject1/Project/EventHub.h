/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __EVENTHUB_H__
#define __EVENTHUB_H__  

#ifdef __cplusplus
 extern "C" {
#endif

#ifdef TLS
#include "crypto.h"
#include "base64.h"
#endif

#include <string.h>
#include <stdio.h>   
#include <stdlib.h>
#include <ctype.h>


   
typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;

#ifdef TLS
  #define EXPIRE_GAP      20                      // Minutes of validity of token
  #define HMAC_LENGTH 32

  #define ServiceBus     "micto-ehctd.servicebus.windows.net"
  #define ServiceBusPort  443
  #define EventHubName   "ehdevices"
  #define KeyName        "D1"
  #define Key            "pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ="
#else
  #define AmsProxy       "micto-msctd.azure-mobile.net"
  #define AmsApi         "api/proxy"
  #define AmsKey         "fcozekPuZuLPcZByrSkWAoUHUliQNr52"
#endif

#define DisplayName    "NucleoF4"

#ifdef TLS
  #define json "{\"guid\": \"4122a248-e2f9-4438-ab23-82a3930662ab\","\
                "\"organization\": \"ISMB\","\
                "\"displayname\": \"%s\","\
                "\"location\": \"Torino\","\
                "\"measurename\": \"Temperature\","\
                "\"unitofmeasure\": \"C\","\
                "\"timecreated\": \"%s\","\
                "\"value\": %s }\n"
               
  #define str_post "POST %s HTTP/1.1\n"\
                 "Host: %s\n"\
                 "Cache-Control: no-cache\n"\
                 "Content-Type: application/json; charset=UTF-8\n"\
                 "Content-Length: %d\n"
#else 
  #define json "{\"guid\": \"4122a248-e2f9-4438-ab23-82a3930662ab\","\
                "\"organization\": \"ISMB\","\
                "\"displayname\": \"%s\","\
                "\"location\": \"Torino\","\
                "\"measurename\": \"Temperature\","\
                "\"unitofmeasure\": \"C\","\
                "\"value\": %s }\n"
                  
  #define str_post "POST %s HTTP/1.1\n"\
                 "Host: %s\n"\
                 "Cache-Control: no-cache\n"\
                 "X-ZUMO-APPLICATION: %s\n"\
                 "Content-Type: application/json\n"\
                 "Content-Length: %d\n"\
                 "\n"
#endif
                                    
#ifdef TLS                   
  #define str_sas  "Authorization: SharedAccessSignature %s\n"\
                   "\n"
#endif
   
extern char *postUri;

#ifdef TLS
  extern char *stringToSign;
  extern char signature[];
  extern char *SAS;
#endif

extern char post[];
extern char jsonBuilder[];

void EventHub_Init(void);
char *PreparePost(int bodyLen);
char *PrepareBody(
                #ifdef TLS
                  char *date, 
                #endif
                  char *value);

#ifdef TLS
char *PrepareSAS(void);
int32_t SASTokenHelper(uint32_t timeNow);
#endif

#ifdef __cplusplus
 }
#endif

#endif