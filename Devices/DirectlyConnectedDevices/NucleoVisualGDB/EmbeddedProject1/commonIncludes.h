#ifdef USE_STM32F4XX_NUCLEO

	#include "stm32f4xx_hal.h"
	#include "stm32f4xx_nucleo.h"
	#include "stm32f4xx_hal_conf.h"
	#include "stm32f4xx_hal_def.h"
	#include "x_nucleo_iks01a1.h"
	#include "x_nucleo_iks01a1_imu_6axes.h"
	#include "x_nucleo_iks01a1_magneto.h"
	#include "x_nucleo_iks01a1_pressure.h"
	#include "x_nucleo_iks01a1_hum_temp.h"

	#define RTC_CLOCK_SOURCE_LSI

	#ifdef RTC_CLOCK_SOURCE_LSI
		#define RTC_ASYNCH_PREDIV  0x7F
		#define RTC_SYNCH_PREDIV   0x0130
	#endif

	#ifdef RTC_CLOCK_SOURCE_LSE
		#define RTC_ASYNCH_PREDIV  0x7F
		#define RTC_SYNCH_PREDIV   0x00FF
	#endif

#endif