## Teoria di funzionamento ##
Il seguente esempio prevede l'utilizzo di una board **Nucleo F4**, della sensor board **ST MEMS** e della board **ST WiFi**

Il progetto è stato creato con Visual Studio 2013 Community Edition e il plugin **VisualGDB**. 

La sensor board ST MEMS utilizza vari sensori (ad es. giroscopio, accelerometro e magnetometro), tra cui un sensore di umidità e temperatura. Tale sensore utilizza il bus **I2C** per la comunicazione con il microprocessore.

Per una **descrizione dettagliata della board ST MEMS** utilizzata fare riferimento al sito ufficiale [[X-NUCLEO-IKS010A](http://www.st.com/web/catalog/tools/FM116/SC1248/PF261191)]

### Schema Logico ###
![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/VisualGDB/18.png) 

Data la presenza di librerie adatte all'utilizzo di una connessione HTTP sicura, i dati del sensore verranno inviati **direttamente** all'EventHub Azure.

L'applicazione di esempio provvede a leggere a intervalli regolari (circa 2 sec.) tramite il bus I2C il valore di temperatura dal sensore, crea il messaggio JSON e lo invia tramite una chiamata **HTTPS POST** all'EventHub.

Inoltre, avendo a disposizione un RTC che gestisce l'orologio interno del microprocessore, è possibile includere direttamente nel pacchetto JSON il timestamp corrente. Nell'esempio è stata inclusa la libreria crittografica utilizzata per la generazione del **SAS Token tramite HMAC-SHA256**, il quale viene aggiunto agli header standard della richiesta HTTPS. 

> Per maggiori informazioni sulla generazione del **SAS Token** clicca [qui](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/SAS.md)


###### Esempio di HTTPS POST da Nucleo F4 a EventHub ######
	POST /<eventHub>/publishers/<device>/messages HTTP/1.1
	Host: <eventHub>.servicebus.windows.net
	Content-Type: application/json; charset=UTF-8
	Content-Length: <body length>
	Authorization: SharedAccessSignature <sas string>
	
	{"guid": "<guid>","organization": "ISMB","displayname": "NucleoF4","location": "Torino","measurename": "Temperature","unitofmeasure": "C","timecreated": "2015-06-19T07:52:37.003712Z","value": 26.43 }

Nell'esempio soprastante è necessario sostituire tutte le stringhe comprese tra < e > con dei valori validi.

### Requisiti Hardware ###
- ST Nucleo F401RE [[Specifiche tecniche](http://www.st.com/web/catalog/tools/FM116/SC959/SS1532/LN1847/PF260000)]
- ST X-NUCLEO-IKS010A [[Specifiche tecniche](http://www.st.com/web/catalog/tools/FM116/SC1248/PF261191)]
- ST WiFi Module [[Specifiche tecniche](http://www.st.com/web/catalog/sense_power/FM2185/SC1930/PF258591)]
- Connessione a internet tramite access point WiFi

### Requisiti Software ###
- Visual Studio 2013 Community Edition
- VisualGDB [[Guida all'installazione](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/VisualGDB/readme.md)]

