﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationIOT
{
    public enum SensorType
    {
        TEMPERATURE,
        HUMIDITY,
    }

    public class BoardSensor
    {
        const int MAX_NUMBER_SENSORS = 2;
        double[] arraySensorList;

        int counterChangesTemperature;

        public BoardSensor()
        {
            arraySensorList = new double[MAX_NUMBER_SENSORS];

            for(int index=0;index<arraySensorList.Length;index++)
            {
                arraySensorList[index] = 25;
            }
        }

        public double GetSensorValue(SensorType typeSensor)
        {
            switch(typeSensor)
            {
                case SensorType.TEMPERATURE:
                    arraySensorList[0]+= 0.1;

                    counterChangesTemperature++;

                    if(counterChangesTemperature>5)
                    {
                        arraySensorList[0] = 25;
                        counterChangesTemperature = 0;
                    }

                    return arraySensorList[0];

                case SensorType.HUMIDITY:
                    arraySensorList[1] += 1.0;
                    return arraySensorList[1];
            }
            return 0;
        }
    }
}
