﻿#define AUTOMATIC_STARTUP_APPLICATION
using System;
using System.Collections.Generic;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Threading.Tasks;
using ConnectedTheDots;
using HALLibrary;
using CommonTypes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ApplicationIOT
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        #region Attributes.Private
        private RuntimeApplication  runtimeApplication                      = null;
        private DispatcherTimer     timerGUIUpdate                          = null;
        private string              defaultTimeZone                         = "Central Europe Standard Time";
        private string              defaultGUID                             = "00-01-02-03-04-05";
        private SettingsRuntime     settingsRunTime                         = new SettingsRuntime();
        private bool                isGUIDAlreadyUpdated                    = false;
        private IDictionary<String, SerializableStyle> dictionaryJSONStyle  = new Dictionary<String, SerializableStyle>()
        {
            {"Connected The Dots",SerializableStyle.CONNECTEDTHEDOTS},
            {"Custom Style",SerializableStyle.CUSTOMSENDING }
        };
        #endregion Attributes

        #region Constructors
        public MainPage()
        {
            this.InitializeComponent();

            this.tbGUIDBoard.Text   = defaultGUID;

            UpdateStatus(Directory.GetCurrentDirectory());

            List<String> timeZones = TimeUtility.GetListTimeZones();

            foreach (string timezone in timeZones)
                comboBoxTimeZone.Items.Add(timezone);

            comboBoxJSONFormat.Items.Clear();

            foreach (string jsonStyle in dictionaryJSONStyle.Keys)
            {
                comboBoxJSONFormat.Items.Add(jsonStyle);
            }
            if (timeZones.Contains(defaultTimeZone))
            {
                comboBoxTimeZone.SelectedValue = defaultTimeZone;
            }

            UpdateGUIWithSettings(settingsRunTime);

            #if AUTOMATIC_STARTUP_APPLICATION
                var task    = AutomaticStartupApplication();
                //var result  = task.WaitAndUnwrapException();
            #endif
            timerGUIUpdate          = new DispatcherTimer();
            timerGUIUpdate.Interval = TimeSpan.FromMilliseconds(1000);
            timerGUIUpdate.Tick     += Timer_Tick;
            timerGUIUpdate.Start();
        }
        #endregion Constructor

        #region Methods.Private.AutomaticStartup

#if AUTOMATIC_STARTUP_APPLICATION
        private async Task<bool> AutomaticStartupApplication()
        {
            try
            {
                String stringReturn = await GetMACAddress();
                UpdateMACAddress(stringReturn);
                Start();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

#endif

        #endregion Methods.Private.AutomaticStartup

        #region Methods.Private.GraphicEvents
        private void ClickGUID(object sender, RoutedEventArgs e)
        {
            var returnString = GetMACAddress();
        }

        private void ClickStart(object sender, RoutedEventArgs e)
        {
            Start();
        }
        private void ClickStop(object sender, RoutedEventArgs e)
        {
            Stop();
        }
        #endregion Methods.Private.GraphicEvents

        #region Methods.Public.RuntimeApplication
        public void Start()
        {
            try
            {
                if (runtimeApplication == null)
                {
                    runtimeApplication = new RuntimeApplication();

                    UpdateSettingsRunTime(ref settingsRunTime);

                    runtimeApplication.Run(settingsRunTime);
                }
            }
            catch (Exception)
            {
            }
        }

        public void Stop()
        {
            if (runtimeApplication != null)
            {
                runtimeApplication.Stop();

                runtimeApplication = null;

                UpdateStatus("APPLICATION STOPPED");
            }
        }
        #endregion Methods.Public.RuntimeApplication

        #region Methods.Private.SystemUtility
        private async System.Threading.Tasks.Task<String> GetMACAddress()
        {
            String ResultMACAddress = "";

            if (OSHALMiddleware.MacAddress == null || OSHALMiddleware.MacAddress.Length == 0)
            {
                System.Threading.Tasks.Task<String> getMacAddress = OSHALMiddleware.GetMAC();

                ResultMACAddress = await getMacAddress;

                return ResultMACAddress;
            }
            return OSHALMiddleware.MacAddress;
        }
        #endregion Methods.Private.SystemUtility

        #region Methods.Private.GUITask
        private void SetupTime(object sender, RoutedEventArgs e)
        {
            string selectedTimeZone = comboBoxTimeZone.SelectedItem.ToString();

            if (selectedTimeZone.Length > 0)
            {
                TimeUtility.SetTimeZone(selectedTimeZone);
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            if(runtimeApplication!=null)
            {
                UpdateStatus("CLOUD TRANSFER SUCCESS: " + runtimeApplication.CounterSuccess.ToString()+" FAILED: "+ runtimeApplication.CounterFailure.ToString());
                UpdateTemperature(runtimeApplication.GetLastAvailableSensorMeasure(SensorType.TEMPERATURE));
                UpdateTimeStamp(TimeUtility.GetLocalTime());

                DequeueTraceMessages();

            }
            if (OSHALMiddleware.MacAddress != null && OSHALMiddleware.MacAddress.Length >0 && isGUIDAlreadyUpdated==false)
            {
                UpdateMACAddress(OSHALMiddleware.MacAddress);

                isGUIDAlreadyUpdated = true;
            }
        }

        private void DequeueTraceMessages()
        {
            if(runtimeApplication!=null)
            {
                if(runtimeApplication.traceActivityMessage.Count>0)
                {
                    String line = runtimeApplication.traceActivityMessage.Dequeue();

                    TraceDebug(line);
                }
            }
        }

        #endregion Methods.Private.GUITask

        #region Methods.Private.Settings2GUIInteraction
        private void UpdateGUIWithSettings(SettingsRuntime settingsRuntime)
        {
            tbSR_eventHubName.Text      = settingsRunTime.eventHubName;
            tbSR_key.Text               = settingsRunTime.key;
            tbSR_Location.Text          = settingsRunTime.location;
            tbSR_Organization.Text      = settingsRunTime.organization;
            tbSR_serviceBusName.Text    = settingsRunTime.serviceBusNamespace;
            tbSR_keyName.Text           = settingsRunTime.keyName;
            tbSR_DisplayName.Text       = settingsRunTime.displayName;
            tbIntervalTransmission.Text = settingsRunTime.intervalTransmission.ToString();

            foreach (string jsonStyle in dictionaryJSONStyle.Keys)
            {
                SerializableStyle st = dictionaryJSONStyle[jsonStyle];

                if(st==settingsRuntime.serializableStyle)
                {
                    comboBoxJSONFormat.SelectedValue = jsonStyle;
                    break;
                }
            }
        }

        private void UpdateSettingsRunTime(ref SettingsRuntime settingsRunTime)
        {
            int     valueInteger = 3000;
            String  labelSerializable;

            if(tbIntervalTransmission.Text.Length>0)
            {
                valueInteger = Int32.Parse(tbIntervalTransmission.Text);
            }

            labelSerializable = comboBoxJSONFormat.SelectedValue.ToString();

            settingsRunTime.intervalTransmission    = valueInteger;
            settingsRunTime.guid                    = this.tbGUIDBoard.Text;
            settingsRunTime.eventHubName            = tbSR_eventHubName.Text  ;
            settingsRunTime.key                     = tbSR_key.Text           ;
            settingsRunTime.location                = tbSR_Location.Text      ;
            settingsRunTime.organization            = tbSR_Organization.Text  ;
            settingsRunTime.serviceBusNamespace     = tbSR_serviceBusName.Text;
            settingsRunTime.keyName                 = tbSR_keyName.Text;
            settingsRunTime.displayName             = tbSR_DisplayName.Text;

            if (dictionaryJSONStyle.ContainsKey(labelSerializable))
            {
                settingsRunTime.serializableStyle = dictionaryJSONStyle[labelSerializable];
            }
        }

        private void onJSONFormatChanged(object sender, SelectionChangedEventArgs e)
        {
            SerializableStyle styleToApply;

            styleToApply = dictionaryJSONStyle[comboBoxJSONFormat.SelectedValue.ToString()];


            if (settingsRunTime.serializableStyle != styleToApply)
            {
                ContainerRuntimeSettingsDefault.ChangeRuntimeSettingsWithDefaultConfiguration(ref settingsRunTime, styleToApply);

                settingsRunTime.serializableStyle = styleToApply;

                UpdateGUIWithSettings(settingsRunTime);
            }
        }
        #endregion Methods.Private.Settings2GUIInteraction

        #region Methods.Private.GUIInteraction
        private void UpdateStatus(string line)
        {
            tbStatus.Text = line;
        }

        private void TraceDebug(string line)
        {
            String newText = "";
            String OldText = "";

            if(line.EndsWith("\r\n")==false)
            {
                line += "\r\n";
            }

            OldText = textBoxTrace.Text;

            if ((OldText.Length > 300 * 1024))
            {
                textBoxTrace.Text = "";
            }

            newText             = line + textBoxTrace.Text;
            textBoxTrace.Text   = newText;
        }

        private void UpdateTemperature(double temperature)
        {
            tbTemperature.Text = temperature.ToString();
        }

        private void UpdateTimeStamp(DateTime dateTime)
        {
            tbTimeStamp.Text = dateTime.ToString();
        }

        private void UpdateMACAddress(string MACAddress)
        {
            tbGUIDBoard.Text = MACAddress;
        }
        private void ClickClearTrace(object sender, RoutedEventArgs e)
        {
            textBoxTrace.Text = "";
        }
        #endregion Methods.Private.GUIInteraction


    }
}
