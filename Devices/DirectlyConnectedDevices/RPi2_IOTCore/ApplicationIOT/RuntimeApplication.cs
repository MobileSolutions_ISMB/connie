﻿// Copyright (c) Microsoft. All rights reserved.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.System.Threading;
using ConnectedTheDots;
using HALLibrary;
using CommonTypes;

namespace ApplicationIOT
{
    public sealed class RuntimeApplication
    {
        #region Attributes.Private
        private ThreadPoolTimer                             timer                   = null;
        private ConnectTheDotsHelper                        ctdHelper               = null;
        private SettingsRuntime                             settingsRunTime         = null;
        private ISensorsJSONSerializeable                   customBoardSensors      = null;
        private BoardSensors                                 boardSensors            = null;
        private List<SensorType>                            listSensorType          = null;
        private Dictionary<SensorType, SensorDescriptor>    dictionaryDescriptor    = null;
        public  Queue<String>                               traceActivityMessage    = new Queue<string>();
        #endregion Attributes.Private

        #region Properties
        public int CounterSuccess
        {
            get
            {
                if(ctdHelper!=null)
                {
                    return ctdHelper.CounterSuccess;
                }

                return 0;
            }
        }
        public int CounterFailure
        {
            get
            {
                if (ctdHelper != null)
                {
                    return ctdHelper.CounterFailure;
                }

                return 0;
            }
        }
        #endregion Properties

        #region Constructor
        public RuntimeApplication()
        {
            listSensorType          = new List<SensorType>();
            dictionaryDescriptor    = new Dictionary<SensorType, SensorDescriptor>();
            settingsRunTime         = new SettingsRuntime();

            listSensorType.Add(SensorType.TEMPERATURE);
            //listSensorType.Add(SensorType.HUMIDITY);

            foreach(SensorType sensorType in listSensorType)
            {
                switch(sensorType)
                {
                    case SensorType.TEMPERATURE:
                        dictionaryDescriptor.Add(SensorType.TEMPERATURE, new SensorDescriptor("Temperature", "C"));
                    break;

                    case SensorType.HUMIDITY:
                        dictionaryDescriptor.Add(SensorType.HUMIDITY, new SensorDescriptor("Humidity", "%"));
                    break;
                }
            }

            boardSensors = new BoardSensors(listSensorType);
        }
        #endregion Constructor

        #region PublicMethods
        public double GetSensorMeasure(SensorType sensorType)
        {
            return boardSensors.GetSensorValue(sensorType);
        }

        public double GetLastAvailableSensorMeasure(SensorType sensorType)
        {
            return boardSensors.GetLastSensorValue(sensorType);
        }

        public void Run(SettingsRuntime settingsRunTime)
        {
            try
            {
                if (ctdHelper == null && timer == null)
                {
                    if (settingsRunTime != null)
                    {
                        this.settingsRunTime.Copy(settingsRunTime);
                    }

                    switch (settingsRunTime.serializableStyle)
                    {
                        case SerializableStyle.CONNECTEDTHEDOTS:

                            customBoardSensors = new BoardSensorsCTDStyle();

                            break;
                        case SerializableStyle.CUSTOMSENDING:
                            customBoardSensors = new CustomBoardSensorsParser();

                            ((CustomBoardSensorsParser)customBoardSensors).mtype = "ins";//FIXME: RIVEDERE
                            break;
                        default:
                            break;
                    }

                    if (customBoardSensors != null)
                    {
                        foreach (SensorType sensorType in dictionaryDescriptor.Keys)
                        {
                            SensorDescriptor sensorDescriptor = dictionaryDescriptor[sensorType];

                            customBoardSensors.AddSensor(sensorType, sensorDescriptor);
                        }
                    }

                    if (customBoardSensors != null)
                    {
                        customBoardSensors.Initialize(new SettingsBoardSensors(settingsRunTime.guid, settingsRunTime.location, settingsRunTime.displayName, settingsRunTime.organization));
                    }

                    ctdHelper = new ConnectTheDotsHelper(
                        serviceBusNamespace: this.settingsRunTime.serviceBusNamespace,
                        eventHubName: this.settingsRunTime.eventHubName,
                        keyName: this.settingsRunTime.keyName,
                        key: this.settingsRunTime.key,
                        displayName: this.settingsRunTime.displayName,//"RaspBerryPI2MAC"+guid,
                        organization: this.settingsRunTime.organization,
                        location: this.settingsRunTime.location,
                        sensorsBoardList: customBoardSensors
                        );

                    timer = ThreadPoolTimer.CreatePeriodicTimer(EventTimerSendingInformation, TimeSpan.FromMilliseconds(settingsRunTime.intervalTransmission));

                    System.Diagnostics.Debug.WriteLine("LAUNCHED TASK");
                }
            }
            catch (Exception)
            {

            }
        }

        public void Stop()
        {
            if(timer!=null)
            {
                timer.Cancel();

                timer       = null;
                ctdHelper   = null;
            }

            if(boardSensors!=null)
            {
                boardSensors.Dispose();

                boardSensors = null;
            }
        }

        #endregion PublicMethods

        #region Callback.Internals
        private void EventTimerSendingInformation(ThreadPoolTimer timer)
        {
            try
            {
                double valueSensor;

                foreach (SensorType sensorType in dictionaryDescriptor.Keys)
                {
                    SensorDescriptor sensorDescriptor = dictionaryDescriptor[sensorType];

                    valueSensor = boardSensors.GetSensorValue(sensorType);

                    if (customBoardSensors != null)
                    {
                        customBoardSensors.SetSensorValue(sensorType, valueSensor, TimeUtility.GetLocalTime());
                    }
                }
                if (customBoardSensors != null)
                {
                    ctdHelper.SendAllSensorData();
                    System.Diagnostics.Debug.WriteLine("Sent Board Sensors Information To Cloud\r\n");
                }

                while(ctdHelper.debuggerView.Count>0)
                {
                    String lineDebug = ctdHelper.debuggerView.Dequeue();

                    traceActivityMessage.Enqueue(lineDebug);
                }

            }
            catch (Exception exc)
            {
                System.Diagnostics.Debug.WriteLine(exc.Message);

                traceActivityMessage.Enqueue(exc.Message);
            }
        }
        #endregion Callback.Internals
    }
}
