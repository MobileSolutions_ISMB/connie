﻿//#define STYLE_JSON_CONNECTEDTHEDOTS
#define STYLE_JSON_CUSTOM

using System;
using System.Collections.Generic;
using CommonTypes;

namespace ApplicationIOT
{
    public static class ContainerRuntimeSettingsDefault
    {
        static IDictionary<String, String> settingsContainerCTDStandard = new Dictionary<String, String>();
        static IDictionary<String, String> settingsContainerCustom      = new Dictionary<String, String>();

        static IDictionary<String,String>  selectedDictionary           = null;

        static ContainerRuntimeSettingsDefault()
        {
            settingsContainerCTDStandard.Add("serviceBusNamespace", "micto-ehctd");
            settingsContainerCTDStandard.Add("eventHubName", "ehdevices");
            settingsContainerCTDStandard.Add("keyName", "D1");
            settingsContainerCTDStandard.Add("key", "pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ=");
            settingsContainerCTDStandard.Add("guid", "00-11-22-33-44-55");
            settingsContainerCTDStandard.Add("displayName", "RaspBerryPI2W10IOT");
            settingsContainerCTDStandard.Add("organization", "ISMB");
            settingsContainerCTDStandard.Add("location", "TORINO");

            settingsContainerCustom.Add("serviceBusNamespace", "ctd-maker-fair");
            settingsContainerCustom.Add("eventHubName", "ehdevices");
            settingsContainerCustom.Add("keyName", "D1");
            settingsContainerCustom.Add("key", "+TJcXijDiC0OOtPP2WQ6gnLhyIqWO5iFW44Z1eKgiGw=");
            settingsContainerCustom.Add("guid", "00-11-22-33-44-55");
            settingsContainerCustom.Add("displayName", "RaspBerryPI2W10IOT");
            settingsContainerCustom.Add("organization", "ISMB");
            settingsContainerCustom.Add("location", "TORINO");

            #if STYLE_JSON_CONNECTEDTHEDOTS
                selectedDictionary = settingsContainerCTDStandard;
            #endif

            #if STYLE_JSON_CUSTOM
                selectedDictionary = settingsContainerCustom;

            #endif
        }

        public static void ChangeRuntimeSettingsWithDefaultConfiguration(ref SettingsRuntime settingsRuntime, SerializableStyle style)
        {
            try
            {
                switch (style)
                {
                    case SerializableStyle.CONNECTEDTHEDOTS:
                        selectedDictionary = settingsContainerCTDStandard;
                        break;
                    case SerializableStyle.CUSTOMSENDING:
                        selectedDictionary = settingsContainerCustom;
                        break;
                    default:
                        break;
                }

                ApplyDictionaryParameters(ref settingsRuntime, selectedDictionary);
            }
            catch (Exception)
            {

            }
        }

        private static void ApplyDictionaryParameters(ref SettingsRuntime settingsRuntime,IDictionary<String,String> selectedDictionary)
        {
            if(selectedDictionary!=null && settingsRuntime!=null)
            {
                settingsRuntime.serviceBusNamespace     = selectedDictionary["serviceBusNamespace"];
                settingsRuntime.eventHubName            = selectedDictionary["eventHubName"];
                settingsRuntime.keyName                 = selectedDictionary["keyName"];
                settingsRuntime.key                     = selectedDictionary["key"];
                settingsRuntime.guid                    = selectedDictionary["guid"];
                settingsRuntime.displayName             = selectedDictionary["displayName"];
                settingsRuntime.organization            = selectedDictionary["organization"];
                settingsRuntime.location                = selectedDictionary["location"];
            }
        }
    }


    public class SettingsRuntime
    {
#if STYLE_JSON_CONNECTEDTHEDOTS
        public string serviceBusNamespace           = "micto-ehctd";
        public string eventHubName                  = "ehdevices";
        public string keyName                       = "D1";
        public string key                           = "pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ=";
        public string guid                          = "00-11-22-33-44-55";
        public string displayName                   = "RaspBerryPI2W10IOT";
        public string organization                  = "ISMB";
        public string location                      = "TORINO";
        public SerializableStyle serializableStyle  = SerializableStyle.CONNECTEDTHEDOTS;
#endif

#if STYLE_JSON_CUSTOM
        //public string serviceBusNamespace   = "micto-maker-fair";
        //public string eventHubName          = "ehdevices";
        //public string keyName               = "D1";
        //public string key                   = "nO5k0cT9A3WiqziMExhWPK5EYwD2T4M9nEE5uEOdWGE=";//FIXME: ORIGINALE
        //public string key                   = "Y4YXNNG9z1RjENo5XVpwS8toN5TdJ/XUmdHiTN7NoQE=";
        //public string guid                  = "00-11-22-33-44-55";
        //public string displayName           = "RaspBerryPI2W10IOT";
        //public string organization          = "ISMB";
        //public string location              = "TORINO";


        public string serviceBusNamespace   = "ctd-maker-fair";
        public string eventHubName          = "ehdevices";
        public string keyName               = "D1";
        //public string key                   = "nO5k0cT9A3WiqziMExhWPK5EYwD2T4M9nEE5uEOdWGE=";//FIXME: ORIGINALE
        public string key                   = "+TJcXijDiC0OOtPP2WQ6gnLhyIqWO5iFW44Z1eKgiGw=";
        public string guid                  = "00-11-22-33-44-55";
        public string displayName           = "RaspBerryPI2W10IOT";
        public string organization          = "ISMB";
        public string location              = "TORINO";

        public SerializableStyle serializableStyle  = SerializableStyle.CUSTOMSENDING;
#endif


        public int intervalTransmission = 3000;


        public SettingsRuntime()
        {

        }

        public SettingsRuntime(string serviceBusNamespace,
            string eventHubName,
            string keyName,
            string key,
            string guid,
            string displayName,
            string organization,
            string location,
            int intervalTransmission,
            SerializableStyle serializableStyle
            )
        {
            this.serviceBusNamespace = serviceBusNamespace;
            this.eventHubName = eventHubName;
            this.keyName = keyName;
            this.guid = guid;
            this.displayName = displayName;
            this.key = key;
            this.organization = organization;
            this.location = location;
            this.intervalTransmission = intervalTransmission;
            this.serializableStyle = serializableStyle;
        }

        public void Copy(SettingsRuntime extRuntime)
        {
            this.serviceBusNamespace = extRuntime.serviceBusNamespace;
            this.eventHubName = extRuntime.eventHubName;
            this.keyName = extRuntime.keyName;
            this.key = extRuntime.key;
            this.displayName = extRuntime.displayName;
            this.guid = extRuntime.guid;
            this.organization = extRuntime.organization;
            this.location = extRuntime.location;
            this.intervalTransmission = extRuntime.intervalTransmission;
            this.serializableStyle = extRuntime.serializableStyle;
        }
    }
}
