﻿namespace CommonTypes
{
    public enum SensorType
    {
        TEMPERATURE,
        HUMIDITY,
    }

    public enum SerializableStyle
    {
        CONNECTEDTHEDOTS,
        CUSTOMSENDING
    }
}
