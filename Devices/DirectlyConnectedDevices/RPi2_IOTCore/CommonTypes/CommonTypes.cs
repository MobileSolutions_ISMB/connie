﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonTypes
{
    public class SensorDescriptor
    {
        public string nameSensor;
        public string measureUnit;

        public SensorDescriptor(string nameSensor, string measureUnit)
        {
            this.nameSensor = nameSensor;
            this.measureUnit = measureUnit;
        }
    }
}
