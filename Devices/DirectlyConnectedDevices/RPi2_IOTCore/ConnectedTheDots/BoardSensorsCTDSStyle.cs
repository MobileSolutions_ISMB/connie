﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using CommonTypes;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ConnectedTheDots
{
    /// <summary>
    /// Class to manage sensor data and attributes 
    /// </summary>
    public sealed class BoardSensorsCTDStyle : ISensorsJSONSerializeable
    {
        #region Properties
        public string guid { get; set; }
        public string displayname { get; set; }
        public string organization { get; set; }
        public string location { get; set; }
        public string measurename { get; set; }
        public string unitofmeasure { get; set; }
        public string timecreated { get; set; }
        public double value { get; set; }

        [IgnoreDataMember]
        //private IList<SensorType> listSensorValues = new List<SensorType>();
        public int test { get; set; } 

        #endregion Properties


        #region Constructor
        /// <summary>
        /// Default parameterless constructor needed for serialization of the objects of this class
        /// </summary>
        public BoardSensorsCTDStyle()
        {
        }
        #endregion Constructor

        #region Methods.Public.ISensorsJSONSerializeable
        /// <summary>
        /// ToJson function is used to convert sensor data into a JSON string to be sent to Azure Event Hub
        /// </summary>
        /// <returns>JSon String containing all info for sensor data</returns>
        public string ToJson()
        {
            DataContractJsonSerializer  ser     = new DataContractJsonSerializer(typeof(BoardSensorsCTDStyle));
            MemoryStream                ms      = new MemoryStream();
            string                      json;

            ser.WriteObject(ms, this);
            
            json = Encoding.UTF8.GetString(ms.ToArray(), 0, (int)ms.Length);

            return json;
        }

        public void Initialize(SettingsBoardSensors settings)
        {
            this.guid           = settings.id;
            this.organization   = settings.organization;
            this.displayname    = settings.displayname;
            this.location       = settings.location;
        }

        public void AddSensor(SensorType sensorType, SensorDescriptor sensorDescription)
        {
            this.measurename    = sensorDescription.nameSensor;
            this.unitofmeasure  = sensorDescription.measureUnit;
        }

        public void SetSensorValue(SensorType sensorType, double value, DateTime timeCreated)
        {
            this.value          = value;
            this.timecreated    = timeCreated.ToString("o");
        }
        #endregion Methods.Public.ISensorsJSONSerializeable
    }
}
