﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Web.Http;

namespace ConnectedTheDots
{
    public sealed class ConnectTheDotsHelper
    {
        #region Constants
        const int MINUTE_TOKENVALIDITY = 20;//F
        #endregion Constants

        #region Attributes.Private
        // App Settings variables
        private AppSettings                 localSettings;
        private ISensorsJSONSerializeable   boardListSensorsSerializeable;

        // Http connection string, SAS tokem and client
        private Uri             uri;
        private string          sas;
        private HttpClient      httpClient;
        private DateTime        requestTokenTime;

        private bool            EventHubConnectionInitialized   = false;
        private int             counterSuccess                  = 0;
        private int             counterFailure                  = 0;
        #endregion Attributes.Private

        #region Attributes.Public
        public Queue<String>   debuggerView = new Queue<string>();
        #endregion Attributes.Public

        #region Properties
        public int CounterSuccess
        {
            get
            {
                return counterSuccess;
            }
        }
        public int CounterFailure
        {
            get
            {
                return counterFailure;
            }
        }
        #endregion Properties

        #region Constructor
        public ConnectTheDotsHelper(string serviceBusNamespace,
            string eventHubName,
            string keyName,
            string key,
            string displayName,
            string organization,
            string location,
            ISensorsJSONSerializeable sensorsBoardList
            )
        {
            localSettings = new AppSettings();

            localSettings.ServicebusNamespace = serviceBusNamespace;
            localSettings.EventHubName = eventHubName;
            localSettings.KeyName = keyName;
            localSettings.Key = key;
            localSettings.DisplayName = displayName;
            localSettings.Organization = organization;
            localSettings.Location = location;

            this.boardListSensorsSerializeable = sensorsBoardList;

            httpClient = new HttpClient();

            SaveSettings();
        }

        #endregion Constructor

        #region Methods.Public.Settings
        public AppSettings getSettings()
        {
            return localSettings;
        }

        /// <summary>
        /// Validate the settings 
        /// </summary>
        /// <returns></returns>
        public bool ValidateSettings()
        {
            if ((localSettings.ServicebusNamespace == "") ||
                (localSettings.EventHubName == "") ||
                (localSettings.KeyName == "") ||
                (localSettings.Key == "") ||
                (localSettings.DisplayName == "") ||
                (localSettings.Organization == "") ||
                (localSettings.Location == ""))
            {
                this.localSettings.SettingsSet = false;
                return false;
            }

            this.localSettings.SettingsSet = true;
            return true;

        }

        /// <summary>
        /// Apply new settings to sensors collection
        /// </summary>
        public bool SaveSettings()
        {
            if (ValidateSettings())
            {
                this.InitEventHubConnection();
                return true;
            } else {
                return false;
            }
        }
        #endregion Methods.Public.Settings

        #region Methods.Public.Sensors
        public ISensorsJSONSerializeable getBoardSensors()
        {
            return boardListSensorsSerializeable;
        }
        #endregion Methods.Public.Sensors

        #region Methods.Public.CloudTransmission
        public void SendAllSensorData()
        {
            if(CheckTokenExpired())
            {
                RequestAuthorization();
            }

            if (boardListSensorsSerializeable!=null)
            {
                var Task = sendMessage(boardListSensorsSerializeable.ToJson());
            }
        }
        /// <summary>
        /// Send message to Azure Event Hub using HTTP/REST API
        /// </summary>
        /// <param name="message"></param>
        public async Task<String> sendMessage(string message)
        {
            String returnString = "";
            if (this.EventHubConnectionInitialized)
            {
                try
                {
                    HttpStringContent content = new HttpStringContent(message, Windows.Storage.Streams.UnicodeEncoding.Utf8, "application/json");
                    HttpResponseMessage postResult = await httpClient.PostAsync(uri, content);

                    returnString = postResult.ReasonPhrase;

                    if (postResult.IsSuccessStatusCode)
                    {
                        PrintDebug(String.Format("Message Sent: {0}", content));

                        counterSuccess++;
                    }
                    else
                    {
                        PrintDebug(String.Format("Failed sending message: {0}", postResult.ReasonPhrase));
                        counterFailure++;
                    }
                }
                catch (Exception e)
                {
                    PrintDebug(String.Format("Exception when sending message:" + e.Message));
                    returnString = e.Message;

                    counterFailure++;
                }
            }
            return returnString;
        }
        #endregion Methods.Public.CloudTransmission

        #region Methods.Private.CloudUtilities
        private bool CheckTokenExpired()
        {
            if ((DateTime.UtcNow - requestTokenTime).TotalMinutes > (double) MINUTE_TOKENVALIDITY)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Helper function to get SAS token for connecting to Azure Event Hub
        /// </summary>
        /// <returns></returns>
        private string SASTokenHelper(DateTime timeToken,int minuteExpiration)
        {
            int expiry          = (int)timeToken.AddMinutes(minuteExpiration).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            string stringToSign = WebUtility.UrlEncode(this.uri.ToString()) + "\n" + expiry.ToString();
            string signature    = HmacSha256(this.localSettings.Key.ToString(), stringToSign);
            string token        = String.Format("sr={0}&sig={1}&se={2}&skn={3}", WebUtility.UrlEncode(this.uri.ToString()), WebUtility.UrlEncode(signature), expiry, this.localSettings.KeyName.ToString());

            return token;
        }

        /// <summary>
        /// Because Windows.Security.Cryptography.Core.MacAlgorithmNames.HmacSha256 doesn't
        /// exist in WP8.1 context we need to do another implementation
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private string HmacSha256(string key, string valueToCheck)
        {
            var keyStrm = CryptographicBuffer.ConvertStringToBinary(key, BinaryStringEncoding.Utf8);
            var valueStrm = CryptographicBuffer.ConvertStringToBinary(valueToCheck, BinaryStringEncoding.Utf8);

            var objMacProv = MacAlgorithmProvider.OpenAlgorithm(MacAlgorithmNames.HmacSha256);
            var hash = objMacProv.CreateHash(keyStrm);
            hash.Append(valueStrm);

            return CryptographicBuffer.EncodeToBase64String(hash.GetValueAndReset());
        }

        /// <summary>
        /// Initialize Event Hub connection
        /// </summary>
        private bool InitEventHubConnection()
        {
            try
            {
                this.uri = new Uri("https://" + this.localSettings.ServicebusNamespace +
                              ".servicebus.windows.net/" + this.localSettings.EventHubName +
                              "/publishers/" + this.localSettings.DisplayName + "/messages");

                this.EventHubConnectionInitialized = true;

                RequestAuthorization();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool RequestAuthorization()
        {
            requestTokenTime = DateTime.UtcNow;

            this.sas = SASTokenHelper(DateTime.UtcNow, MINUTE_TOKENVALIDITY);

            this.httpClient.DefaultRequestHeaders.Authorization = new Windows.Web.Http.Headers.HttpCredentialsHeaderValue("SharedAccessSignature", sas);

            PrintDebug("SET AUTHORIZATION");

            return true;
        }
        #endregion Methods.Private.CloudUtilities

        #region Methods.Private.Utilities.Write
        private void PrintDebug(String PrintDebug)
        {
            Debug.WriteLine(PrintDebug);

            debuggerView.Enqueue(PrintDebug);
        }
        #endregion Methods.Private.Utilities.Write
    }
}
