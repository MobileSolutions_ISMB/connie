﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using CommonTypes;

namespace ConnectedTheDots
{
    public sealed class CustomBoardSensorsParser: ISensorsJSONSerializeable
    {
        #region Properties
        public string id { get; set; }
        public string name { get; set; }
        public string ts { get; set; }
        public string mtype { get; set; }
        public double temp { get; set; }
        public double hum { get; set; }
        public double accX { get; set; }
        public double accY { get; set; }
        public double accZ { get; set; }
        public double gyrX { get; set; }
        public double gyrY { get; set; }
        public double gyrZ { get; set; }
        #endregion Properties

        #region Constructor
        public CustomBoardSensorsParser()
        {

        }
        #endregion Constructor

        #region Methods.Public.ISensorsJSONSerializeable
        /// <summary>
        /// ToJson function is used to convert sensor data into a JSON string to be sent to Azure Event Hub
        /// </summary>
        /// <returns>JSon String containing all info for sensor data</returns>
        public string ToJson()
        {
            DataContractJsonSerializer  ser  = new DataContractJsonSerializer(typeof(CustomBoardSensorsParser));
            MemoryStream                ms   = new MemoryStream();

            ser.WriteObject(ms, this);
            string json                     = Encoding.UTF8.GetString(ms.ToArray(), 0, (int)ms.Length);

            return json;
        }

        public void Initialize(SettingsBoardSensors settings)
        {
            this.id     = settings.id;
            this.name   = settings.displayname;
        }

        public void AddSensor(SensorType sensorType, SensorDescriptor sensorDescription)
        {
            //FIXME: EMPTY
        }

        public void SetSensorValue(SensorType sensorType, double value, DateTime timeCreated)
        {
            switch(sensorType)
            {
                case SensorType.TEMPERATURE:
                    this.temp = value;
                break;

                case SensorType.HUMIDITY:
                    this.hum = value;
                break;
            }

            this.ts = timeCreated.ToString("o");
        }
        #endregion Methods.Public.ISensorsJSONSerializeable
    }
}
