﻿using System;
using CommonTypes;

namespace ConnectedTheDots
{
    public interface ISensorsJSONSerializeable
    {
        void    Initialize(SettingsBoardSensors settings);
        void    AddSensor(SensorType sensorType, SensorDescriptor sensorDescription);
        void    SetSensorValue(SensorType sensorType,double value,DateTime timeCreated);
        string  ToJson();
    }
}
