﻿using System;
using System.Collections.Generic;

namespace ConnectedTheDots
{
    public static class TimeUtility
    {
        #region Attributes.Public
        static String timeZoneSelected = "Central Europe Standard Time";
        #endregion Attributes.Public

        #region Methods.Public
        public static DateTime GetLocalTime()
        {
            DateTime databaseUtcTime = DateTime.Now;
            var localTime = TimeZoneInfo.FindSystemTimeZoneById(TimeUtility.timeZoneSelected);
            //var japaneseTime        = TimeZoneInfo.ConvertTimeFromUtc(databaseUtcTime, japaneseTimeZone);

            databaseUtcTime = TimeZoneInfo.ConvertTime(databaseUtcTime, localTime);

            return databaseUtcTime;
        }

        public static List<String> GetListTimeZones()
        {
            List < String > returnList = new List<string>();

            foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                returnList.Add(z.Id);

            return returnList;
        }

        public static void SetTimeZone(string localTimeZone)
        {
            TimeUtility.timeZoneSelected = localTimeZone;
        }
        #endregion Methods.Public
    }
}
