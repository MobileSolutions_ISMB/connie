﻿using System;
using System.Collections.Generic;
using CommonTypes;

namespace HALLibrary
{
    public class BoardSensors
    {
        #region Attributes.Private.Collections
        Dictionary<SensorType, InterfaceSensor> collectionSensors;
        Dictionary<SensorType, double>          collectionSensorsValues;
        #endregion Attributes.Private.Collections

        #region Attributes.Private.Sensors
        TemperatureSensorI2C temperatureSensor;

        #if FAKE_SENSOR_HUMIDITY
        SensorHumidity sensorHumidity;
        #endif

        #endregion Attributes.Private.Sensors

        #region Constructor
        public BoardSensors(List<SensorType> listSensorsTypes)
        {
            collectionSensorsValues = new Dictionary<SensorType, double>();

            collectionSensorsValues.Add(SensorType.TEMPERATURE, 0);
            collectionSensorsValues.Add(SensorType.HUMIDITY, 0);

            collectionSensorsValues[SensorType.TEMPERATURE] = 0;

            collectionSensors = new Dictionary<SensorType, InterfaceSensor>();

            foreach (SensorType sensorType in listSensorsTypes)
            {
                switch (sensorType)
                {
                    case SensorType.TEMPERATURE:
                        temperatureSensor = new TemperatureSensorI2C();

                        temperatureSensor.Initialize();

                        collectionSensors.Add(SensorType.TEMPERATURE, temperatureSensor);
                    break;

                    case SensorType.HUMIDITY:
                        #if FAKE_SENSOR_HUMIDITY
                        sensorHumidity = new SensorHumidity();
                        collectionSensors.Add(SensorType.HUMIDITY, sensorHumidity);
                        #endif

                        break;
                }
            }
        }
        #endregion Constructor

        #region Methods.Public
        public void Dispose()
        {
            try
            {
                InterfaceSensor sensor;
                foreach (SensorType sensorType in collectionSensors.Keys)
                {
                    sensor = collectionSensors[sensorType];

                    if (sensor != null)
                    {
                        sensor.DetachSensor();
                    }
                }
            }
            catch(Exception)
            {

            }
        }

        public double GetSensorValue(SensorType typeSensor)
        {
            InterfaceSensor sensor = null;
            double          returnValue;

            if (collectionSensors.ContainsKey(typeSensor))
            {
                sensor = collectionSensors[typeSensor];

                if (sensor != null)
                {
                    sensor.GetSensorValue(out returnValue);

                    collectionSensorsValues[typeSensor] = returnValue;

                    return returnValue;
                }
            }

            return 0;
        }

        public double GetLastSensorValue(SensorType typeSensor)//FIXME: THIS METHOD IS CALLED FROM GRAPHIC INTERFACE (SUCH APPROACH MUST BE MODIFIED)
        {
            return collectionSensorsValues[typeSensor];
        }
        #endregion Methods.Public
    }
}
