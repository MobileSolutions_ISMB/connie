﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Windows.Data.Json;

namespace HALLibrary
{
    public static class OSHALMiddleware
    {
        #region Constants
        const String URLREFERENCE                   = "http://localhost:8080/api/networking/ipconfig";
        const String ADMINISTRATORACCOUNTUSERNAME   = "Administrator";
        const String ADMINISTRATORACCOUNTPASSWORD   = "p@ssw0rd";
        #endregion Constants

        #region Attributes.Public
        public static string MacAddress = "";
        #endregion Attributes.Public

        #region Methods.Public
        public static async Task<String> GetMAC()
        {
            String          MAC         = null;
            StreamReader    SR          = await GetJsonStreamData(URLREFERENCE, ADMINISTRATORACCOUNTUSERNAME, ADMINISTRATORACCOUNTPASSWORD);
            JsonObject      ResultData  = null;
            try
            {
                String      JSONData;
                JsonArray   Adapters;

                JSONData    = SR.ReadToEnd();
                ResultData  = (JsonObject)JsonObject.Parse(JSONData);
                Adapters    = ResultData.GetNamedArray("Adapters");

                //foreach (JsonObject Adapter in Adapters) 
                for (uint index = 0; index < Adapters.Count; index++)
                {
                    JsonObject  Adapter = Adapters.GetObjectAt(index).GetObject();
                    String      Type    = Adapter.GetNamedString("Type");

                    if (Type.ToLower().CompareTo("ethernet") == 0)
                    {
                        MAC         = ((JsonObject)Adapter).GetNamedString("HardwareAddress");

                        MacAddress  = MAC;

                        break;
                    }
                }
            }
            catch (Exception E)
            {
                System.Diagnostics.Debug.WriteLine(E.Message);
            }

            return MAC;
        }
        #endregion Methods.Public

        #region Methods.Private
        private static async Task<StreamReader> GetJsonStreamData(String URL,String Username,String Password)
        {
            HttpWebRequest  wrGETURL    = null;
            Stream          objStream   = null;
            StreamReader    objReader   = null;

            try
            {
                wrGETURL                    = (HttpWebRequest)WebRequest.Create(URL);
                wrGETURL.Credentials        = new NetworkCredential("Administrator", "p@ssw0rd");
                HttpWebResponse Response    = (HttpWebResponse)(await wrGETURL.GetResponseAsync());

                if (Response.StatusCode == HttpStatusCode.OK)
                {
                    objStream = Response.GetResponseStream();
                    objReader = new StreamReader(objStream);
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("GetData " + e.Message);
            }
            return objReader;
        }
        #endregion Methods.Private
    }
}
