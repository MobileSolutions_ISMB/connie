﻿using System;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.I2c;


namespace HALLibrary
{
    #if FAKE_SENSOR_HUMIDITY
    class SensorHumidity : InterfaceSensor
    {
        double humiditySimulated = 30;

        void InterfaceSensor.DetachSensor()
        {

        }

        bool InterfaceSensor.GetSensorValue(out double humidity)
        {
            humidity = humiditySimulated;
            humiditySimulated+=0.1;

            if(humiditySimulated>80)
            {
                humiditySimulated = 50;
            }

            return true;
        }

        void InterfaceSensor.Initialize()
        {

        }
    }
    #endif
    
    class TemperatureSensorI2C:InterfaceSensor
    {
        private const byte I2C_ADDR     = 0x4F;           /* 7-bit I2C address of the ADXL345 with SDO pulled low */
        private const byte T_OUT_REG    = 0x00;         /* Address of the Power Control register */

        private bool checkIsInitialized = false;

        private I2cDevice I2CHandler    = null;

        #if FAKE_SENSOR_TEMPERATURE
            private double  temperatureSimulated        = 25;
        #endif

        public void Initialize()//FIXME: DA RIVEDERE
        {
            var result = InitializeAsync();
        }

        public bool GetSensorValue(out double temperatureValue)
        {
#if !FAKE_SENSOR_TEMPERATURE
            UInt16               digitalReading;
#endif
   
            byte[] RegAddrBuf   = new byte[] { 0x00 }; // FIXME: DA VERIFICARE QUESTA PARTE!!!!
            byte[] ReadBuf      = new byte[2];

            /* 
             * Read from the accelerometer 
             * We call WriteRead() so we first write the address of the X-Axis I2C register, then read all 3 axes
             */

            temperatureValue = 0;

            if (I2CHandler != null && checkIsInitialized==true)
            {
                I2cTransferResult resultOperation = I2CHandler.ReadPartial(ReadBuf);

                
#if !FAKE_SENSOR_TEMPERATURE
                    switch (resultOperation.Status)
                    {
                        case I2cTransferStatus.FullTransfer:
                        //digitalReading = BitConverter.ToInt16(ReadBuf, 0);
                            digitalReading = (UInt16)(ReadBuf[0] << 8 | ReadBuf[1]);
                            digitalReading >>= 4;

                            temperatureValue = ((double)digitalReading * 0.0625);

                            return true;
                        case I2cTransferStatus.PartialTransfer:
                            System.Diagnostics.Debug.Write("WARNING!! PARTIAL TRANSFER DONE!");
                            break;
                        case I2cTransferStatus.SlaveAddressNotAcknowledged:
                            System.Diagnostics.Debug.Write("WARNING!! SLAVE NOT ACKNOWLEDGE!");
                            break;
                        default:
                            break;
                    }

                    return false;
#endif
            }
            else
            {
#if FAKE_SENSOR_TEMPERATURE
                            temperatureValue = temperatureSimulated;

                            temperatureSimulated += 0.1;

                            if (temperatureSimulated>35)
                            {
                                temperatureSimulated = 25;
                            }

                            return true;
#else
                return false;
#endif
            }
            /* 
             * In order to get the raw 16-bit data values, we need to concatenate two 8-bit bytes from the I2C read for each axis.
             * We accomplish this by using the BitConverter class.
             */

#if FAKE_SENSOR_TEMPERATURE
            return false;
#endif
        }

        public void DetachSensor()
        {
            if(I2CHandler!=null)
            {
                I2CHandler.Dispose();

                I2CHandler = null;
            }
        }

        public async Task InitializeAsync()
        {
            string aqs  = I2cDevice.GetDeviceSelector();                     /* Get a selector string that will return all I2C controllers on the system */
            var dis     = await DeviceInformation.FindAllAsync(aqs);            /* Find the I2C bus controller device with our selector string           */

            if (dis.Count == 0)
            {
                System.Diagnostics.Debug.Write("No I2C controllers were found on the system");
                return;
            }

            var settings = new I2cConnectionSettings(I2C_ADDR);
            settings.BusSpeed = I2cBusSpeed.FastMode;

#if !FAKE_SENSOR_TEMPERATURE
            I2CHandler = await I2cDevice.FromIdAsync(dis[0].Id, settings);    /* Create an I2cDevice with our selected bus controller and I2C settings */
#endif

            if (I2CHandler == null)
            {
                System.Diagnostics.Debug.Write(string.Format(
                    "Slave address {0} on I2C Controller {1} is currently in use by " +
                    "another application. Please ensure that no other applications are using I2C.",
                    settings.SlaveAddress,
                    dis[0].Id));
                return;
            }

            /* 
             * Initialize the accelerometer:
             *
             * For this device, we create 2-byte write buffers:
             * The first byte is the register address we want to write to.
             * The second byte is the contents that we want to write to the register. 
             */
            byte[] sensorConf = new byte[] { 0x01, 0x40 };
            byte[] readPtrReg = new byte[] { T_OUT_REG };

            /* Write the register settings */
            try
            {
                I2CHandler.Write(sensorConf);
                I2CHandler.Write(readPtrReg);
            }
            /* If the write fails display the error and stop running */
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write("Failed to communicate with device: " + ex.Message);
                return;
            }

            checkIsInitialized = true;

            return;
        }
    }
}
