Set-ExecutionPolicy RemoteSigned
net start WinRM
Set-Variable -name "ALIASBOARD" -value "minwinpc"
Set-Item WSMan:\localhost\Client\TrustedHosts -Value $ALIASBOARD
Enter-PSSession -ComputerName $ALIASBOARD -Credential $ALIASBOARD\Administrator
<# Password Default: p@ssw0rd #>
<# Web Page Device: $IPBOARD:8080/AppXManager.htm #>
