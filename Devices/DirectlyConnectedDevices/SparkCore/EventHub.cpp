#include "EventHub.h"
#include <stdlib.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

void EventHubClass::init(HttpClient c)
{
	client = c;
}

void EventHubClass::send_measure(char *val) {
	char buffer[300];
	// POST body
	sprintf(buffer, json,
		"4198a348-e2f9-4438-ab23-82a3930662ab",
		"ISMB",
		"SparkCore",
		"Torino",
		"Temperature",
		"C",
		val
		);

	http_header_t headers[] = {
        { "X-ZUMO-APPLICATION", AzureMobileServiceKey },
        { "Cache-Control", "no-cache" },
				{ "Content-Type", "application/json" },
        { NULL, NULL } // NOTE: Always terminate headers with NULL
  };

	http_request_t request;
  http_response_t response;

  request.hostname = AzureMobileService;
  request.port = 80;
  request.path = "/api/proxy";
  request.body = buffer;

  client.post(request, response, headers);
  Serial.print("Application>\tResponse status: ");
  Serial.println(response.status);

  Serial.print("Application>\tHTTP Response Body: ");
  Serial.println(response.body);
}

EventHubClass EventHub;
