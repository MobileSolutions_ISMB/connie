#include "Sensor.h"


void SensorClass::init()
{
	// Initialize I2C port
	Wire.begin();

#ifdef HTS221
	// Initialize HTS221, pulling out from power down mode
	Wire.beginTransmission(byte(I2C_ADDR));
	Wire.write(byte(CTRL_REG1));
	Wire.write(byte(0x81));				// PD: 1, BDU: when data ready, ODR: 1Hz
	Wire.endTransmission();

	// Read Calibration Data
	byte c[4];
	Wire.beginTransmission(byte(I2C_ADDR));
	Wire.write(byte(CAL_REG1 | AUTOINCREMENT));
	Wire.endTransmission(false);				// Perform I2C Repeated Start
	Wire.requestFrom(byte(I2C_ADDR), byte(4));
	for (int i = 0; i < 4; i++) {
		c[i] = Wire.read();
	}

	int T0_degC_x8, T1_degC_x8;
	T0_degC_x8 = (c[3] & 3) | c[0];
	T0_degC = (float)T0_degC_x8 / 8;
	T1_degC_x8 = ((c[3] & 12) << 6) | c[1];
	T1_degC = (float)T1_degC_x8 / 8;

	Wire.beginTransmission(byte(I2C_ADDR));
	Wire.write(byte(CAL_REG2 | AUTOINCREMENT));
	Wire.endTransmission(false);
	Wire.requestFrom(byte(I2C_ADDR), byte(4));
	for (int i = 0; i < 4; i++) {
		c[i] = Wire.read();
	}

	T0_OUT = (c[1] << 8) | c[0];
	T1_OUT = (c[3] << 8) | c[2];

	Serial.println("Calibration values:");
	Serial.print("   T0_degC: "); Serial.println(T0_degC);
	Serial.print("   T1_degC: "); Serial.println(T1_degC);
	Serial.print("   T0_OUT: "); Serial.println(T0_OUT);
	Serial.print("   T1_OUT: "); Serial.println(T1_OUT);
#else
	// Initialize STTS75, point to T_OUT_REG
	Wire.beginTransmission(byte(I2C_ADDR));
	Wire.write(byte(T_OUT_REG));
	Wire.endTransmission();
#endif
}

float SensorClass::getTemperature() {
	int i = 0;
	byte c[4];
	Wire.beginTransmission(byte(I2C_ADDR));
#ifdef HTS221
	Wire.write(byte(T_OUT_REG | AUTOINCREMENT));
	Wire.endTransmission(false);
	Wire.requestFrom(byte(I2C_ADDR), byte(4));
#else
	Wire.requestFrom(byte(I2C_ADDR), byte(2));
#endif

	while (Wire.available()) {
		c[i] = Wire.read();
		i++;
	}

#ifdef HTS221
	if (i == 4) {
		int digitalReading = (c[1] << 8) | c[0];
		float temperature = T0_degC + (T1_degC - T0_degC) * ((float)(digitalReading - T0_OUT) / (float)(T1_OUT - T0_OUT));

#else
	if (i == 2) {
		int digitalReading = (c[0] << 8) | c[1];
		digitalReading = digitalReading >> 4;
		float temperature = ((float)digitalReading * 0.0625);
#endif
		Serial.print("Temperature: "); Serial.print(temperature); Serial.println(" C");
		return temperature;
	}
	else {
		Serial.println("Warning: I2C Read error! Retrying...");
		return -1;
	}
}


SensorClass Sensors;
