#include "Sensor.h"
#include "EventHub.h"
#include <math.h>

char str_temperature[10];

HttpClient client;
SensorClass sensor;
EventHubClass eventDispatcher;

void setup()
{
	// add setup code here

	// Initialize Serial Port
	Serial.begin(115200);

	Serial.println("Starting...");
	while(!WiFi.ready()) Spark.process();
	Serial.println("Connected to wifi");
	printWifiStatus();

	delay(2000);

	// Initialize temperature sensor
	sensor.init();
	eventDispatcher.init(client);


}

void loop()
{
	// add main program code here

	// Read temperature from sensor and dispatch to Event Hub
	float a = sensor.getTemperature();
	floatToStr(a, 2, str_temperature);
	Serial.println(str_temperature);
	eventDispatcher.send_measure(str_temperature);
	delay(1500);
}

void printWifiStatus() {
	// print the SSID of the network you're attached to:
	Serial.print("SSID: ");
	Serial.println(WiFi.SSID());

	// print your WiFi shield's IP address:
	IPAddress ip = WiFi.localIP();
	Serial.print("IP Address: ");
	Serial.println(ip);

	// print the received signal strength:
	long rssi = WiFi.RSSI();
	Serial.print("signal strength (RSSI):");
	Serial.print(rssi);
	Serial.println(" dBm");
}

static void floatToStr(float in, uint8_t dec_prec, char *str)
{
	int32_t out_int = (int32_t)in;
	in = in - (float)(out_int);
	int32_t out_dec = (int32_t)trunc(in*pow(10.0f, dec_prec));
	sprintf(str, "%d.%d", out_int, out_dec);
}
