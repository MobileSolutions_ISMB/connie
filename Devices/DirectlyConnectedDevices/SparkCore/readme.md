## Teoria di funzionamento ##
Il seguente esempio prevede l'utilizzo di una board **SparkCore (ora Particle)**, una **custom sensor board** e un **AzureMobileService**.

Il progetto è stato creato con la IDE  **Particle Dev**. 

La custom sensor board, adatta ad essere collegata alla SparkCore direttamente sulla breadboard, utilizza un sensore digitale di temperatura STTS75. Tale sensore utilizza il bus **I2C** per la comunicazione con il microcontrollore.

Per una **descrizione dettagliata del protocollo** utilizzato dal sensore STTS75 fare riferimento a [[Protocollo di comunicazione STTS75](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/STTS75/readme.md)]

Per lo **schematico** e il **layout** fisico della board fare riferimento a [[Custom sensor board](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/CustomSensorBoard/readme.md)]

### Schema Logico ###
![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/ArduinoToolchain/7.png)

Data la mancanza di librerie necessarie per l'utilizzo di una connessione HTTP sicura, verrà utilizzato un AzureMobileService, che fungerà da **proxy** verso l'eventHub.

L'applicazione di esempio provvede a leggere a intervalli regolari (1 sec.) tramite il bus I2C il valore di temperatura dal sensore, crea il messaggio JSON e lo invia tramite una chiamata **HTTP POST** all'AzureMobileService.
Poichè non è presente un modulo RTC (Real Time Controller) all'interno del microcontrollore a bordo della SparkCore, il servizio mobile provvede ad aggiungere al messaggio JSON ricevuto il timestamp corrente. In seguito esso genera il **SAS Token tramite HMAC-SHA256**, e infine reinvia il nuovo messaggio JSON tramite **HTTPS POST** all'EventHub Azure.

###### Esempio di HTTP POST da Arduino a AzureMobileService ######
	POST /api/<amsApi> HTTP/1.1
	Host: <ams>
	X-ZUMO-APPLICATION: <amsKey>
	Cache-Control: no-cache
	Content-type: application/json
	Content-Length: <body length>

	{"guid": "<guid>","organization": "ISMB","displayname": "SparkCore","location": "Torino","measurename": "Temperature","unitofmeasure": "C","value": 26.43 }

###### Esempio di HTTPS POST da AzureMobileService a EventHub ######
	POST /<eventHub>/publishers/<device>/messages HTTP/1.1
	Host: <eventHub>.servicebus.windows.net
	Cache-Control: no-cache
	Content-Type: application/json; charset=UTF-8
	Content-Length: <body length>
	Authorization: SharedAccessSignature <sas string>
	
	{"guid": "<guid>","organization": "ISMB","displayname": "SparkCore","location": "Torino","measurename": "Temperature","unitofmeasure": "C","timecreated": "2015-05-19T07:52:37.003712Z","value": 26.43 }

Negli esempi soprastanti è necessario sostituire tutte le stringhe comprese tra < e > con dei valori validi.

### Requisiti Hardware ###
- SparkCore  [[Specifiche tecniche](http://docs.particle.io/core/start/)]
- Custom sensor board [[Specifiche tecniche](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/CustomSensorBoard/readme.md)]
- Connessione a internet tramite WiFi

### Requisiti Software ###

- Particle Dev [[Guida all'installazione](http://docs.particle.io/core/dev/)]
- Driver SparkCore [[Guida all'installazione](http://docs.particle.io/core/connect/#connecting-your-device-using-windows)]

