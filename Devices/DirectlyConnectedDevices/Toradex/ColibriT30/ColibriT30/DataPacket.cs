﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace ColibriT30
{
    public class DataPacket
    {
        // Almost constant
        public string guid { get; set; }
        public string organization { get; set; }
        public string displayname { get; set; }
        public string location { get; set; }
        public string measurename { get; set; }
        public string unitofmeasure { get; set; }

        // Variable values
        public string timecreated { get; set; }
        public float  value { get; set; }

        public DataPacket(string _guid, string _org, string _disp, string _loc, string _measname, string _unit)
        {
            guid = _guid;
            organization = _org;
            displayname = _disp;
            location = _loc;
            measurename = _measname;
            unitofmeasure = _unit;
        }
    }
}
