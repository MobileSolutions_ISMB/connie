﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Collections.Specialized;
using System.IO;
using System.Web.Services;
using System.Web;
using OpenNETCF.Security.Cryptography;
using Newtonsoft.Json;
using System.Security.Cryptography.X509Certificates;

namespace ColibriT30
{
    unsafe public partial class Form1 : Form
    {
        private const int    ServiceBusPort = 80;
        private const string ServiceBus =    "micto-ehctd";
        private const string EventHubName =  "ehdevices";
        private const string KeyName =       "D1";
        private const string Key =           "pGrHDydQFVUGkw2o5Y1E4CdQEWQc3wyC3DvICVAavfQ=";
        private const string DisplayName =   "Toradex";

        private Uri uri;
        private DataPacket dataPkt;
        private Random r;

        public Form1()
        {
            InitializeComponent();
            ServicePointManager.Expect100Continue = false;
            //ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();

            I2CUtility.I2CInit();                                           ///< Initialise I2C bus
            I2CUtility.I2CSetSpeed(I2CUtility.I2CSpeed.I2C_400KBPS);        ///< Set I2C bus speed
            I2CUtility.GetI2CLock(5000);                                                ///< Wait for 5000 ms to acquire I2C lock 
            I2CUtility.I2CBurstWrite(0x4F, new byte[] { 0x01, 0x60 }, -1, 2);                    ///< Write: resolution to 0.0625 in CTRL_REG
            I2CUtility.I2CBurstWrite(0x4F, new byte[] { 0x00}, -1, 1);                           ///< Write: point to T_OUT_REG
            I2CUtility.ReleaseI2CLock();

            r = new Random();
            timer1.Interval = 1000;
            timer1.Enabled = true;

            uri = new Uri("https://" + ServiceBus +
                          ".servicebus.windows.net/" + EventHubName +
                          "/publishers/" + DisplayName + "/messages");

            dataPkt = new DataPacket(
                "1178a348-e2f9-4438-ab23-82a3930662ab",
                "ISMB",
                DisplayName,
                "Torino",
                "Temperature",
                "C");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = !timer1.Enabled;            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Byte[] i2cDataRead = new Byte[8];
            I2CUtility.GetI2CLock(5000);                                                ///< Wait for 5000 ms to acquire I2C lock 
            I2CUtility.I2CBurstRead(0x4F, i2cDataRead, -1, 2);                          ///< Read: T_OUT_REG register (2 bytes)
            I2CUtility.ReleaseI2CLock();

            int digitalReading = (i2cDataRead[0] << 8) | i2cDataRead[1];
            digitalReading = digitalReading >> 4;
            float temperature = ((float)digitalReading * 0.0625f);
            label1.Text = temperature.ToString();
            
            SendPost(CreateJSON(temperature));
        }

        private string CreateJSON(float temperature)
        {
            dataPkt.timecreated = DateTime.UtcNow.ToString("o");
            dataPkt.value = temperature;
            string json = JsonConvert.SerializeObject(dataPkt, Formatting.None);
            return json;
        }

        private void SendPost(string content)
        {

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
            req.Method = "POST";
            req.Headers.Add("Authorization", "SharedAccessSignature " + SASTokenHelper(uri.ToString()));
            req.ContentType = "application/json; charset=UTF-8";
            req.Headers.Add("Cache-Control", "no-cache");
            req.KeepAlive = true;
            req.AllowWriteStreamBuffering = true;
            req.Timeout = 800;

            try
            {
                // Retrieve request stream and wrap in StreamWriter
                Stream reqStream = req.GetRequestStream();
                StreamWriter wrtr = new StreamWriter(reqStream);
                wrtr.Write(content);
                wrtr.Close();

                // Submit the request and get the response object

                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                // Retrieve the response stream and wrap in a StreamReader
                Stream respStream = resp.GetResponseStream();
                StreamReader rdr = new StreamReader(respStream);
                resp.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        private string SASTokenHelper(string strToSign)
        {
            int expiry = (int)DateTime.UtcNow.AddMinutes(20).Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            string stringToSign = HttpUtility.UrlEncode(strToSign) + "\n" + expiry.ToString();
            string signature = getHmacSha256(Key.ToString(), stringToSign);
            string token = String.Format("sr={0}&sig={1}&se={2}&skn={3}", HttpUtility.UrlEncode(uri.ToString()), HttpUtility.UrlEncode(signature), expiry, KeyName.ToString());

            return token;
        }
        

        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public string getHmacSha256(string key, string value)
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            byte[] keyStrm = utf8.GetBytes(key);
            byte[] valueStrm = utf8.GetBytes(value);

            HMACSHA256 hmacsha256 = new HMACSHA256(keyStrm);
            byte[] hashmessage = hmacsha256.ComputeHash(valueStrm);

            string s = Convert.ToBase64String(hashmessage);
            return s;
        }
    }

    /*
    public class TrustAllCertificatePolicy : ICertificatePolicy
    {
        public TrustAllCertificatePolicy()
        {
        }

        public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest req, int problem)
        {
            return true;
        }
    }
    */ 
}