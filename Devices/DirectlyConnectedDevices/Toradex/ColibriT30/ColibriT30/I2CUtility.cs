﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;       ///< To use Toradex C Win32 DLLs
                                            
namespace ColibriT30
{
    unsafe public static class I2CUtility
    {
        public enum I2CSpeed { I2C_100KBPS, I2C_400KBPS, I2C_UNDEF_KBPS };

        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]                             ///< Importing DLL
        public static extern bool I2CInit();                                          ///< External function declaration
        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]
        public static extern void I2CDeInit();
        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]
        public static extern bool ReleaseI2CLock();
        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]
        public static extern bool GetI2CLock(Int32 timeout);
        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]
        public static extern bool I2CBurstRead(Byte uSlaveAddress, Byte[] pBuffer, short ucOffset, Int16 iNumberBytes);
        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]
        public static extern bool I2CBurstWrite(Byte uSlaveAddress, Byte[] pBuffer, short ucOffset, Int16 iNumberBytes);
        [DllImport("I2cLib.dll", CharSet = CharSet.Auto)]
        public static extern void I2CSetSpeed(I2CSpeed Speed);

    }
}
