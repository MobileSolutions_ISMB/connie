## Teoria di funzionamento ##
Il seguente esempio prevede l'utilizzo di una board **Toradex Colibri T30** e di una **custom sensor board**.

Il progetto è stato creato con Visual Studio 2008 e **Windows Embedded Compact 7.0**. 

La custom sensor board, utilizzabile tramite bread-board, utilizza un sensore digitale di temperatura STTS75. Tale sensore utilizza il bus **I2C** per la comunicazione con il microprocessore.

Per una **descrizione dettagliata del protocollo** utilizzato dal sensore STTS75 fare riferimento a [[Protocollo di comunicazione STTS75](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/STTS75/readme.md)]

Per lo **schematico** e il **layout** fisico della board fare riferimento a [[Custom sensor board](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/CustomSensorBoard/readme.md)]

### Schema Logico ###
![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Toradex/1.png) 

Data la presenza di librerie adatte all'utilizzo di una connessione HTTP sicura, i dati del sensore verranno inviati **direttamente** all'EventHub Azure.

L'applicazione di esempio provvede a leggere a intervalli regolari (1 sec.) tramite il bus I2C il valore di temperatura dal sensore, crea il messaggio JSON e lo invia tramite una chiamata **HTTPS POST** all'EventHub.

Inoltre, avendo a disposizione un sistema operativo (WinCE 7.0) che gestisce l'orologio interno del microprocessore, è possibile includere direttamente nel pacchetto JSON il timestamp corrente. Le librerie crittografiche di WinCE permettono la generazione del **SAS Token tramite HMAC-SHA256**, il quale viene aggiunto agli header standard della richiesta HTTPS.

> Per maggiori informazioni sulla generazione del **SAS Token** clicca [qui](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/SAS.md)

###### Esempio di HTTPS POST da Toradex Colibri T30 a EventHub ######
	POST /<eventHub>/publishers/<device>/messages HTTP/1.1
	Host: <eventHub>.servicebus.windows.net
	Cache-Control: no-cache
	Content-Type: application/json; charset=UTF-8
	Content-Length: <body length>
	Authorization: SharedAccessSignature <sas string>
	
	{"guid": "<guid>","organization": "ISMB","displayname": "Arduino","location": "Torino","measurename": "Temperature","unitofmeasure": "C","timecreated": "2015-05-19T07:52:37.003712Z","value": 26.43 }

Nell'esempio soprastante è necessario sostituire tutte le stringhe comprese tra < e > con dei valori validi.

### Requisiti Hardware ###
- Toradex Colibri T30 [[Specifiche tecniche](https://www.toradex.com/computer-on-modules/colibri-arm-family/nvidia-tegra-3)]
- Viola Carrier Board [[Specifiche tecniche](https://www.toradex.com/products/carrier-boards/viola-carrier-board)
- Custom sensor board [[Specifiche tecniche](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Docs/CustomSensorBoard/readme.md)]
- Connessione a internet tramite cavo ethernet

### Requisiti Software ###

- Visual Studio 2008
- Visual Studio Plugin for Embedded Compact 7.0 [[Installer](http://www.microsoft.com/en-us/download/details.aspx?id=27729)]
- Toradex Toolchain for WinCE 7.0 [[Guida all'installazione](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Toradex/readme.md)]

