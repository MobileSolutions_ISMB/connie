## Setup dei Device##
La premessa di base di questo progetto è che i dati raccolti dai sensori possono essere inviati tramite il formato JSON descritto precedentemente. Ciò può essere ottenuto programmando direttamente i dispositivi affinchè spediscano direttamente il valore dei sensori letti, utilizzando una connessione internet (ad es. compilando e caricando uno sketch su un Arduino Ethernet), o usando dei dispositivi che si comportino da gateway (ad es. usando uno script Python su un Raspberry Pi che legga il valore di un sensore USB e lo trasmetta formattando il messaggio JSON in modo corretto). 

Per costruire una configurazione funzionale è necessario identificare e configurare il device che produrrà i dati che verranno trasmessi al cloud Azure e visualizzati/analizzati. I dispositivi generalmente si possono suddividere in 2 categorie: quelli che sono connessi direttamente a internet, e quelli che hanno bisogno di connettersi a internet tramite dei dispositivi intermedi. 

Codice di esempio e la relativa documentazione può essere trovata nelle seguenti cartelle:

1. [Devices che si connettono direttamente ad Azure](DirectlyConnectedDevices/) - Devices con una discreta potenza di calcolo e velocità tale da supportare una connessione IP sicura
2. [Device semplici che richiedono un gateway](GatewayConnectedDevices/) - Dispositivi troppo piccoli o con poca potenza di calcolo per supportare una connessione IP sicura, o che generano dati da aggregare prima di essere inviati al cloud
3. [Gateway o altri device intermedi](Gateways/) - Dispositivi che collezionano dati da altre fonti e li trasmettono al cloud.

## Soluzioni di esempio ##
Le soluzioni di esempio presenti nelle sottocartelle mostrano come utilizzare diversi dispositivi connessi direttamente a internet per trasmettere i dati in tempo reale al cloud. In dettaglio:

- [Arduino Ethernet + ST Sensor Board](DirectlyConnectedDevices/Arduino/)
	
	Questa soluzione prevede l'utilizzo di una board Arduino Ethernet e dell'expansion board ST X-NUCLEO-IKS01A1. Inoltre data la limitata capacità di calcolo del microcontrollore, viene utilizzato un AzureMobileService come gateway.

- [Edison + Custom Sensor Board](DirectlyConnectedDevices/Edison/)
	
	Questa soluzione prevede l'utilizzo di una board Edison e di un sensore di temperatura digitale (STTS75) . Data la mancanza di librerie adatte alla creazione di una connessione IP sicura, viene utilizzato un AzureMobileService come gateway.

- [Galileo Gen 2 + Custom Sensor Board](DirectlyConnectedDevices/Galileo/)
	
	Questa soluzione prevede l'utilizzo di una board Galileo Gen 2 e di un sensore di temperatura digitale (STTS75) . Data la mancanza di librerie adatte alla creazione di una connessione IP sicura, viene utilizzato un AzureMobileService come gateway.

- [Toradex Colibri T30  + Custom Sensor Board](DirectlyConnectedDevices/Toradex/)
	
	Questa soluzione prevede l'utilizzo di un SoM Colibri T30 e di un sensore di temperatura digitale (STTS75). Il SoM è basato su Windows CE 7.0. Data la presenza di librerie adatte alla creazione di una connessione IP sicura, la trasmissione dei dati viene effettuata direttamente verso l'EventHub di Azure.

- [Gadgeteer FEZ Spider](DirectlyConnectedDevices/FEZSpider/)
	
	Questa soluzione prevede l'utilizzo di una board FEZ Spider e di un sensore di temperatura digitale (Si7020). Data la presenza di librerie adatte alla creazione di una connessione IP sicura, la trasmissione dei dati viene effettuata direttamente verso l'EventHub di Azure.

- [ST Nucleo  + ST Sensor Board + ST WiFi](DirectlyConnectedDevices/NucleoVisualGDB/)
	
	Questa soluzione prevede l'utilizzo di una board Nucleo F401RE, dell'expansion board ST X-NUCLEO-IKS01A1 e dell'expansion ST WiFi. Data la presenza di librerie adatte alla creazione di una connessione IP sicura, la trasmissione dei dati viene effettuata direttamente verso l'EventHub di Azure.

- [SparkCore (Particle)  + Custom Sensor Board](DirectlyConnectedDevices/SparkCore/)
	
	Questa soluzione prevede l'utilizzo di una board SparkCore (adesso Particle)e di un sensore di temperatura digitale (STTS75). Data la mancanza di librerie adatte alla creazione di una connessione IP sicura, viene utilizzato un AzureMobileService come gateway.

