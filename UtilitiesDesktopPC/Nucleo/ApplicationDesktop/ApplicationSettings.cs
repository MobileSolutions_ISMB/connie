﻿using System;
using System.Collections.Generic;
using System.IO;

namespace TestSerial
{
    #region AdditionalTypes
    enum TypeAttribute
    {
        NONE,
        INTEGER,
        FLOAT,
        DOUBLE,
        STRING,
        GENERICOBJECT
    }

    class ValueContainer
    {
        public TypeAttribute    typeAttribute;
        public Object           value;
        
        public ValueContainer()
        {
            typeAttribute   = TypeAttribute.NONE;
            value           = null;
        }

        public ValueContainer(TypeAttribute typeAttribute,Object value)
        {
            this.typeAttribute  = typeAttribute;
            this.value          = value;
        }  
    }
    #endregion AdditionalTypes

    class ApplicationSettings
    {
        #region Constant.Labels
        const String LabelSerialPort    = "SerialPort";
        const String LabelBaudrate      = "Baudrate";
        const String LabelWiFiSSID      = "SSID";
        const String LabelWiFiKey       = "WiFiKey";
        const String Separator          = ";";
        const String Terminator         = "\r\n";
        #endregion Constant.Label

        #region Attributes
        IDictionary<String, ValueContainer> listAttributes = new Dictionary<String, ValueContainer>();
        #endregion Attributes

        #region Constructor
        public ApplicationSettings()
        {
            BaudRate    = 115200;
            SerialPort  = "COM1";
            WiFiSSID    = "NONE";
            WiFiKey     = "NONE";
        }
        #endregion Constructor

        #region Properties
        public String SerialPort
        {
            get
            {
                return GetAttributeValue(LabelSerialPort, "COM1").ToString();
            }
            set
            {
                AddOrUpdateAttribute(LabelSerialPort,TypeAttribute.STRING, value);
            }
        }
        
        public int BaudRate
        {
            get
            {
                return (int)GetAttributeValue(LabelBaudrate,115200);
            }
            set
            {
                AddOrUpdateAttribute(LabelBaudrate, TypeAttribute.INTEGER, value);
            }
        }

        public String WiFiSSID
        {
            get
            {
                return GetAttributeValue(LabelWiFiSSID, "NONE").ToString();
            }
            set
            {
                AddOrUpdateAttribute(LabelWiFiSSID, TypeAttribute.STRING, value);
            }
        }

        public String WiFiKey
        {
            get
            {
                return GetAttributeValue(LabelWiFiKey, "NONE").ToString();
            }
            set
            {
                AddOrUpdateAttribute(LabelWiFiKey, TypeAttribute.STRING, value);
            }
        }
        #endregion Properties

        #region PublicMethods
        public void SaveInFile(String pathFile)
        {
            try
            {
                StreamWriter fileHandle;

                fileHandle = new StreamWriter(pathFile, false);

                if (fileHandle != null)
                {
                    foreach(String label in listAttributes.Keys)
                    {
                        String line = CreateLineFromAttributre(label, Separator);

                        fileHandle.Write(line + Terminator);
                    }

                    fileHandle.Close();

                    fileHandle = null;
                }
            }
            catch(Exception)
            {

            }
        }

        public bool LoadFromFile(String pathFile)
        {
            try
            {
                StreamReader    fileHandle;
                int             counterParameters;

                counterParameters   = 0;
                fileHandle          = new StreamReader(pathFile);

                if (fileHandle != null)
                {
                    String line = "";

                    while (fileHandle.EndOfStream == false)
                    {
                        line = fileHandle.ReadLine();

                        if(line!=null && line.Length>0)
                        {
                            if (line.EndsWith(Terminator))
                            {
                                line = line.Remove(line.IndexOf(Terminator));
                            }
                            String[]    listParameters  = line.Split(Separator.ToCharArray());
                            String      label           = "";
                            String      Value           = "";

                            for(int indexParameter=0;listParameters!=null && indexParameter<listParameters.Length;indexParameter++)
                            {
                                switch(indexParameter)
                                {
                                    case 0:
                                        label = listParameters[indexParameter];
                                    break;

                                    case 1:
                                        Value = listParameters[indexParameter];
                                        break;
                                }
                            }

                            if(label!=null && label.Length>0 && Value!=null && Value.Length>0)
                            {
                                UpdateSingleAttribute(label, Value);
                                counterParameters++;
                            }
                        }
                    }
                    fileHandle.Close();
                    fileHandle.Dispose();
                }

                return counterParameters > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion PublicMethods

        #region PrivateMethods
        private String CreateLineFromAttributre(string label, string separator)
        {
            String result = label + separator;

            if (listAttributes.ContainsKey(label))
            {
                ValueContainer getValue = listAttributes[label];

                if (getValue != null && getValue.value!=null)
                {
                    result += getValue.value.ToString();
                }
                return result;
            }

            return "";
        }

        private bool UpdateSingleAttribute(String label,string StringValue)
        {
            if(listAttributes.ContainsKey(label) && StringValue!=null && StringValue.Length>0)
            {
                ValueContainer container = listAttributes[label];

                if(container!=null)
                {
                    if(container.value==null)
                    {
                        return false;
                    }

                    switch(container.typeAttribute)
                    {
                        case TypeAttribute.INTEGER:
                            container.value = int.Parse(StringValue);
                        break;

                        case TypeAttribute.STRING:
                            container.value = StringValue;
                        break;

                        case TypeAttribute.DOUBLE:
                            container.value = double.Parse(StringValue);
                        break;

                        default:

                        break;
                    }

                    listAttributes[label] = container;

                    return true;
                }
            }

            return false;
        }

        private void AddOrUpdateAttribute(String label,TypeAttribute typeAttribute,Object newValue)
        {
            try
            {
                if(listAttributes.ContainsKey(label))
                {
                    ValueContainer container = listAttributes[label];

                    if (container == null)
                    {
                        container = new ValueContainer(typeAttribute, newValue);
                    }
                    else
                    {
                        container.typeAttribute = typeAttribute;
                        container.value         = newValue;
                    }
                    listAttributes[label] = container;
                }
                else
                {
                    listAttributes.Add(label, new ValueContainer(typeAttribute, newValue));
                }
            }
            catch(Exception)
            {

            }
        }

        private Object GetAttributeValue(String label,Object defaultValue)
        {
            try
            {
                if (listAttributes.ContainsKey(label))
                {
                    ValueContainer container = listAttributes[label];

                    if(container!=null)
                    {
                        return container.value;
                    }
                }

                listAttributes.Add(label, new ValueContainer(TypeAttribute.GENERICOBJECT, defaultValue));//FIXME: CASO DIFFICILE DA GESTIRE!

                return defaultValue;
            }
            catch (Exception)
            {
                return defaultValue;
            }
        }
        #endregion PrivateMethods
    }
}
