﻿namespace TestSerial
{
    partial class MainFormFrontEnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFormFrontEnd));
            this.panelSerialConfiguration = new System.Windows.Forms.Panel();
            this.buttonUpdateCOMList = new System.Windows.Forms.Button();
            this.comboBoxCOMPortList = new System.Windows.Forms.ComboBox();
            this.labelSerialSpeed = new System.Windows.Forms.Label();
            this.textBoxComSpeed = new System.Windows.Forms.TextBox();
            this.labelSerialPort = new System.Windows.Forms.Label();
            this.panelSTMManagement = new System.Windows.Forms.Panel();
            this.buttonWiFiKey = new System.Windows.Forms.Button();
            this.labelNetKey = new System.Windows.Forms.Label();
            this.textBoxWiFiKey = new System.Windows.Forms.TextBox();
            this.buttonSetSSID = new System.Windows.Forms.Button();
            this.textBoxMACAddres = new System.Windows.Forms.TextBox();
            this.buttonGetMACAddress = new System.Windows.Forms.Button();
            this.labelSSID = new System.Windows.Forms.Label();
            this.textBoxSSID = new System.Windows.Forms.TextBox();
            this.labelMACAddress = new System.Windows.Forms.Label();
            this.groupBoxTraceApplication = new System.Windows.Forms.GroupBox();
            this.lbText = new System.Windows.Forms.ListBox();
            this.panelDeviceStatus = new System.Windows.Forms.Panel();
            this.checkBoxWiFiKey = new System.Windows.Forms.CheckBox();
            this.checkBoxWiFiSSID = new System.Windows.Forms.CheckBox();
            this.checkBoxAlive = new System.Windows.Forms.CheckBox();
            this.panelActionButtons = new System.Windows.Forms.Panel();
            this.buttonIsAlive = new System.Windows.Forms.Button();
            this.buttonReboot = new System.Windows.Forms.Button();
            this.cmdDisconnect = new System.Windows.Forms.Button();
            this.cmdConnect = new System.Windows.Forms.Button();
            this.groupBoxStatusInfo = new System.Windows.Forms.GroupBox();
            this.textBoxStatusInfo = new System.Windows.Forms.TextBox();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panelSerialConfiguration.SuspendLayout();
            this.panelSTMManagement.SuspendLayout();
            this.groupBoxTraceApplication.SuspendLayout();
            this.panelDeviceStatus.SuspendLayout();
            this.panelActionButtons.SuspendLayout();
            this.groupBoxStatusInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSerialConfiguration
            // 
            this.panelSerialConfiguration.Controls.Add(this.buttonUpdateCOMList);
            this.panelSerialConfiguration.Controls.Add(this.comboBoxCOMPortList);
            this.panelSerialConfiguration.Controls.Add(this.labelSerialSpeed);
            this.panelSerialConfiguration.Controls.Add(this.textBoxComSpeed);
            this.panelSerialConfiguration.Controls.Add(this.labelSerialPort);
            this.panelSerialConfiguration.Location = new System.Drawing.Point(3, 262);
            this.panelSerialConfiguration.Name = "panelSerialConfiguration";
            this.panelSerialConfiguration.Size = new System.Drawing.Size(401, 100);
            this.panelSerialConfiguration.TabIndex = 13;
            // 
            // buttonUpdateCOMList
            // 
            this.buttonUpdateCOMList.Location = new System.Drawing.Point(258, 25);
            this.buttonUpdateCOMList.Name = "buttonUpdateCOMList";
            this.buttonUpdateCOMList.Size = new System.Drawing.Size(125, 20);
            this.buttonUpdateCOMList.TabIndex = 17;
            this.buttonUpdateCOMList.Text = "Update COM List";
            this.buttonUpdateCOMList.Click += new System.EventHandler(this.buttonUpdateCOMList_Click);
            // 
            // comboBoxCOMPortList
            // 
            this.comboBoxCOMPortList.FormattingEnabled = true;
            this.comboBoxCOMPortList.Location = new System.Drawing.Point(106, 27);
            this.comboBoxCOMPortList.Name = "comboBoxCOMPortList";
            this.comboBoxCOMPortList.Size = new System.Drawing.Size(146, 21);
            this.comboBoxCOMPortList.TabIndex = 16;
            // 
            // labelSerialSpeed
            // 
            this.labelSerialSpeed.Location = new System.Drawing.Point(18, 55);
            this.labelSerialSpeed.Name = "labelSerialSpeed";
            this.labelSerialSpeed.Size = new System.Drawing.Size(82, 20);
            this.labelSerialSpeed.TabIndex = 13;
            this.labelSerialSpeed.Text = "Speed";
            // 
            // textBoxComSpeed
            // 
            this.textBoxComSpeed.Location = new System.Drawing.Point(106, 54);
            this.textBoxComSpeed.Name = "textBoxComSpeed";
            this.textBoxComSpeed.Size = new System.Drawing.Size(146, 20);
            this.textBoxComSpeed.TabIndex = 14;
            this.textBoxComSpeed.Text = "115200";
            // 
            // labelSerialPort
            // 
            this.labelSerialPort.Location = new System.Drawing.Point(18, 26);
            this.labelSerialPort.Name = "labelSerialPort";
            this.labelSerialPort.Size = new System.Drawing.Size(82, 20);
            this.labelSerialPort.TabIndex = 15;
            this.labelSerialPort.Text = "Serial";
            // 
            // panelSTMManagement
            // 
            this.panelSTMManagement.Controls.Add(this.buttonWiFiKey);
            this.panelSTMManagement.Controls.Add(this.labelNetKey);
            this.panelSTMManagement.Controls.Add(this.textBoxWiFiKey);
            this.panelSTMManagement.Controls.Add(this.buttonSetSSID);
            this.panelSTMManagement.Controls.Add(this.textBoxMACAddres);
            this.panelSTMManagement.Controls.Add(this.buttonGetMACAddress);
            this.panelSTMManagement.Controls.Add(this.labelSSID);
            this.panelSTMManagement.Controls.Add(this.textBoxSSID);
            this.panelSTMManagement.Controls.Add(this.labelMACAddress);
            this.panelSTMManagement.Location = new System.Drawing.Point(3, 368);
            this.panelSTMManagement.Name = "panelSTMManagement";
            this.panelSTMManagement.Size = new System.Drawing.Size(401, 133);
            this.panelSTMManagement.TabIndex = 18;
            // 
            // buttonWiFiKey
            // 
            this.buttonWiFiKey.Location = new System.Drawing.Point(258, 74);
            this.buttonWiFiKey.Name = "buttonWiFiKey";
            this.buttonWiFiKey.Size = new System.Drawing.Size(125, 20);
            this.buttonWiFiKey.TabIndex = 22;
            this.buttonWiFiKey.Text = "Set WiFi Key";
            this.buttonWiFiKey.Click += new System.EventHandler(this.buttonSetWiFiKey_Click);
            // 
            // labelNetKey
            // 
            this.labelNetKey.Location = new System.Drawing.Point(18, 74);
            this.labelNetKey.Name = "labelNetKey";
            this.labelNetKey.Size = new System.Drawing.Size(82, 20);
            this.labelNetKey.TabIndex = 20;
            this.labelNetKey.Text = "WiFi Key";
            // 
            // textBoxWiFiKey
            // 
            this.textBoxWiFiKey.Location = new System.Drawing.Point(106, 73);
            this.textBoxWiFiKey.Name = "textBoxWiFiKey";
            this.textBoxWiFiKey.Size = new System.Drawing.Size(146, 20);
            this.textBoxWiFiKey.TabIndex = 21;
            // 
            // buttonSetSSID
            // 
            this.buttonSetSSID.Location = new System.Drawing.Point(258, 48);
            this.buttonSetSSID.Name = "buttonSetSSID";
            this.buttonSetSSID.Size = new System.Drawing.Size(125, 20);
            this.buttonSetSSID.TabIndex = 19;
            this.buttonSetSSID.Text = "Set SSID";
            this.buttonSetSSID.Click += new System.EventHandler(this.buttonSetSSID_Click);
            // 
            // textBoxMACAddres
            // 
            this.textBoxMACAddres.Location = new System.Drawing.Point(106, 22);
            this.textBoxMACAddres.Name = "textBoxMACAddres";
            this.textBoxMACAddres.Size = new System.Drawing.Size(146, 20);
            this.textBoxMACAddres.TabIndex = 18;
            // 
            // buttonGetMACAddress
            // 
            this.buttonGetMACAddress.Location = new System.Drawing.Point(258, 22);
            this.buttonGetMACAddress.Name = "buttonGetMACAddress";
            this.buttonGetMACAddress.Size = new System.Drawing.Size(125, 20);
            this.buttonGetMACAddress.TabIndex = 17;
            this.buttonGetMACAddress.Text = "Get MAC Address";
            this.buttonGetMACAddress.Click += new System.EventHandler(this.buttonGetMACAddress_Click);
            // 
            // labelSSID
            // 
            this.labelSSID.Location = new System.Drawing.Point(18, 48);
            this.labelSSID.Name = "labelSSID";
            this.labelSSID.Size = new System.Drawing.Size(82, 20);
            this.labelSSID.TabIndex = 13;
            this.labelSSID.Text = "SSID";
            // 
            // textBoxSSID
            // 
            this.textBoxSSID.Location = new System.Drawing.Point(106, 47);
            this.textBoxSSID.Name = "textBoxSSID";
            this.textBoxSSID.Size = new System.Drawing.Size(146, 20);
            this.textBoxSSID.TabIndex = 14;
            // 
            // labelMACAddress
            // 
            this.labelMACAddress.Location = new System.Drawing.Point(18, 26);
            this.labelMACAddress.Name = "labelMACAddress";
            this.labelMACAddress.Size = new System.Drawing.Size(82, 20);
            this.labelMACAddress.TabIndex = 15;
            this.labelMACAddress.Text = "MAC Address";
            // 
            // groupBoxTraceApplication
            // 
            this.groupBoxTraceApplication.Controls.Add(this.lbText);
            this.groupBoxTraceApplication.Location = new System.Drawing.Point(3, 52);
            this.groupBoxTraceApplication.Name = "groupBoxTraceApplication";
            this.groupBoxTraceApplication.Size = new System.Drawing.Size(401, 146);
            this.groupBoxTraceApplication.TabIndex = 21;
            this.groupBoxTraceApplication.TabStop = false;
            this.groupBoxTraceApplication.Text = "Trace Application";
            // 
            // lbText
            // 
            this.lbText.ContextMenuStrip = this.contextMenuStrip;
            this.lbText.Location = new System.Drawing.Point(7, 19);
            this.lbText.Name = "lbText";
            this.lbText.Size = new System.Drawing.Size(383, 121);
            this.lbText.TabIndex = 1;
            this.lbText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.LbText_MouseClick);
            // 
            // panelDeviceStatus
            // 
            this.panelDeviceStatus.Controls.Add(this.checkBoxWiFiKey);
            this.panelDeviceStatus.Controls.Add(this.checkBoxWiFiSSID);
            this.panelDeviceStatus.Controls.Add(this.checkBoxAlive);
            this.panelDeviceStatus.Location = new System.Drawing.Point(3, 201);
            this.panelDeviceStatus.Name = "panelDeviceStatus";
            this.panelDeviceStatus.Size = new System.Drawing.Size(401, 55);
            this.panelDeviceStatus.TabIndex = 22;
            // 
            // checkBoxWiFiKey
            // 
            this.checkBoxWiFiKey.AutoSize = true;
            this.checkBoxWiFiKey.Enabled = false;
            this.checkBoxWiFiKey.Location = new System.Drawing.Point(109, 6);
            this.checkBoxWiFiKey.Name = "checkBoxWiFiKey";
            this.checkBoxWiFiKey.Size = new System.Drawing.Size(68, 17);
            this.checkBoxWiFiKey.TabIndex = 2;
            this.checkBoxWiFiKey.Text = "WiFi Key";
            this.checkBoxWiFiKey.UseVisualStyleBackColor = true;
            // 
            // checkBoxWiFiSSID
            // 
            this.checkBoxWiFiSSID.AutoSize = true;
            this.checkBoxWiFiSSID.Enabled = false;
            this.checkBoxWiFiSSID.Location = new System.Drawing.Point(7, 29);
            this.checkBoxWiFiSSID.Name = "checkBoxWiFiSSID";
            this.checkBoxWiFiSSID.Size = new System.Drawing.Size(75, 17);
            this.checkBoxWiFiSSID.TabIndex = 1;
            this.checkBoxWiFiSSID.Text = "WiFi SSID";
            this.checkBoxWiFiSSID.UseVisualStyleBackColor = true;
            // 
            // checkBoxAlive
            // 
            this.checkBoxAlive.AutoSize = true;
            this.checkBoxAlive.Enabled = false;
            this.checkBoxAlive.Location = new System.Drawing.Point(7, 6);
            this.checkBoxAlive.Name = "checkBoxAlive";
            this.checkBoxAlive.Size = new System.Drawing.Size(49, 17);
            this.checkBoxAlive.TabIndex = 0;
            this.checkBoxAlive.Text = "Alive";
            this.checkBoxAlive.UseVisualStyleBackColor = true;
            // 
            // panelActionButtons
            // 
            this.panelActionButtons.Controls.Add(this.buttonIsAlive);
            this.panelActionButtons.Controls.Add(this.buttonReboot);
            this.panelActionButtons.Controls.Add(this.cmdDisconnect);
            this.panelActionButtons.Controls.Add(this.cmdConnect);
            this.panelActionButtons.Location = new System.Drawing.Point(3, 507);
            this.panelActionButtons.Name = "panelActionButtons";
            this.panelActionButtons.Size = new System.Drawing.Size(401, 25);
            this.panelActionButtons.TabIndex = 23;
            // 
            // buttonIsAlive
            // 
            this.buttonIsAlive.Location = new System.Drawing.Point(308, 3);
            this.buttonIsAlive.Name = "buttonIsAlive";
            this.buttonIsAlive.Size = new System.Drawing.Size(72, 20);
            this.buttonIsAlive.TabIndex = 21;
            this.buttonIsAlive.Text = "Is Alive";
            this.buttonIsAlive.Click += new System.EventHandler(this.buttonIsAlive_Click);
            // 
            // buttonReboot
            // 
            this.buttonReboot.Location = new System.Drawing.Point(208, 2);
            this.buttonReboot.Name = "buttonReboot";
            this.buttonReboot.Size = new System.Drawing.Size(72, 20);
            this.buttonReboot.TabIndex = 20;
            this.buttonReboot.Text = "Reboot";
            this.buttonReboot.Click += new System.EventHandler(this.buttonReboot_Click);
            // 
            // cmdDisconnect
            // 
            this.cmdDisconnect.Location = new System.Drawing.Point(108, 2);
            this.cmdDisconnect.Name = "cmdDisconnect";
            this.cmdDisconnect.Size = new System.Drawing.Size(72, 20);
            this.cmdDisconnect.TabIndex = 3;
            this.cmdDisconnect.Text = "Disconnect";
            this.cmdDisconnect.Click += new System.EventHandler(this.cmdDisconnect_Click);
            // 
            // cmdConnect
            // 
            this.cmdConnect.Location = new System.Drawing.Point(8, 2);
            this.cmdConnect.Name = "cmdConnect";
            this.cmdConnect.Size = new System.Drawing.Size(72, 20);
            this.cmdConnect.TabIndex = 2;
            this.cmdConnect.Text = "Connect";
            this.cmdConnect.Click += new System.EventHandler(this.cmdConnect_Click);
            // 
            // groupBoxStatusInfo
            // 
            this.groupBoxStatusInfo.Controls.Add(this.textBoxStatusInfo);
            this.groupBoxStatusInfo.Location = new System.Drawing.Point(3, 2);
            this.groupBoxStatusInfo.Name = "groupBoxStatusInfo";
            this.groupBoxStatusInfo.Size = new System.Drawing.Size(401, 44);
            this.groupBoxStatusInfo.TabIndex = 22;
            this.groupBoxStatusInfo.TabStop = false;
            this.groupBoxStatusInfo.Text = "Status Information";
            // 
            // textBoxStatusInfo
            // 
            this.textBoxStatusInfo.Location = new System.Drawing.Point(7, 15);
            this.textBoxStatusInfo.Name = "textBoxStatusInfo";
            this.textBoxStatusInfo.Size = new System.Drawing.Size(383, 20);
            this.textBoxStatusInfo.TabIndex = 0;
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(61, 4);
            // 
            // MainFormFrontEnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(407, 532);
            this.Controls.Add(this.groupBoxStatusInfo);
            this.Controls.Add(this.panelActionButtons);
            this.Controls.Add(this.panelDeviceStatus);
            this.Controls.Add(this.groupBoxTraceApplication);
            this.Controls.Add(this.panelSTMManagement);
            this.Controls.Add(this.panelSerialConfiguration);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainFormFrontEnd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ST Nucleo Desktop Management";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EventFormClosed);
            this.panelSerialConfiguration.ResumeLayout(false);
            this.panelSerialConfiguration.PerformLayout();
            this.panelSTMManagement.ResumeLayout(false);
            this.panelSTMManagement.PerformLayout();
            this.groupBoxTraceApplication.ResumeLayout(false);
            this.panelDeviceStatus.ResumeLayout(false);
            this.panelDeviceStatus.PerformLayout();
            this.panelActionButtons.ResumeLayout(false);
            this.groupBoxStatusInfo.ResumeLayout(false);
            this.groupBoxStatusInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelSerialConfiguration;
        private System.Windows.Forms.Button buttonUpdateCOMList;
        private System.Windows.Forms.ComboBox comboBoxCOMPortList;
        private System.Windows.Forms.Label labelSerialSpeed;
        private System.Windows.Forms.TextBox textBoxComSpeed;
        private System.Windows.Forms.Label labelSerialPort;
        private System.Windows.Forms.Panel panelSTMManagement;
        private System.Windows.Forms.Button buttonWiFiKey;
        private System.Windows.Forms.Label labelNetKey;
        private System.Windows.Forms.TextBox textBoxWiFiKey;
        private System.Windows.Forms.Button buttonSetSSID;
        private System.Windows.Forms.TextBox textBoxMACAddres;
        private System.Windows.Forms.Button buttonGetMACAddress;
        private System.Windows.Forms.Label labelSSID;
        private System.Windows.Forms.TextBox textBoxSSID;
        private System.Windows.Forms.Label labelMACAddress;
        private System.Windows.Forms.GroupBox groupBoxTraceApplication;
        private System.Windows.Forms.ListBox lbText;
        private System.Windows.Forms.Panel panelDeviceStatus;
        private System.Windows.Forms.CheckBox checkBoxWiFiKey;
        private System.Windows.Forms.CheckBox checkBoxWiFiSSID;
        private System.Windows.Forms.CheckBox checkBoxAlive;
        private System.Windows.Forms.Panel panelActionButtons;
        private System.Windows.Forms.Button buttonIsAlive;
        private System.Windows.Forms.Button buttonReboot;
        private System.Windows.Forms.Button cmdDisconnect;
        private System.Windows.Forms.Button cmdConnect;
        private System.Windows.Forms.GroupBox groupBoxStatusInfo;
        private System.Windows.Forms.TextBox textBoxStatusInfo;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
    }
}

