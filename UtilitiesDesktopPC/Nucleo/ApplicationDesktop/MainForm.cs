﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using STNucleoConnector;
using System.Reflection;

namespace TestSerial
{
    public partial class MainFormFrontEnd : Form
    {
        #region Constants
        const String pathFileSettings                       = "SettingsApplication.csv";
        #endregion Constants

        #region Attributes.Private
        private STConnectorManagement       stConnectionManager = null;
        private SettingsNucleoConnector     settingsApplication = null;
        private Timer                       dispatcherTimer     = null;
        private ApplicationSettings         applicationSettings = null;
        private Dictionary<TypeTraceNotification, String> listVisualizationPrefixTrace = new Dictionary<TypeTraceNotification, string>()
        {
            {TypeTraceNotification.NONE,"" },
            {TypeTraceNotification.SERIALEVENT,"SERIALEVENT" },
            {TypeTraceNotification.APPLICATIONINFO,"APPINFO" },
            {TypeTraceNotification.SERIALSENDINFO,"SERIALSEND" },
            {TypeTraceNotification.RECEIVEDINFO,"SERIALRECEIVED" },
            {TypeTraceNotification.ERROR,"ERROR" },
            {TypeTraceNotification.DEBUG,"DEBUG" }
        };
        #endregion Attributes.Private

            #region Constructor
        public MainFormFrontEnd()
        {
            try
            {
                InitializeComponent();

                CreateContextMenuStrip();

                UpdateCOMPortList();

                string version = System.Reflection.Assembly.GetExecutingAssembly()
                                                           .GetName()
                                                           .Version
                                                           .ToString();

                this.Text = TitleApplication + " Version:" + version;

                applicationSettings = new ApplicationSettings();

                if (applicationSettings.LoadFromFile(pathFileSettings))
                {
                    ApplySettingsToGUI(applicationSettings);
                }
            }
            catch(Exception exc)
            {
                EffectivePrint(exc.Message, TypeTraceNotification.ERROR);
            }
        }
        #endregion Constructor

        #region SettingsApplication
        private String TitleApplication
        {
            get
            {
                var attributes = System.Reflection.Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    var titleAttribute = (System.Reflection.AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title.Length > 0)
                        return titleAttribute.Title;
                }

                return "";
            }
        }

        private void ApplySettingsToGUI(ApplicationSettings appSettings)
        {
            try
            { 
                textBoxComSpeed.Text        = appSettings.BaudRate.ToString();
                comboBoxCOMPortList.Text    = applicationSettings.SerialPort;
                textBoxSSID.Text            = applicationSettings.WiFiSSID;
                textBoxWiFiKey.Text         = applicationSettings.WiFiKey;
            }
            catch(Exception)
            {

            }
        }

        private void ExtractSettingsFromGUI(ref ApplicationSettings appSettings)
        {
            try
            {
                appSettings.BaudRate            = int.Parse(textBoxComSpeed.Text);
                applicationSettings.SerialPort  = comboBoxCOMPortList.SelectedItem.ToString();

                if (checkBoxWiFiSSID.Checked == true)
                {
                    applicationSettings.WiFiSSID = textBoxSSID.Text;
                }

                if(checkBoxWiFiKey.Checked==true)
                { 
                    applicationSettings.WiFiKey     = textBoxWiFiKey.Text;
                }
            }
            catch (Exception)
            {
            }
        }
        #endregion SettingsApplication

        #region GraphicMethods

        private void CreateContextMenuStrip()
        {
            var ToolStripMenuCopyToClipboard    = new ToolStripMenuItem {Text="Copy to Clipboard"};
            var ToolStripMenuClearListBox       = new ToolStripMenuItem { Text = "Clear" };
            ToolStripMenuCopyToClipboard.Click += copyToClipboardToolStripMenuItem_Click;
            ToolStripMenuClearListBox.Click     += clearTraceInfoToolStripMenuItem_Click;

            contextMenuStrip.Items.Add(ToolStripMenuCopyToClipboard);
            contextMenuStrip.Items.Add(ToolStripMenuClearListBox);
        }

        private void LbText_MouseClick(object sender, MouseEventArgs e)
        {
            //if(e.Button==MouseButtons.Right)
            {
                var index = lbText.IndexFromPoint(e.Location);
                {
                    contextMenuStrip.Show(Cursor.Position);
                    contextMenuStrip.Visible = true;
                }
            }
        }

        private void UpdateCOMPortList()
        {
            try
            {
                string[] listCOMPort = System.IO.Ports.SerialPort.GetPortNames();

                comboBoxCOMPortList.Items.Clear();

                if (listCOMPort != null)
                {
                    foreach (string comPort in listCOMPort)
                    {
                        comboBoxCOMPortList.Items.Add(comPort);
                    }
                }
            }
            catch (Exception)
            {

            }
        }
        #endregion GraphicMethods

        #region TimerMethods
        private void EnablePeriodicTimerCheck()
        {
            try
            {
                if (dispatcherTimer == null)
                {
                    dispatcherTimer = new Timer();
                    dispatcherTimer.Interval = 500;
                    dispatcherTimer.Tick += Timer_Tick;
                    dispatcherTimer.Start();
                }
            }
            catch(Exception)
            {

            }
        }

        private void StopTimer()
        {
            try
            {
                if (dispatcherTimer != null)
                {
                    dispatcherTimer.Stop();
                    dispatcherTimer.Dispose();

                    dispatcherTimer = null;
                }
            }
            catch (Exception)
            {
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            PeriodicConnectionManagerCheck();
        }
        #endregion TimerMethods

        #region GraphicEvents
        private void cmdConnect_Click(object sender, EventArgs e)
        {
            try
            {
                ExtractSettingsFromGUI(ref applicationSettings);
                StartConnectorManager(applicationSettings.SerialPort, applicationSettings.BaudRate);

                EnablePeriodicTimerCheck();
            }
            catch(Exception exc)
            {
                EffectivePrint(exc.Message, TypeTraceNotification.ERROR);
            }
        }

        private void buttonUpdateCOMList_Click(object sender, EventArgs e)
        {
            UpdateCOMPortList();
        }

        private void cmdDisconnect_Click(object sender, EventArgs e)
        {
            Stop();
        }

        #endregion GraphicEvents

        #region ConnectionManager
        public void StartConnectorManager(string COMPort, int BaudRate)
        {
            if (stConnectionManager == null)
            {
                settingsApplication = new SettingsNucleoConnector();

                settingsApplication.COMPort = COMPort;
                settingsApplication.BaudRate = BaudRate;

                stConnectionManager = new STConnectorManagement(settingsApplication);
                stConnectionManager.SetCallBackEvent(Print);
            }

            stConnectionManager.Start();
        }

        public void Stop()
        {
            try
            {
                if (stConnectionManager != null)
                {
                    stConnectionManager.Stop();
                    stConnectionManager = null;

                    ExtractSettingsFromGUI(ref applicationSettings);
                    applicationSettings.SaveInFile(pathFileSettings);
                }
                StopTimer();
            }
            catch (Exception)
            {

            }
        }

        private void DetachGraphicEvents()
        {
            if (stConnectionManager != null)
            {
                stConnectionManager.DetachCallBackEvent();
            }
        }


        public void CheckIsAlive()
        {
            if (stConnectionManager != null)
            {
                stConnectionManager.CheckIsAlive();
            }
        }

        public void ForceReboot()
        {
            if (stConnectionManager != null)
            {
                stConnectionManager.Reboot();
            }
        }

        public void RequestMACAddress()
        {
            if (stConnectionManager != null)
            {
                stConnectionManager.RequestMACAddress();
            }
        }
        public void SetSSID(string SSID)
        {
            if (stConnectionManager != null)
            {
                stConnectionManager.SetSSID(SSID);
            }
        }
        public void SetWiFiKey(string WiFiKey)
        {
            if (stConnectionManager != null)
            {
                stConnectionManager.SetWiFiKey(WiFiKey);
            }
        }

        public void PeriodicConnectionManagerCheck()
        {
            try
            {
                if (stConnectionManager != null)
                {
                    while (stConnectionManager.queueAnswer.Count > 0)
                    {
                        STAnswer answer = stConnectionManager.queueAnswer.Dequeue();

                        switch (answer.command)
                        {
                            case TypeCommand.NONE:
                                break;
                            case TypeCommand.ISALIVE:
                                UpdateStatus("Device is Alive");
                                UpdateStatusIsAlive(true);
                                break;
                            case TypeCommand.GETMACADDRESS:
                                if (answer.contentReceived != null)
                                {
                                    UpdateMACAddress(answer.contentReceived.ToString());
                                }
                                break;
                            case TypeCommand.SETSSID:
                                if (answer.contentReceived != null)
                                {
                                    switch((ResultBoardSetting)answer.contentReceived)
                                    {
                                        case ResultBoardSetting.SUCCESS:
                                            UpdateStatus("SSID Correctly SET");
                                            UpdateStatusWiFiSSiD(true);
                                            break;

                                        case ResultBoardSetting.FAILED:
                                            UpdateStatus("SSID NOT SET");
                                            UpdateStatusWiFiSSiD(false);
                                            break;
                                    }
                                }
                                break;
                            case TypeCommand.SETNETWORKKEY:
                                if (answer.contentReceived != null)
                                {
                                    switch ((ResultBoardSetting)answer.contentReceived)
                                    {
                                        case ResultBoardSetting.SUCCESS:
                                            UpdateStatus("WiFi Key Correctly SET");
                                            UpdateStatusWiFiKey(true);
                                            break;

                                        case ResultBoardSetting.FAILED:
                                            UpdateStatus("WiFi Key NOT SET");
                                            UpdateStatusWiFiKey(false);
                                            break;
                                    }
                                }
                                break;
                            case TypeCommand.REBOOT:
                                break;
                            default:
                                break;
                        }
                    }

                }
            }
            catch(Exception)
            {

            }
        }


        #endregion ConnectionManager

        #region Callback.Events
        private void Print(String Line, TypeTraceNotification Parameter)
        {
            Invoke(new STNucleoConnector.STNucleoConnector.OnWrite(EffectivePrint), Line, Parameter);
        }

        private void EffectivePrint(String Line, TypeTraceNotification Parameter)
        {
            string Prefix = "";

            if (Parameter == TypeTraceNotification.STATUS)
            {
                UpdateStatus(Line);
            }
            else
            {
                if (listVisualizationPrefixTrace.ContainsKey(Parameter))
                {
                    Prefix = listVisualizationPrefixTrace[Parameter];
                }

                lbText.Items.Insert(0, Prefix + ": " + Line);
            }
        }

        private void UpdateMACAddress(string contentReceived)
        {
            textBoxMACAddres.Text = contentReceived;
        }

        private void UpdateStatus(string Status)
        {
            textBoxStatusInfo.Text = Status;
        }

        private void UpdateStatusWiFiSSiD(bool status)
        {
            this.checkBoxWiFiSSID.Checked = status;
        }
        private void UpdateStatusWiFiKey(bool status)
        {
            this.checkBoxWiFiKey.Checked = status;
        }
        private void UpdateStatusIsAlive(bool status)
        {
            this.checkBoxAlive.Checked = status;
        }
        #endregion Callback.Events

        #region GraphicEvents.Application

        private void GetSerialSettings(ref String serialPort,ref int baudrate)
        {
            try
            {
                baudrate    = int.Parse(textBoxComSpeed.Text);
                serialPort  = comboBoxCOMPortList.SelectedItem.ToString();
            }
            catch(Exception)
            {

            }
        }

        private void buttonIsAlive_Click(object sender, EventArgs e)
        {
            CheckIsAlive();
        }

        private void buttonReboot_Click(object sender, EventArgs e)
        {
            ForceReboot();
        }

        private void EventFormClosed(object sender, FormClosingEventArgs e)
        {
            DetachGraphicEvents();
            Stop();
        }

        private void buttonGetMACAddress_Click(object sender, EventArgs e)
        {
            RequestMACAddress();
        }

        private void buttonSetSSID_Click(object sender, EventArgs e)
        {
            SetSSID(textBoxSSID.Text);
        }

        private void buttonSetWiFiKey_Click(object sender, EventArgs e)
        {
            SetWiFiKey(textBoxWiFiKey.Text);
        }

        private void clearTraceInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lbText.Items.Clear();
        }
        private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.lbText.Items.Count > 0)
            {
                String text = "";

                foreach (string line in this.lbText.Items)
                {
                    text += line + "\r\n";
                }
                Clipboard.SetText(text);
                UpdateStatus("Trace Info copied into Clipboard");
            }
            else
            {
                UpdateStatus("No Trace Info to copy");
            }

        }

        #endregion GraphicEvents.Application
    }
}
