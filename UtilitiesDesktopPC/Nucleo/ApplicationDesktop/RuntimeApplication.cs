﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace TestSerial
{
    public delegate void OnWrite(String Line, int Parameter);

    public class RuntimeApplication
    {
        public string COMPort = "COM1";
        public int COMSpeed = 0;
        public SerialPort COMObj = null;
        public event OnWrite Write = null;
        private Thread ThreadObj = null;
        private bool Terminated = false;
        private string PathFileLog = "";

        public RuntimeApplication(string COMPort, int COMSpeed)
        {
            this.COMPort    = COMPort;
            this.COMSpeed   = COMSpeed;
        }

        public void Start()
        {
            if (ThreadObj == null)
            {
                try
                {
                    COMObj = new SerialPort(COMPort, COMSpeed, Parity.None, 8, StopBits.One);

                    COMObj.Open();
                    Print("OPEN " + COMPort + " at Speed " + COMSpeed.ToString() + " SUCCESS", 1);


                }
                catch (Exception Exc)
                {
                    Print(Exc.Message, 2);
                    Terminated = true;
                }


                ThreadObj = new Thread(new ThreadStart(Execute));
                ThreadObj.Start();
            }
        }

        public void Stop()
        {
            Terminated = true;
        }

        public void Execute()
        {

            byte[] ArrayData = new byte[128];
            int sizeRead = 0;
            //String  InputStream = "";

            System.IO.FileStream sw = null;
            System.IO.BinaryWriter bw = null;

            try
            {
                sw = new System.IO.FileStream(PathFileLog, System.IO.FileMode.Create);

                bw = new System.IO.BinaryWriter(sw);
            }
            catch (Exception exc)
            {
                Print(exc.Message, 2);
            }

            while (!Terminated)
            {
                try
                {
                    //COMObj.Write("TEST\r\n");

                    if (COMObj.ReadBufferSize > 0)
                    {

                        sizeRead = COMObj.Read(ArrayData, 0, ArrayData.Length);

                        //sizeRead = 10;

                        //for (int index = 0; index < 10; index++)
                        //    ArrayData[index] = (byte)(index + 1);

                        Print("Reading " + sizeRead + " bytes", 0);

                        if (sizeRead > 0)
                        {
                            //InputStream = Encoding.ASCII.GetString(ArrayData, 0, sizeRead);
                            //Print(InputStream, 0);

                            if (sw != null && bw != null)
                            {
                                bw.Write(ArrayData, 0, sizeRead);
                                bw.Flush();

                                Print("Logging " + sizeRead + " bytes", 0);
                            }
                        }
                        else
                        {
                            //                        COMObj.Write("TEST\r\n");
                        }

                    }
                }
                catch (Exception Exc)
                {
                    Print(Exc.Message, 2);
                }

                Thread.Sleep(100);
            }

            if (bw != null)
            {
                bw.Close();
            }

            if (sw != null)
            {
                sw.Close();
            }
            if (COMObj != null)
            {
                COMObj.Close();
            }
            COMObj = null;
            ThreadObj = null;

            Print("APPLICATION CORRECTLY CLOSED", 1);
        }

        private void Print(String Line, int Parameter)
        {
            if (Write != null)
                Write(Line, Parameter);
        }
    }
}
