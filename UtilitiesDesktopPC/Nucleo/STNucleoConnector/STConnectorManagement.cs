﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace STNucleoConnector
{
    public class STConnectorManagement
    {
        private event STNucleoConnector.OnWrite onWriteEvent = null;
        private Timer timer = null;
        private STNucleoConnector stNucleoConnector = null;
        private Queue<String> listPacketToSend = new Queue<string>();
        public Queue<STAnswer> queueAnswer = new Queue<STAnswer>();

        public STConnectorManagement(SettingsNucleoConnector settingsApp)
        {
            stNucleoConnector = new STNucleoConnector(settingsApp);
        }

        public void Start()
        {
            stNucleoConnector.onWriteEvent += onWriteEvent;
            stNucleoConnector.Connect();

            timer = new Timer(100);
            timer.Elapsed += new ElapsedEventHandler(EventTimerSendingInformation);
            timer.Enabled = true; // Enable it
        }

        public void SetCallBackEvent(STNucleoConnector.OnWrite onWriteext)
        {
            this.onWriteEvent = onWriteext;
        }

        public void DetachCallBackEvent()
        {
            if (stNucleoConnector != null)
            {
                stNucleoConnector.onWriteEvent -= onWriteEvent;
            }
        }

        public void Print(String line, TypeTraceNotification traceNotification)
        {
            if(onWriteEvent!=null)
            {
                onWriteEvent(line, traceNotification);
            }
        }

        private void EventTimerSendingInformation(object state,System.Timers.ElapsedEventArgs eventArgs)
        {
            try
            {
                while (listPacketToSend.Count > 0)
                {
                    String packetToSend = listPacketToSend.Dequeue();

                    stNucleoConnector.SendText(packetToSend);
                }

                while (stNucleoConnector.listPacketReceived.Count > 0)
                {
                    String packetReceived = stNucleoConnector.listPacketReceived.Dequeue();

                    ParsePacket(packetReceived);
                }
            }
            catch(Exception)
            {

            }
        }

        private void ParsePacket(String PacketReceived)
        {
            String[]        listString      = PacketReceived.Split(NucleoDictionaryCommand.Separator.ToCharArray());
            TypeCommand     getTypeCommand  = TypeCommand.NONE;
            STAnswer        stAnswer        = null;

            for(int indexField=0;indexField<listString.Length;indexField++)
            {
                String singleParam = listString[indexField];

                switch (indexField)
                {
                    case 0:

                        break;

                    case 1:
                        getTypeCommand  = TranslateCommand(singleParam);
                        stAnswer        = new STAnswer(getTypeCommand, null);
                        break;

                    case 2:
                        switch (getTypeCommand)
                        {
                            case TypeCommand.SETNETWORKKEY:
                            case TypeCommand.SETSSID:
                                if (ResultBoardTranslation.checkResult.ContainsKey(singleParam))
                                {
                                    stAnswer.contentReceived = (ResultBoardSetting)ResultBoardTranslation.checkResult[singleParam];
                                }
                            break;
                            default:
                                stAnswer.contentReceived = singleParam;
                                break;
                        }
                        break;
                }
            }
            if (stAnswer != null)
            {
                queueAnswer.Enqueue(stAnswer);
            }
        }

        public void Stop()
        {
            if(stNucleoConnector!=null)
            {
                stNucleoConnector.Disconnect();
            }
        }

        private TypeCommand TranslateCommand(String CommandText)
        {
            foreach(TypeCommand command in NucleoDictionaryCommand.conversion.Keys)
            {
                if(NucleoDictionaryCommand.conversion[command]==CommandText)
                {
                    return command;
                }
            }

            return TypeCommand.NONE;
        }

        private void RequestGeneric(TypeCommand command,List<String> parameters)
        {
            String          PacketToSend;
            StringBuilder   stringBuilder = new StringBuilder();

            stringBuilder.Append(NucleoDictionaryCommand.MessagePrefixToSend);
            stringBuilder.Append(NucleoDictionaryCommand.Separator);
            stringBuilder.Append(NucleoDictionaryCommand.conversion[command]);

            if(parameters!=null && parameters.Count>0)
            {
                foreach(string parameter in parameters)
                {
                    stringBuilder.Append(NucleoDictionaryCommand.Separator);
                    stringBuilder.Append(parameter);
                }
            }

            stringBuilder.Append(NucleoDictionaryCommand.Terminator);

            PacketToSend = stringBuilder.ToString();

            listPacketToSend.Enqueue(PacketToSend);
        }

        public void RequestMACAddress()
        {
            Print("REQUEST MAC ADDRESS", TypeTraceNotification.APPLICATIONINFO);
            RequestGeneric(TypeCommand.GETMACADDRESS, null);
        }

        public void SetSSID(string SSID)
        {
            Print("SET SSID"+ SSID, TypeTraceNotification.APPLICATIONINFO);
            RequestGeneric(TypeCommand.SETSSID, new List<string>() {SSID});
        }

        public void SetWiFiKey(string WiFiKey)
        {
            Print("SET SecureKey" + WiFiKey, TypeTraceNotification.APPLICATIONINFO);
            RequestGeneric(TypeCommand.SETNETWORKKEY, new List<string>() { WiFiKey });
        }
        public void Reboot()
        {
            Print("REQUEST REBOOT", TypeTraceNotification.APPLICATIONINFO);
            RequestGeneric(TypeCommand.REBOOT, null);
        }
        public void CheckIsAlive()
        {
            Print("CHECK IS ALIVE", TypeTraceNotification.APPLICATIONINFO);
            RequestGeneric(TypeCommand.ISALIVE, null);
        }
    }
}
