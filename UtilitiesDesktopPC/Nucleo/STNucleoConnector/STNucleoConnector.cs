﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;


namespace STNucleoConnector
{
    public class STNucleoConnector
    {
        public delegate void OnWrite(String Line, TypeTraceNotification Parameter);

        const int Timeoutms = 5000;

        public event OnWrite onWriteEvent = null;

        private SettingsNucleoConnector     settingsApplication         = null;
        private Queue<String>           listPacketToSend            = new Queue<string>();
        private SerialPort              serialPort                  = null;
        private Thread                  ThreadObj                   = null;
        private bool                    Terminated                  = false;
        private FSMStatus               status                      = FSMStatus.WAITFORPACKET;
        private byte[]                  globalBuffer                = new byte[1024];
        private int                     indexGlobalBuffer           = 0;
        private int                     indexGlobalTerminator       = 0;

        public Queue<String>            listPacketReceived          = new Queue<string>();


        public STNucleoConnector(SettingsNucleoConnector settingsApplication)
        {
            this.settingsApplication = settingsApplication;
        }

        public void Connect()
        {
            try
            {

                if (ThreadObj == null)
                {
                    try
                    {
                        serialPort = new SerialPort(settingsApplication.COMPort, settingsApplication.BaudRate, Parity.None, 8, StopBits.One);

                        serialPort.Open();
                        Print("OPEN " + settingsApplication.COMPort + " at Speed " + settingsApplication.BaudRate.ToString() + " SUCCESS", TypeTraceNotification.SERIALEVENT);
                        Print("APPLICATION IS CONNECTED WITH DEVICE", TypeTraceNotification.STATUS);
                    }
                    catch (Exception Exc)
                    {
                        Print(Exc.Message, TypeTraceNotification.ERROR);
                        Terminated = true;
                    }


                    ThreadObj = new Thread(new ThreadStart(Execute));
                    ThreadObj.Start();
                }
            }
            catch (Exception)
            {

            }
        }

        public void Disconnect()
        {
            Terminated = true;
        }

        private int FSMReadingMachine(byte[] bufferPacket,int size)
        {
            String  PacketCompleted         = "";
            int     numberPacketCompleted   = 0;

            foreach(byte singleByte in bufferPacket)
            {
                switch (status)
                {
                    case FSMStatus.WAITFORPACKET:

                        if(singleByte== (byte)NucleoDictionaryCommand.MessagePrefixToReceive[0])
                        {
                            ResetLocalBuffer();

                            globalBuffer[0]     = singleByte;
                            indexGlobalBuffer   = 1;
                            status              = FSMStatus.ATTACHTOBEGIN;
                        }

                        break;
                    case FSMStatus.ATTACHTOBEGIN:
                        if(indexGlobalBuffer==NucleoDictionaryCommand.MessagePrefixToReceive.Length)
                        {
                            if((char)singleByte== NucleoDictionaryCommand.Separator[0])
                            {
                                if(System.Text.Encoding.Default.GetString(globalBuffer,0, indexGlobalBuffer)== NucleoDictionaryCommand.MessagePrefixToReceive)
                                {
                                    status = FSMStatus.WAITFOREND;
                                }

                                globalBuffer[indexGlobalBuffer++] = singleByte;
                            }
                            else
                            {
                                status = FSMStatus.WAITFORPACKET;
                            }
                        }
                        else
                        {
                            globalBuffer[indexGlobalBuffer++] = singleByte;
                        }

                        break;
                    case FSMStatus.WAITFOREND:
                        if(indexGlobalBuffer>= globalBuffer.Length)
                        {
                            status = FSMStatus.WAITFORPACKET;
                        }
                        else
                        {
                            if(indexGlobalTerminator<NucleoDictionaryCommand.Terminator.Length)
                            {
                                if((char)singleByte== NucleoDictionaryCommand.Terminator[indexGlobalTerminator])
                                {
                                    indexGlobalTerminator++;

                                    if(indexGlobalTerminator == NucleoDictionaryCommand.Terminator.Length)
                                    {
                                        PacketCompleted = System.Text.Encoding.Default.GetString(globalBuffer, 0, (int)indexGlobalBuffer);

                                        listPacketReceived.Enqueue(PacketCompleted);

                                        ResetLocalBuffer();

                                        numberPacketCompleted++;

                                        status = FSMStatus.WAITFORPACKET;
                                    }
                                }
                                else
                                {
                                    globalBuffer[indexGlobalBuffer++] = singleByte;
                                }
                            }
                        }

                        break;
                    default:
                        break;
                }
            }

            return numberPacketCompleted;
        }

        private void ResetLocalBuffer()
        {
            for (int indexBuffer = 0; indexBuffer < indexGlobalBuffer; indexBuffer++)
            {
                globalBuffer[indexBuffer] = 0;
            }

            indexGlobalBuffer       = 0;
            indexGlobalTerminator   = 0;
        }

        private void Execute()
        {
            try
            {
                TimeSpan    timeout;
                DateTime    timeStart;
                int         packetCompleted;
                byte[]      bufferTempRead;

                while (!Terminated)
                {
                    if (serialPort != null)
                    {
                        while (listPacketToSend.Count>0)
                        {
                            String command = listPacketToSend.Dequeue(); 
                            {
                                Print("SEND COMMAND: " + command, TypeTraceNotification.SERIALSENDINFO);

                                serialPort.Write(Encoding.ASCII.GetBytes(command), 0, command.Length);

                                timeout     = new TimeSpan(1);
                                timeStart   = DateTime.Now;

                                packetCompleted = 0;

                                while (!Terminated && packetCompleted == 0 && timeout.TotalMilliseconds < Timeoutms)
                                {
                                    int readbytesNumber = serialPort.BytesToRead;

                                    if (readbytesNumber > 0)
                                    {
                                        int counterBytes    = readbytesNumber;

                                        bufferTempRead      = new byte[counterBytes];
                                        readbytesNumber     = serialPort.Read(bufferTempRead, 0, (int)counterBytes);
                                        packetCompleted     = FSMReadingMachine(bufferTempRead, counterBytes);

                                        Print(System.Text.Encoding.Default.GetString(bufferTempRead,0, counterBytes).ToString(), TypeTraceNotification.RECEIVEDINFO);
                                    }
                                    if (readbytesNumber == 0)
                                    {
                                        Thread.Sleep(1);
                                    }

                                    timeout = DateTime.Now - timeStart;
                                }

                                if(packetCompleted==0)
                                {
                                    Print("NO ANSWER (TIMEOUT EXPIRED)", TypeTraceNotification.DEBUG);
                                }


                                serialPort.BaseStream.Flush();
                            }
                        }
                    }

                    Thread.Sleep(10);
                }

                if(serialPort!=null)
                {
                    serialPort.Dispose();
                    serialPort = null;
                }

                Print("APPLICATION TERMINATED", TypeTraceNotification.APPLICATIONINFO);
                Print("APPLICATION DISCONNECTED", TypeTraceNotification.STATUS);
            }
            catch (Exception exc)
            {
                Print(exc.Message, 0);
            }
        }

        private void Print(String traceInfo, TypeTraceNotification verbosity)
        {
            if (onWriteEvent != null)
                onWriteEvent(traceInfo, verbosity);
        }

        public void SendText(string textToSend)
        {
            try
            {
                if (serialPort != null)
                {
                    listPacketToSend.Enqueue(textToSend);
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
