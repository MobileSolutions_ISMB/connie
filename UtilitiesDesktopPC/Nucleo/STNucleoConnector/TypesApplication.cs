﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STNucleoConnector
{
    public class NucleoDictionaryCommand
    {
        public const String MessagePrefixToSend    = "STMR";
        public const String MessagePrefixToReceive = "STMA";
        public const String CommandIsAlive         = "ISALIVE";
        public const String CommandGetMACAddress   = "GETMAC";
        public const String CommandSetSSID         = "SETSSID";
        public const String CommandSetNetworkKey   = "SETNETKEY";
        public const String CommandReboot          = "REBOOT";
        public const String Separator              = ";";
        public const String Terminator             = "\r\n";

        public static Dictionary<TypeCommand, String> conversion = new Dictionary<TypeCommand, string>()
        {
            {TypeCommand.ISALIVE,CommandIsAlive},
            {TypeCommand.GETMACADDRESS,CommandGetMACAddress},
            {TypeCommand.SETSSID,CommandSetSSID},
            {TypeCommand.SETNETWORKKEY,CommandSetNetworkKey},
            {TypeCommand.REBOOT,CommandReboot}
        };
    }

    enum FSMStatus
    {
        WAITFORPACKET,
        ATTACHTOBEGIN,
        WAITFOREND
    }

    public enum TypeCommand
    {
        NONE            = 0,
        ISALIVE         = 1,
        GETMACADDRESS   = 2,
        SETSSID         = 3,
        SETNETWORKKEY   = 4,
        REBOOT          = 5
    }

    public enum ResultBoardSetting
    {
        NONE,
        SUCCESS,
        FAILED
    }

    public enum TypeTraceNotification
    {
        NONE,
        SERIALEVENT,
        APPLICATIONINFO,
        SERIALSENDINFO,
        RECEIVEDINFO,
        STATUS,
        ERROR,
        DEBUG
    }

    public static class ResultBoardTranslation
    {
        public static Dictionary<String, ResultBoardSetting> checkResult;

        static ResultBoardTranslation()
        {
            checkResult = new Dictionary<string, ResultBoardSetting>()
            {
                {"OK", ResultBoardSetting.SUCCESS},
                {"KO", ResultBoardSetting.FAILED}
            };
        }
    }


    public class STAnswer
    {
        public TypeCommand      command;
        public Object           contentReceived;
        
        public STAnswer(TypeCommand command, String contentReceived)
        {
            this.command            = command;
            this.contentReceived    = contentReceived;
        }
    }

    public class SettingsNucleoConnector
    {
        IDictionary<String,Object> appSettings;

        const string KeySerialPort  = "Serial";
        const string KeyBaudRate    = "Baudrate";

        public SettingsNucleoConnector()
        {
            // Get the settings for this application.
            appSettings = new Dictionary<String,Object>();
        }

        /// <summary>
        /// Update a setting value for our application. If the setting does not
        /// exist, then add the setting.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool AddOrUpdateValue(string Key, Object valueToSet)
        {
            bool valueChanged = false;

            // If the key exists
            if (appSettings.Values.Contains(Key))
            {
                // If the value has changed
                if (appSettings[Key] != valueToSet)
                {
                    // Store the new value
                    appSettings[Key] = valueToSet;
                    valueChanged = true;
                }
            }
            // Otherwise create the key.
            else
            {
                appSettings.Add(Key, valueToSet);
                valueChanged = true;
            }
            return valueChanged;
        }

        public int GetValueOrDefaultInteger(String Key, int defaultValue)
        {
            int value;

            // If the key exists, retrieve the value.
            if (appSettings.ContainsKey(Key))
            {
                value = Int32.Parse(appSettings[Key].ToString());
            }
            // Otherwise, use the default value.
            else
            {
                value = defaultValue;
            }
            return value;
        }


        public String GetValueOrDefaultString(string Key, String defaultValue)
        {
            String value;

            // If the key exists, retrieve the value.
            if (appSettings.ContainsKey(Key))
            {
                value = appSettings[Key].ToString();
            }
            // Otherwise, use the default value.
            else
            {
                value = defaultValue;
            }
            return value;
        }

        public string COMPort
        {
            get
            {
                return GetValueOrDefaultString(KeySerialPort, "COM1");
            }
            set
            {
                if (AddOrUpdateValue(KeySerialPort, value))
                {

                }
            }
        }

        public int BaudRate
        {
            get
            {
                return GetValueOrDefaultInteger(KeyBaudRate, 115200);
            }
            set
            {
                if (AddOrUpdateValue(KeyBaudRate, value))
                {

                }
            }
        }

    }
}
