﻿using System;

namespace HALLibrary
{
    public interface InterfaceSensor
    {
        void Initialize();
        bool GetSensorValue(out double value);
        void DetachSensor();
    }
}
