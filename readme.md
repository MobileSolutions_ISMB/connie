## Soluzione Azure per l'interconnessione dei punti ##

![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Images/CTD-logo-v5-02.png)

Il progetto esempio contenuto in questo repository proviene dal progetto [ConnectTheDots.io](http://connectthedots.io). Tale progetto è un progetto Open Source creato da [Microsoft Open Technologies](http://msopentech.com), che mira a creare una facile e veloce infrastruttura per l'interconnesione di punti oppure per la strutturazione di soluzioni IoT più vaste. Questo è reso possibile sfruttando strumenti messi a disposizione da Microsoft, come **Azure Stream Analytics**, **Azure Machine Learning**, **Azure Event Hub**.

![](https://bitbucket.org/MobileSolutions_ISMB/connie/wiki/Images/ConnectTheDots%20architecture.png)

In questa parte del repository, ci sono esempi di codice, configurazioni, script e guide che consentiranno la creazione dei servizi di Windows Azure e dell'interfaccia web per la consultazione dei dati. Alcuni di questi esempi sono stati forniti da MS Open Tech, altri invece da terze parti.

Nelle cartella Devices è possibile trovare il codice sorgente, con la relativa guida, per vari dispositivi.
Nella cartella Azure è possibile trovare il codice sorgente, gli script e le configurazioni necessari per creare il servizio su Azure.